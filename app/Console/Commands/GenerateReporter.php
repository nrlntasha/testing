<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\StubGenerator;

class GenerateReporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:reporter
                        {className : The new reporter class name.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $className = $this->argument('className');

        try {
            $stub = new StubGenerator(
                app_path("Services/Stubs/Reporter.stub"),
                app_path("Services/Reporters/$className.php")
            );

            $stub->render([
                ':CLASS_NAME:' => $className
            ]);
            
            return $this->info('Reporter Generated Successfully!');
        } catch(\Exception $e) {
            return $this->error($e->getMessage());
        }
    }
}
