<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SplitTreePerMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'split:trees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunkNumber = 0;
        DB::table('trees')->orderBy('treeID')->chunk(rand(1200, 12000), function($trees) use(&$chunkNumber) {
            $this->info('Processing Chunk #' . $chunkNumber);

            $splitted = $trees->split(12);

            $month = Carbon::createFromDate('2020', 1, 1);

            foreach($splitted as $splittedTrees) {
                $this->comment('Processing date: ' . $month->format('m-Y'));

                $ids = $splittedTrees->pluck('treeID')->toArray();

                DB::table('trees')->whereIn('treeID', $ids)->update([
                    'created_at' => $month->copy()->add(rand(1, 30), 'days'),
                    'updated_at' => $month->copy()->add(rand(1, 30), 'days'),
                ]);

                $month->addMonth();
            }


            $chunkNumber++;
        });

        $this->info('DONE');
    }
}
