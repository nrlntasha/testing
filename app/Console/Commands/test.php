<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Str;
use App\User;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $input['username'] = Str::random(8);
        $input['password'] = bcrypt('Admin123');
        $input['role'] = 'superadmin';
        $input['email'] = Str::random(8) . '@gmail.com';
        $input['ic_number'] = Str::random(12);

        User::create($input);

        // echo 'Success';
        // dd('Cron executed');
    }
}
