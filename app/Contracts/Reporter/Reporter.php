<?php 

namespace App\Contracts\Reporter;

interface Reporter {


  /**
   * Handle the report generation.
   * 
   * @param  array $data
   * @return mixed
   */
  public function handle(array $data = []);

}