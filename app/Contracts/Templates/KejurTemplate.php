<?php

namespace App\Contracts\Templates;

interface KejurTemplate {

  public function getTemplate(array $data = []);

}