<?php

namespace App\DataTables;

use App\Tree;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Utilities\Request;
use Illuminate\Support\Str;

class TreeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            // ->filter(function ($q) use ($request) {
            //     if($request->has('risk')){
            //         $q->collection = $q->collection->filter(function ($row) use ($request) {
            //             return Str::contains($row['risk'], $request->get('risk')) ? true : false;
            //         });
            //     }
            // })
            ->addIndexColumn()
            ->addColumn('action', function($query){
                return '<a href="maintance-pokok/'.$query->treeID.'" class="btn btn-outline-success waves-effect waves-light">Selenggara</a>';
            })
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TreeDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Tree::select('treeID', 'zone', 'location', 'local_name', 'saintifik_name', 'inventory_no', 'risk')
                    ->when(request('zone'), function($q){
                        return $q->where('zone', request('zone'));
                    })
                    ->when(request('location'), function($q){
                        return $q->where('location', request('location'));
                    })
                    ->when(request('name'), function($q){
                        return $q->where('local_name', request('name'));
                    })
                    ->when(request('risk'), function($q){
                        return $q->whereIn('risk', request('risk'));
                    })
                    ->when(request('category'), function($q){
                        return $q->whereIn('category', request('category'));
                    })
                    ->when(request('silara'), function($q){
                        return $q->whereIn('silara', request('silara  '));
                    })
                    ->orderBy('created_at', 'DESC');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->addColumnBefore([
                        'defaultContent' => '',
                        'data'           => 'DT_RowIndex',
                        'name'           => 'DT_RowIndex',
                        'title'          => 'Bil',
                        'render'         => null,
                        'orderable'      => false,
                        'searchable'     => false,
                        'exportable'     => false,
                        'printable'      => true,
                        'footer'         => '',
                    ])
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                        ->buttons(
                            Button::make('create'),
                            Button::make('export'),
                            Button::make('print'),
                            Button::make('reset'),
                            Button::make('reload')
                        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('local_name'),
            Column::make('saintifik_name'),
            Column::make('zone'),
            Column::make('location'),
            Column::make('inventory_no'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Tree_' . date('YmdHis');
    }

    public function printPreview()
    {
        $title = "Senarai Pokok";
        $data = $this->getDataForPrint();
        return view($this->printPreview, compact('data', 'title'));
    }
}
