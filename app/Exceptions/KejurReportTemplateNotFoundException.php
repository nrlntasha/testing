<?php

namespace App\Exceptions;

use Exception;

class KejurReportTemplateNotFoundException extends Exception
{
    /**
     * Build the Exception instance.
     * 
     * @param  string $key
     * @return Exception
     */
    public function __construct($key) {
        return parent::__construct(
            sprintf("No Kejur Template with key [%s]", $key)
        );
    }
    

}
