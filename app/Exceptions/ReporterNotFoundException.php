<?php

namespace App\Exceptions;

use Exception;

class ReporterNotFoundException extends Exception
{
    /**
     * Build the Exception instance.
     * 
     * @param  string $key
     * @return Exception
     */
    public function __construct($key) {
        return parent::__construct(
            sprintf("No Reporter with key [%s]", $key)
        );
    }
    
}
