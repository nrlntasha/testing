<?php

namespace App\Http\Controllers\API;

use DB;
use Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kejur;
use App\LogKejur;
use App\KejurArea;
use App\KejurMedia;
use App\KejurStatus;
use App\SystemLogs;
use Dotenv\Regex\Success;
use Exception;
use Illuminate\Support\Facades\Schema;
use stdClass;
use Throwable;

class APIKejurController extends Controller
{
    public function index(Request $request)
    {
        try {

            $kejurs = Kejur::select('kejurID', 'no_inventori','location', 'kawasan')
                    ->when($request->keywords, function($q) use($request){
                        $q->where('location', 'LIKE', "%{$request->keywords}%")->orWhere('no_inventori', 'LIKE',"%{$request->keywords}%");
                    })
                    ->orderBy('created_at', 'desc')->paginate(10);

            return response()->json(['message' => 'success', 'data' => $kejurs], 200);

        } catch (Exception $e) {

            return response()->json(['message' => 'Error', 'data' => 'Internal Server Error'], 400);

        }
    }

    public function getKawasan(Request $request){
        $data = Kejur::where('kawasan', 'LIKE', "%{$request->keywords}%")->distinct()->get('kawasan');

        return response()->json([
            'message' => 'success',
            'keywords' => $request->keywords,
            'count' => $data->count(),
            'data' => $data
        ]);
    }

    public function getAlatan(){

        $data = collect(config('AlatanLandskapKejur'));

        return response()->json([
            'message' => 'Success',
            'data' => $data->flatten()
        ]);
    }

    public function store(Request $request)
    {
        try {

            $kejurID = DB::table('kejurs')
                ->latest('kejurID')
                ->first();

            if (is_null($kejurID)) {
                $kejurIDs = 'KJ0000000';
            } else {
                $kejurID = Str::substr($kejurID->kejurID, 2);
                $kejurIDs = sprintf('KJ%07d', $kejurID + 1);
            }

            $input = $request->all();
            $input['kejurID'] = $kejurIDs;
            $input['perwartaan'] = $this->boolCast($request->perwartaan);
            $input['taman'] = $this->boolCast($request->taman);
            $input['kawasan_lapang'] = $this->boolCast($request->kawasan_lapang);
            $input['penampan'] = $this->boolCast($request->penampan);
            $input['padang'] = $this->boolCast($request->padang);
            $input['dewan'] = $this->boolCast($request->dewan);
            $input['dijumpai'] = $this->boolCast($request->dijumpai);
            $input['created_by'] = $request->username;
            $input['status_pengambilan'] = KejurStatus::whereStatus($request->status_pengambilan)->first()->getKey();

            Kejur::create($input);

            // DB::table('kejurs')
            //     ->insert([
            //         'kejurID' => $kejurIDs,
            //         'panel_id' => $request->panel_id,
            //         'location' => $request->location,
            //         'inventory_no' => $request->inventory_no,
            //         'maintance_status' => $request->maintance_status,
            //         'notes' => $request->notes,
            //         'acres' => $request->acres,
            //         'intake_date' => $request->intake_date,
            //         'ambilalih' => $request->ambilalih,
            //         'kemudahan' => $request->kemudahan,
            //         'perwartaan' => $request->perwartaan,
            //         'kawasan_lapang' => $request->kawasan_lapang,
            //         'taman' => $request->taman,
            //         'penampan' => $request->penampan,
            //         'bola' => $request->bola,
            //         'dewan' => $request->dewan,
            //         'dijumpai' => $request->dijumpai
            //     ]);

            // return redirect('/superadmin/kejur/gambar/'.$kejurIDs);

            // if($request->hasfile('gambar_kejur')){

            //     //dd($request->file('gambar_pokok'));
            //     foreach($request->file('gambar_kejur') as $file){

            //         $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            //         $file->move(public_path().'/kejur/', $imgName);
            //         //$data[$file] = $name;

            //         DB::table('media')
            //             ->insert([
            //                 'treeID' => $kejurIDs,
            //                 'image_name' => $imgName,
            //                 'img_ext' => $file->getClientOriginalExtension(),
            //                 'tags' => 'Kejur'
            //             ]);
            //     }

            // }

            // return redirect('/superadmin/kejur')->with('success', 'Data Kejur Disimpan');
            return response()->json(['message' => 'success', 'data' => 'Data Kejur Disimpan'], 200);
        } catch (Exception $e) {
            // return back()->with('error', 'Intenal Server Error');
            return response()->json(['message' => 'error', 'data' => 'Server Error', 'details' => $e->getMessage()], 400);
        }
    }

    public function detail($id)
    {

        try {

            $data = Kejur::where('kejurID', $id)->select('kejurID', 'no_inventori', 'location', 'keluasan')->get();

            // $kejur = DB::table('kejurs')
            //     ->where('kejurID', $id)
            //     ->first();

            // $log_kejurs = DB::table('log_kejurs')
            //     ->where('log_kejurs.kejurID', $id)
            //     ->get();

            // $kejur_media = DB::table('kejur_media')
            //                 ->where('kejurID', $id)
            //                 ->orderBy('nama_alatan')
            //                 ->get();

            // $collection = DB::table('kejur_media')
            //     ->where('kejurID', $id)
            //     ->get()
            //     ->map(function ($media) {

            //         $imagePath = collect(explode(" ", $media->tags))
            //             ->toLower()
            //             ->implode("/");

            //         $media->image_url = url(sprintf("/%s/%s", $imagePath, $media->image_name));

            //         return $media;
            //     });

            // $kejur_medias = $collection->groupBy('nama_alatan');

            // return view('superadmin.landskap.kejur.detail', compact('kejur', 'log_kejurs', 'kejur_medias'));
            // return response()->json(['message' => 'success', 'data' => $kejur, 'data_logs' => $log_kejurs, 'kejur_medias' => $kejur_medias], 200);
            return response()->json(['message' => 'success', 'data' => $data], 200);
        } catch (Exception $e) {
            // dd($e);
            return response()->json(['message' => 'error', 'data' => 'Server Error'], 400);
        }
    }

    public function detail2($id)
    {

        try {
            $kejur = DB::table('kejurs')
                ->where('kejurID', $id)
                ->first();

            // return view('superadmin.landskap.kejur.update', compact('kejur'));
            return response()->json(['message' => 'success', 'data' => $kejur], 200);
        } catch (Exception $e) {
            return response()->json(['message' => 'error', 'data' => 'Server Error'], 400);
        }
    }

    public function saveUpdate(Request $request, $id)
    {

        try {

            DB::table('kejurs')
                ->where('KejurID', $id)
                ->update([
                    'location' => $request->location,
                    'inventory_no' => $request->inventory_no,
                    'acres' => $request->acres,
                    'maintance_status' => $request->maintance_status,
                    'intake_date' => $request->intake_date,
                    'notes' => $request->notes
                ]);

            // return redirect('/superadmin/kejur/detail/'.$id)->with('success', 'Data Dikemaskini');
            return response()->json(['message' => 'success', 'data' => 'Kejur Dikemaskini'], 200);
        } catch (Exception $e) {
            // dd($e);
            return response()->json(['message' => 'error', 'data' => 'Server Error'], 400);
        }
    }

    public function destroy($kejurID)
    {

        try {

            DB::table('kejurs')
                ->where('kejurID', $kejurID)
                ->delete();

            $medias = DB::table('medias')
                ->where('treeID', $kejurID)
                ->get();

            foreach ($medias as $media) {
                File::delete('kejur/' . $media->image_name);
            }

            DB::table('medias')
                ->where('treeID', $kejurID)
                ->delete();

            // return redirect('/superadmin/kejur')->with('success', 'Data Dipadam');
            return response()->json(['message' => 'success', 'data' => 'Kejur Dipadam'], 200);
        } catch (Exception $e) {
            // return back()->withErrors('error', 'Internal Data Error');
            return response()->json(['message' => 'error', 'data' => 'Server Error'], 400);
        }
    }

    public function gambar($id)
    {

        $kejurs = DB::table('kejurs')
            ->where('kejurID', $id)
            ->first();

        $log_kejurs = DB::table('log_kejurs')
            ->where('kejurID', $id)
            ->get();

        // $kejur_media = DB::table('kejur_media')
        //                 ->where('kejurID', $id)
        //                 ->orderBy('nama_alatan')
        //                 ->get();

        $collection = DB::table('kejur_media')
            ->where('kejurID', $id)
            ->get();

        $kejur_medias = $collection->groupBy('nama_alatan');

        // dd($kejur_medias);

        // foreach ($kejur_medias as $key=>$medias) {
        //     echo $key;
        //     foreach ($medias as $media) {
        //         echo $media->image_name;
        //     }
        // }

        // return view('superadmin.landskap.kejur.gambar', compact('kejur', 'log_kejurs', 'kejur_medias'));
        return response()->json(['message' => 'success', 'data' => $kejurs, 'data_logs' => $log_kejurs, 'kejur_medias' => $kejur_medias], 200);
    }

    public function upload(Request $request)
    {

        try {

            if ($request->hasfile('gambar_alatan')) {

                $file = $request->file('gambar_alatan');

                // dd($request->file('gambar_alatan'));
                // foreach ($request->file('gambar_alatan') as $file) {

                    $imgName = md5($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path() . '/kejur/', $imgName);

                    KejurMedia::create([
                            'kejurID' => $request->kejurID,
                            'nama_alatan' => $request->nama_alatan,
                            'image_name' => $imgName,
                            'img_ext' => $file->getClientOriginalExtension(),
                            'tags' => 'Kejur'
                        ]);
                // }

                LogKejur::create($request->all());

            }

            return response()->json(['message' => 'success', 'data' => 'Alatan kejur berjaya disimpan.'], 200);

        } catch (Exception $e) {

            return response()->json(['message' => 'error', 'data' => 'Server Error'], 400);

        }
    }

    public function deleteGambar(Request $request, $id)
    {

        // dd($request->query('id'));
        try {

            $medias = DB::table('kejur_media')
                ->where('kejurID', $id)
                ->where('nama_alatan', $request->query('id'))
                ->get();

            // dd($medias);

            foreach ($medias as $media) {
                File::delete('kejur/' . $media->image_name);
            }

            DB::table('kejur_media')
                ->where('kejurID', $id)
                ->where('nama_alatan', $request->query('id'))
                ->delete();

            DB::table('log_kejurs')
                ->where('kejurID', $id)
                ->where('nama_alatan', $request->query('id'))
                ->delete();

            // return redirect()->back()->with('success', 'Gambar Dipadam');
            return response()->json(['message' => 'success', 'data' => 'Gambar Kejur Dipadam'], 200);
        } catch (Exception $e) {
            return response()->json(['message' => 'error', 'data' => 'Server Error', 'description' => $e], 400);
        }
    }

    public function countKejur()
    {

        try {
            $data = Kejur::count();

            return response()->json(['message' => 'success', 'data' => $data], 200);
        } catch (Exception $ex) {
            return response()->json(['message' => 'error', 'data' => 'Server Error', 'description' => $ex], 400);
        }
    }

    public function perZone(Request $request)
    {
        try {

            // $data = DB::table('kejurs as a')->selectRaw('IFNULL(sum(kuantiti_semasa), 0) as totalCount, zon as zone')
            //         ->leftJoin('log_kejurs as b', 'a.kejurID', '=', 'b.kejurID')
            //         ->when($request->year, function($q) use($request){
            //             return $q->whereYear('b.created_at', $request->year);
            //         })
            //         ->when($request->month, function($q) use($request){
            //             return $q->whereMonth('b.created_at', $request->month);
            //         })
            //         ->groupBy('zone')->get();

            $data = Kejur::when($request->year, function($q) use($request){
                            return $q->whereYear('created_at', $request->year);
                        })
                        ->when($request->month, function($q) use($request){
                            return $q->whereMonth('created_at', $request->month);
                        })
                        ->get()->groupBy('zon')->map(function($item, $key){
                            return ['zone' => $key, 'totalCount' => $item->count()];
                        })->values();

            $total_kejur = $data->sum('totalCount');

            $month = config('month');
            $month = $request->month ? $month[$request->month - 1]: 'All';
            $year = $request->year ?:'All';

            return response()->json([
                'message' => 'success',
                'year' => $year,
                'month' => $month,
                'total' => $total_kejur,
                'data' => $data
            ], 200);

        } catch (Exception $ex) {
            return response()->json(['message' => 'error', 'data' => 'Server Error', 'description' => $ex], 400);
        }
    }

    public function propertyList(Request $request)
    {
        try{

            $itemLists = config('AlatanLandskapKejur');
            $itemLists2 = [];

            foreach ($itemLists['SET ALAT PERMAINAN BERSEPADU'] as $a) {
                array_push($itemLists2, $a);
            }
            foreach ($itemLists['SET PENCAHAYAAN'] as $b) {
                array_push($itemLists2, $b);
            }
            foreach ($itemLists['LAIN-LAIN'] as $c) {
                array_push($itemLists2, $c);
            }

            $item = DB::table('kejurs as a')->select('kuantiti_semasa', 'nama_alatan')
                    ->join('log_kejurs as b', 'a.kejurID', '=', 'b.kejurID')
                    ->where('zon', $request->zone)
                    ->when($request->year, function($q) use($request){
                        return $q->whereYear('b.created_at', $request->year);
                    })
                    ->when($request->month, function($q) use($request){
                        return $q->whereMonth('b.created_at', $request->month);
                    })
                    ->get();

            $month = config('month');
            $month = $request->month ? $month[$request->month - 1]: 'All';
            $year = $request->year ?:'All';

            $data = new stdClass;
            $zone = $request->zone;
            $total = $item->sum('kuantiti_semasa');

            $property = [];

            foreach($itemLists2 as $value){
                $property2 = new stdClass;
                $property2->item_name = $value;
                $property2->count = $item->where('nama_alatan', $value)->sum('kuantiti_semasa');
                array_push($property, $property2);
            }

            $data = $property;

            return response()->json(['message' => 'successful', 'year' => $year,'month' => $month, 'zone' => $zone, 'total' => $total, 'data' => $data], 200);
        }catch(Throwable $e){
            return response()->json(['message' => 'error', 'data' => 'Server Error', 'description' => $e], 400);
        }
    }

    public function boolCast($value){
        return $value == "true" ? true : false ;
    }

    public function updateMaintainance(Request $request){

        try{
            $input = $request->all();
            $data = LogKejur::create($input);

            // $logs['username'] = $request->username;
            // $logs['comments'] = "Alatan kejur berjaya ditambah kejurID {$request->kejurID}";
            // $logs['category'] = "Kejur";
            // SystemLogs::create($logs);

            return response()->json(['message' => 'Success', 'data' => $data], 200);
        }catch(Throwable $th){
            return response()->json(['message' => 'Error', 'data' => 'no data', 'description' => $th->getMessage()], 400);
        }
    }

    /* FOR SYNC PURPOSE!!! DON'T EVER TOUCH IF YOU DON'T KNOW */

    public function syncKejur(){
        $data = KejurArea::all();

        Schema::disableForeignKeyConstraints();
        DB::table('kejurs')->truncate();
        Schema::enableForeignKeyConstraints();
        foreach($data as $item){
            $new = sprintf('KJ%07d',$item->id);
            DB::table('kejurs')->insert([
                'kejurID' => $new,
                'no_inventori' => $item->no_inventori,
                'zon' => $item->zon,
                'kawasan' => $item->kawasan,
                'location' => $item->lokasi,
                'catatan' => $item->catatan,
                'keluasan' => $item->keluasan,
                'status_pengambilan' => $item->status_pengambilan,
                'taman' => $item->taman,
                'penampan' => $item->taman,
                'padang' => $item->padang,
                'dewan' => $item->dewan
                ]);
        }
        $lol = Kejur::all();
        return response()->json(['message' => 'success', 'data' => $lol],200);
    }
    /* ******************************************************************************************** */
}
