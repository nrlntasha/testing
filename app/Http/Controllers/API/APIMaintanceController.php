<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MaintainanceActivity;
use App\MaintainanceStatus;
use App\Maintance;
use App\SystemLogs;
use App\TreeMedia;
use App\TreeRisk;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Type;
use Exception;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

class APIMaintanceController extends Controller
{
    /* MAINTAINANCE INDEX PAGE FUNCTION */
    public function index(){

        try{

            $maintances = DB::table('maintances')
                            ->simplePaginate(10);

            return response()->json([
                'message'   =>  'success',
                'data'      =>  $maintances
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message'       =>  'Error',
                'data'          =>  'Internal Server Error',
                'description'   =>  $e->getMessage()
            ],400);

        }
    }
    /* END MAINTAINANCE INDEX PAGE FUNCTION */

    /* MAINTAINANCE DETAIL PAGE FUNCTION */
    public function getByID($maintainID){

        try{

            $maintances = DB::table('maintances')
                            ->where('maintainID', $maintainID)
                            ->get();

            $media = DB::table('media')
                        ->where('treeID', $maintainID)
                        ->get()
                        ->map(function($media) {

                            $imagePath = collect(explode(" ", $media->tags))
                                            ->toLower()
                                            ->implode("/");

                            $media->image_url = url(sprintf("/%s/%s", $imagePath, $media->image_name));

                            return $media;

                        });


            return response()->json([
                'message'   =>  'success',
                'data'      =>  $maintances,
                'media'     =>  $media
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message'       =>  'Error',
                'data'          =>  'Internal Server Error',
                'description'   =>  $e->getMessage()
            ],400);
        }
    }
    /* END MAINTAINANCE DETAIL FUNCTION */

    /* GET ALL MAINTAINANCE ACTIVITY */
    public function getMaintainanceActivity()
    {
        try{

            $data = MaintainanceActivity::all();

            return response()->json([
                'message'   =>  'success',
                'data'      =>  $data
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message'       => 'error',
                'data'          => 'Internal Server Error',
                'description'   => $e->getMessage()
            ]);

        }
    }
    /* END GET ALL MAINTAINANCE ACTIVITY */

    /* STORE NEW RECORD FOR MAINTAINANCE */
    public function store(Request $request){
        try{

            /* GET LATEST MAINTAINANCE ID */
            $mainID = DB::table('maintances')
                    ->latest('maintainID')
                    ->first();

            /* IF NO RECORD EXISTED THEN RETURN MT00000000; ELSE CONTINUE */
            if(is_null($mainID)){
                $mainIDs = 'MT0000000';
            }else{
                $mainID = Str::substr($mainID -> maintainID, 2);
                $mainIDs = sprintf('MT%07d',$mainID + 1);
            }

            /* REQUEST ALL PARAM EXCEPT IMAGES */
            $input = $request->except(['img_before', 'img_after']);
            $input['maintainID'] = $mainIDs;
            $input['risk_level'] = TreeRisk::whereName($request->risk_level)->first()->getKey();
            $input['status_maintance'] = MaintainanceStatus::whereStatus($request->status_maintance)->first()->getKey();
            $input['maintain_date'] = date('Y-m-d H:i:s', strtotime($request->maintain_date));

            /* CREATE RECORD AND SAVE INTO MAINTAINANCE TABLE */
            $createMaintainance = Maintance::create($input);

            /* CHECK IF IMAGE IS EXIST */
            if($request->hasfile('img_before')){

                    $file = $request->file('img_before');

                    /* RENAME IMAGE USING MD5 ENCODING IMAGE NAME */
                    $imgNameBefore = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
                    // $file->move(public_path().'/maintance/sebelum/', $imgNameBefore);

                    /* TRANSFER FILE TO PUBLIC_FOLDER/MAINTANCES/SEBELUM AND REDUCE IMAGE QUALITY DUE TO PERFORMANCE OPTIMIZATION */
                    Image::make($file)->save(public_path()."/maintance/sebelum/{$imgNameBefore}", 35);

                    $mediaInputBefore['reference_id'] = $createMaintainance->getKey(); // GET PRIMARY KEY AFTER INSERTED AS FOREIGN KEY.
                    $mediaInputBefore['image_name'] = $imgNameBefore;
                    $mediaInputBefore['img_ext'] = $file->getClientOriginalExtension();
                    $mediaInputBefore['tags'] = 'Sebelum Penyelenggaraan';
                    $mediaInputBefore['type_id'] = Type::whereType('Penyelenggaraan')->first()->getKey(); // GET PRIMARY KEY AS FOREIGN KEY FOR REFERNCE

                    /* INSERT RECORD INTO MEDIA TABLE */
                    TreeMedia::create($mediaInputBefore);
            }

            /* CHECK IF IMAGE IS EXIST */
            if($request->hasfile('img_after')){

                $selepas = $request->file('img_after');

                /* RENAME IMAGE USING MD5 ENCODING IMAGE NAME */
                $imgNameAfter = md5($selepas->getClientOriginalName()).'.'.$selepas->getClientOriginalExtension();
                // $selepas->move(public_path().'/maintance/selepas/', $imgNameAfter);

                /* TRANSFER FILE TO PUBLIC_FOLDER/MAINTANCES/SEBELUM AND REDUCE IMAGE QUALITY DUE TO PERFORMANCE OPTIMIZATION */
                Image::make($selepas)->save(public_path()."/maintance/selepas/{$imgNameAfter}", 35);

                $mediaInputAfter['reference_id'] = $createMaintainance->getKey();
                $mediaInputAfter['image_name'] = $imgNameAfter;
                $mediaInputAfter['img_ext'] = $selepas->getClientOriginalExtension();
                $mediaInputAfter['tags'] = 'Selepas Penyelenggaraan';
                $mediaInputAfter['type_id'] = Type::whereType('Penyelenggaraan')->first()->getKey();

                /* INSERT RECORD INTO MEDIA TABLE */
                TreeMedia::create($mediaInputAfter);

            }

            /* WAIT AFTER AUTHENTICATE FUNCTION */
            // SystemLogs::create([
            //     'username' => Session::get('username'),
            //     'comments' => "Penyelenggaraan telah ditambah melalui peranti mudah alih id => {$mainIDs}",
            //     'category' => 'Penyelenggaraan Pokok'
            // ]);

            return response()->json([
                'message'   =>  'success',
                'data'      =>  'Penyelenggaraan Berjaya Disimpan'
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message'       =>  'error',
                'data'          =>  'Server Error',
                'description'   =>  $e->getMessage()
            ],400);

        }
    }
    /* END STORE NEW MAINTAINANCE RECORD */

    /* UPDATE MAINTAINANCE RECORD FUNCTION */
    public function update(Request $request, $mainID){

        try{

            DB::table('maintances')
                ->where('maintainID', $mainID)
                ->update([
                    'username'          => $request->username,
                    'risk_level'        => $request->risk_level,
                    'type_maintance'    => $request->type_maintance,
                    'status_maintance'  => $request->status_maintance,
                    'maintain_date'     => $request->maintain_date,
                    'notes'             => $request->notes
                ]);

            return response()->json([
                'message'   =>  'success',
                'data'      =>  'Penyelenggaraan Dikemaskini'
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message'       =>  'error',
                'data'          =>  'Server Error',
                'description'   =>  $e->getMessage()
            ],400);

        }
    }
    /* END UPDATE MAINTAINANCE RECORD FUNCTION */

    /* DELETE MAINTAIANCE RECORD */
    public function destroy($mainID){
        try{

            DB::table('maintances')
                ->where('maintainID', $mainID)
                ->delete();

            return response()->json([
                'message'   =>  'success',
                'data'      =>  'Penyelenggaraan Dipadam'
            ],200);
        }catch(Exception $e){
            return response()->json([
                'message'       =>  'error',
                'data'          =>  'Server Error',
                'description'   =>  $e->getMessage()
            ],400);
        }
    }
    /* END DELETE MAINTAINANCE RECORD */
}
