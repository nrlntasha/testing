<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Message;
use App\MessageDetail;
use App\Notification;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use stdClass;

class APINotificationController extends Controller
{

    /* LIST OF USER FOR MESSAGE FORM */
    public function listUsers(Request $request){

        try{

            $data = User::where('username', '!=', $request->username)->pluck('username');

            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'Error',
                'description' => $ex->getMessage()
            ]);

        }

    }

    /* SEND MESSAGE NOTIFICAITON */
    public function sendMessage(Request $request)
    {
        try{

            $receipent = $request->receipent;
            $subject = $request->subject;
            $message = $request->message;

            $receipent_id = Notification::whereIn('user_id', User::whereIn('username', $receipent)->pluck('user_id'))->pluck('onesignal_id');

            /* CREATE OBJECT INSTANCES FOR BODY */
            $body = new stdClass;
            $body->app_id = config('onesignal.app_id');
            $body->include_player_ids = $receipent_id;
            $body->headings = (object)array('en' => $subject);
            $body->contents = (object)array('en' => $message);

            /* STORE MESSAGE INTO TABLE */
            $insertMessage = Message::create($request->all());

            /* STORE MESSAGE DETAILS INTO TABLE */
            $input['from'] = User::whereUsername($request->from)->first()->user_id;
            $input['message_id'] = $insertMessage->getKey();
            $to = User::whereIn('username', $receipent)->pluck('user_id');

            foreach($to as $input['to']){
                MessageDetail::create($input);
            }

            /* SEND NOTIFICATION TO USER USING REQUEST */
            $body->android_channel_id = "a4793d1a-4a91-4077-bd62-2a788e87b2a5";
            $response = Curl::to('https://onesignal.com/api/v1/notifications')
                        ->withHeaders(array('Content-Type:application/json', 'Authorization:YTJkNWE5ZWUtNGI2MS00MDE2LWE1ZGItZTFhOTA0MzI3NDM1'))
                        ->withData($body)
                        ->asJson()
                        ->post();


            return response()->json([
                'message' => 'Success',
                'response' => $response
            ]);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'Error',
                'description' => $ex->getMessage()
            ]);

        }

    }

    /* GET ALL MESSAGE BASED ON USERNAME */
    public function message($username)
    {
        try{

            $user_id = User::find($username)->user_id;

            $data = MessageDetail::whereTo($user_id)->latest('created_at')->paginate(10);

            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'Error',
                'data' => 'No Data',
                'description' => $ex->getMessage()
            ]);

        }
    }

    /* READ MESSAGE FUNCTION */
    public function read($id)
    {
        try{

            $data = MessageDetail::find($id);
            $data->is_read = true;
            $data->save();

            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'Error',
                'data' => 'No Data',
                'description' => $ex->getMessage()
            ]);

        }
    }

    /* COUNT UNREAD MESSAGES */
    public function unreadCount($username)
    {

        $data = MessageDetail::whereTo(User::find($username)->user_id)->unread()->count();

        return response()->json([
            'message' => 'Success',
            'data' => $data
        ]);
    }

}
