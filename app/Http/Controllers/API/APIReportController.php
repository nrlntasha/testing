<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tree;
use App\Kejur;
use App\LogKejur;
use App\Maintance;
use Exception;
use Illuminate\Support\Facades\DB;
use Throwable;

class APIReportController extends Controller
{
    public function __construct()
    {
        //
    }
    public function treeReport(Request $request)
    {
        try{

            $data = Tree::with([
                        /* GET LATEST MAINTAINANCE LIST */
                        'maintainance' => function($q){
                            return $q->latest('created_at');
                        },
                        'maintainance.images'
                    ])
                    ->whereHas('maintainance')
                    ->whereHas('species', function($q)use($request){
                        /* SEARCH WHETHER SCIENTIFIC NAME NOR LOCAL NAME */
                        $q->where('tree_species.scientific_name', 'LIKE', "%$request->keywords%")
                        ->orWhere('tree_species.local_name', 'LIKE', "%$request->keywords%");

                    })
                    ->paginate($request->paginate ?: 10);

            // $data = Maintance::with('getTree', 'images')
            // ->whereHas('getTree.species', function($q) use($request){
            //         $q->where('tree_species.scientific_name', 'LIKE', "%$request->keywords%");
            // })
            // ->whereUsername($request->username)
            // ->latest('created_at')
            // ->paginate($request->paginate ?: 10);

            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);

        }catch(Exception $th){

            response()->json([
                'message' => 'Error',
                'data' => 'No Data',
                'description' => $th->getMessage()
            ]);

        }
    }

    public function kejurReport(Request $request)
    {
        try{

            $data = LogKejur::whereUsername($request->username)
                    ->with('kejur', 'images')
                    ->where('kejurID', 'LIKE',"%{$request->keywords}%")
                    ->latest('created_at')
                    ->paginate($request->paginate ?: 10);

            return response()->json([
                'message' => 'Success',
                'data' => $data
            ]);

        }catch(Exception $e){

            return response()->json([
                'message' => 'Exception',
                'description' => $e->getMessage()
            ]);

        }
    }
}
