<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\saintifikName;
use App\Tree;
use App\TreeSilara;
use App\TreeSpecies;

class APISearchController extends Controller
{
    public function allzone(Request $request){

        try{
            $zone = DB::table('trees')
                        ->select('zone')
                        ->groupBy('zone')
                        ->get();

            return response()->json(['message'=> 'success', 'data' => $zone], 200);
        }catch(Exception $e){
            return response()->json(['message'=>'error', 'data'=>'Internal Server Error'],400);
        }

    }

    public function lokasi(Request $request){
        try{
            $location = DB::table('trees')
                        ->where('zone', $request->zone)
                        ->select('location')
                        ->groupBy('location')
                        ->get();

            return response()->json(['message'=>'success', 'data' => $location], 200);
        }catch(Exception $e){
            return response()->json(['message'=>'error', 'data'=>'Internal Server Error'],400);
        }

    }

    public function nama_biasa(Request $request){
        try{
            $local_name = Tree::with('media:reference_id,image_name','species:id,local_name,scientific_name')
                            ->where([
                                'zone' => $request->zone,
                                'location' => $request->location
                                ])
                            ->get();

            return response()->json(['message'=>'success', 'data' => $local_name], 200);
        }catch(Exception $e){
            return response()->json(['message'=>'error', 'data'=>'Internal Server Error'],400);
        }

    }

    public function semua(Request $request){
        try{

            $semua = Tree::whereHas('species', function($query) use($request){
                            return $query->where('local_name', $request->id);
                        })->with('species:id,local_name,scientific_name')
                        ->where([
                            'zone' => $request->zone,
                            'location' => $request->location
                        ])
                        // ->where('trees.location', $request->location)
                        ->simplePaginate($request->limit ?: 10);

            return response()->json(['message'=>'success', 'data' => $semua], 200);
        }catch(Exception $e){
            return response()->json(['message'=>'error', 'data'=>'Internal Server Error'],400);
        }

    }

    public function saintifik(Request $request) {
        // $saintifik = DB::table('trees')
        //                     ->distinct()
        //                     ->whereNotNull('saintifik_name')
        //                     ->where('saintifik_name', '!=', "")
        //                     ->selectRaw('saintifik_name as name')
        //                     ->groupBy('name')
        // $saintifik = TreeSpecies::select('scientific_name')->whereNotNull('scientific_name')->where('scientific_name', '!=', '')->simplePaginate($request->limit ?: 10);
        // dd(TreeSpecies::scientificName());
        $saintifik = TreeSpecies::scientificName();

        return response()->json(['message'=>'success', 'data' => $saintifik], 200);
    }

    public function allSaintifik(Request $request){
        $data = TreeSpecies::select('scientific_name')->whereNotNull('scientific_name')->where('scientific_name', 'LIKE', "%{$request->keywords}%")->get();
        $count = $data->count();

        return response()->json([
            'message' => 'success',
            'total' => $count,
            'data' => $data
        ], 200);
    }

    public function allLocal(Request $request){
        $data = TreeSpecies::select('local_name')->whereNotNull('local_name')->where('scientific_name', $request->keywords)->get();
        $count = $data->count();

        return response()->json([
            'message' => 'success',
            'count' => $count,
            'data'=> $data
        ], 200);
    }

    public function findByScientificName(Request $request){

        $query = Tree::whereHas('species', function($q) use($request){
            $q->where('scientific_name', $request->name);
        })->when($request->zone, function($q) use($request){
            $q->where('zone', $request->zone);
        })->when($request->keywords, function($q) use($request){
            $q->where('location', 'LIKE', "%{$request->keywords}%");
        });

        $count = $query->count();
        $response = $query->simplePaginate(10);

        return response()->json([
            'message' => 'success',
            'param' => $request->keywords,
            'count' => $count,
            'data' => $response
        ], 200);
    }

    public function getSilara(Request $request){
        $data = TreeSilara::get('name');
        $count = $data->count();

        return response()->json([
            'message' => 'success',
            'count' => $count,
            'data' => $data
        ]);
    }

    public function getZone(){
        $data = DB::table('trees')->distinct('zone')->get('zone');
        $count = $data->count();

        return response()->json([
            'message' => 'success',
            'count' => $count,
            'data' => $data
        ]);
    }
}
