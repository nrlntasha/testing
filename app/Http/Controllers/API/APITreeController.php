<?php

namespace App\Http\Controllers\API;

use File;
use App\Tree;
use App\Kejur;
use App\Services\TreeList;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\TreeMedia;
use App\TreeSpecies;
use Mockery\CountValidator\Exception;
use stdClass;
use Throwable;
use App\SystemLogs;
use App\TreeCategory;
use App\TreeRisk;
use App\TreeSilara;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class APITreeController extends Controller
{

    protected $treeList;

    public function __construct(TreeList $treeList)
    {
        $this->treeList = $treeList;
    }

    public function store(Request $request)
    {

        try {

            /* GET LAST INSERTED TREE FROM RECORD */
            $treeID = Tree::latest('treeID')->first();

            if (is_null($treeID)) {
                $treeIDs = 'TR0000000';
            } else {
                $treeID = Str::substr($treeID->treeID, 2);
                $treeIDs = sprintf('TR%07d', $treeID + 1);
            }

            /* DECLARE & BIND PARAMETER TO NEW VARIABLE */
            $input = $request->all();
            $input['inventory_no'] = "MBPJ/${treeIDs}";
            $input['height'] = (float)$request->height;
            $input['treeID'] = $treeIDs;

            /* FIND FROM EXISTING TREE */
            try{

                $query = TreeSpecies::where([
                    'local_name' => $request->local_name,
                    'scientific_name' => $request->scientific_name
                ])->first();

                /* INSERT NEW SPECIES IF NOT EXIST AND GET INSERTED ID */
                if(!$query){
                    $createSpecies = TreeSpecies::create([
                        'local_name' => $request->local_name,
                        'scientific_name' => $request->scientific_name,
                        'genus' => $request->genus
                    ]);
                    /* BIND TO INPUT VARIABLE WITH INSERTED ID */
                    $input['species_id'] = $createSpecies->id;

                }else{
                    /* BIND TO INPUT VARIABLE WITH EXISTING ID */
                    $input['species_id'] = $query->id;

                }

            }catch(Throwable $th){

                return response()->json([
                    'message' => 'error',
                    'status_code' => 400,
                    'description' => $th->getMessage()
                ],400);

            }

            /* MODIFY VARIABLE */
            $input['silara'] = TreeSilara::whereName($request->silara)->first()->id;
            $input['risk'] = TreeRisk::whereName($request->risk)->first()->id;
            $input['category'] = TreeCategory::where('category', $request->category)->first()->id;
            ////////////////////

            /* CREATE NEW RECORD INTO TREE TABLE */
            Tree::create($input);

            /* CREATE NEW RECORD INTO IMAGE TABLE & COPY IMAGE TO SYSTEM DIRECTORY (../mbpj/public/pokok) */
            if ($request->hasfile('image')) {

                // foreach ($request->file('gambar_pokok') as $file) {

                    $file = $request->image;

                    $imgName = md5($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path() . '/pokok/', $imgName);

                    $inputImage['image_name'] = $imgName;
                    $inputImage['type_id'] = "1"; // 1 Stands for tree type
                    $inputImage['reference_id'] = $treeIDs;
                    $inputImage['img_ext'] = $file->getClientOriginalExtension();
                    $inputImage['tags'] = "Pokok";

                    TreeMedia::create($inputImage);

                    /* FOR LOGGING */
                    // $logs['username'] = Session::get('username');
                    // $logs['category'] = "Lembut";
                    // $logs['comments'] = "Pokok {$treeIDs} telah berjaya ditambah.";
                    // $in = SystemLogs::create($logs);
                    ////////////////////

                // }
            }

        // $system_logs = sess;

            /* RETURN SUCCESS WITH JSON ENCODED DATA */
            return response()->json([
                'message' => 'success',
                'status_code' => 200
            ], 200);

        } catch (Exception $e) {

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'description' => $e->getMessage()
            ], 400);

        }
    }

    public function index(Request $request)
    {

        try {

            $trees = Tree::with('media:reference_id,image_name')->simplePaginate($request->limit ?: 10);

            return response()->json([
                'message' => 'success',
                'data' => $trees
            ], 200);

        } catch (Execption $e) {

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'description' => $e->getMessage()
            ], 400);

        }
    }

    public function treeByID($id){

        try{

            $tree = Tree::with('media:reference_id,image_name')->findOrFail($id);

            return response()->json([
                'message' => 'success',
                'data' => $tree
            ], 200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => 'No Match',
                'description' => $e->getMessage()
            ],400);

        }
    }

    public function update(Request $request, $treeID)
    {

        try {

            DB::table('trees')
                ->where('treeID', $treeID)
                ->update([
                    'saintifik_name' => $request->saintifik_name,
                    'local_name' => $request->local_name,
                    'type' => $request->type,
                    'category' => $request->category,
                    'height' => $request->height,
                    'silara' => $request->silara,
                    'location' => $request->location,
                    'zone' => $request->zone,
                    'inventory_no' => $request->inventory_no,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'risk' => $request->risk,
                    'notes' => $request->notes
                ]);

            return response()->json([
                'message' => 'success',
                'data' => 'Kemaskini Berjaya'
            ], 200);

        } catch (Exception $e) {

            return response()->json([
                'message' => 'error',
                'data' => 'Pokok Tiada dalam Database'
            ], 400);

        }
    }

    public function destroy($treeID)
    {

        try {

            DB::table('trees')
                ->where('treeID', $treeID)
                ->delete();

            $medias = DB::table('media')
                ->where('treeID', $treeID)
                ->get();

            foreach ($medias as $media) {
                File::delete('pokok/' . $media->image_name);
            }

            DB::table('media')
                ->where('treeID', $treeID)
                ->delete();

            return response()->json([
                'message' => 'success',
                'data' => 'Pokok Dipadam'
            ], 200);

        } catch (Exception $e) {

            return response()->json([
                'message' => 'error',
                'data' => 'Pokok Tiada Dalam Database',
                'description' => $e->getMessage()
            ], 400);

        }
    }

    public function treeGroupData($id)
    {

        $pokokRisikoRendah      = $this->treeList->bilPokokRisikoRendah($id);
        $pokokRisikoSederhana   = $this->treeList->bilPokokRisikoSederhana($id);
        $pokokRisikoTinggi      = $this->treeList->bilPokokRisikoTinggi($id);

        return response()->json([
            'message' => 'success',
            'risikoRendah' => $pokokRisikoRendah,
            'risikoSederhana' => $pokokRisikoSederhana,
            'risikoTinggi' => $pokokRisikoTinggi
        ], 200);
    }

    public function counter($query, $type = null, $year = null, $month = null)
    {

        $data = $query->when($type == "today", function($q){
                    return $q->whereDate('created_at', Carbon::today()->toDateString());
                })
                ->when(!is_null($year), function($q) use($year){
                    return $q->whereYear('created_at', $year);
                })
                ->when(!is_null($month), function($q) use($month){
                    return $q->whereMonth('created_at', $month);
                })
                ->count();

        return $data;
    }

    public function treeCount(Request $request) {

        try{

            $treeCount = $this->counter(Tree::query(), null, $request->year, $request->month);

            $kejurCount = $this->counter(Kejur::query(), null, $request->year, $request->month);

            $treeToday = $this->counter(Tree::query(), "today");

            $kejurToday = $this->counter(Kejur::query(), "today");

            $treeOverall = $this->counter(Tree::query());

            $kejurOverall = $this->counter(Kejur::query());

            $months = config('month');
            $month = $request->month ? $months[$request->month - 1] : 'All';
            $request->merge(['month' => $month, 'year' => $request->year ?: 'All']);

            return response() -> json([
                'message' => 'success',
                'param' => $request->except('type'),
                'tree' =>[
                    'overall'  => $treeOverall,
                    'today'   => $treeToday,
                    'count'   => $treeCount
                ],
                'kejur' => [
                    'overall'  => $kejurOverall,
                    'today' => $kejurToday,
                    'count' => $kejurCount
                    ]
            ], 200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'details' => $e->getMessage()
            ], 400);

        }
    }

    public function statistic(Request $request){

        try{

            $stat = $this->counter(Tree::query(), null, $request->year ?: date('Y'));

            return response()->json([
                'message' => 'success',
                'data' => $stat
            ],200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'description' => $e->getMessage()
            ], 400);

        }
    }

    public function riskStatistic(Request $request){

        try{

            $risks = DB::table('trees')
                    ->select(DB::raw('COUNT(created_at) count, MONTH(created_at) month '))
                    ->whereNotNull('created_at')->groupBy('month')->get();

            $month = config('month');

            $data = $risks->mapWithKeys(function($risk) use($month){
                return [$month[$risk->month - 1] => $risk->count];
            });

            return response()->json([
                'message' => 'success',
                'data' => $data
            ], 200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'error_description' => $e->getMessage()
            ], 400);

        }
    }

    public function risk(Request $request){

        try{

            $query = DB::table('trees')->whereYear('created_at', $request->year ?: date('Y'))->get();

            $lowRisk = $query->where('risk', 'Rendah')->count();
            $mediumRisk = $query->where('risk', 'Sederhana')->count();
            $highRisk = $query->where('risk', 'Tinggi')->count();

            $data = new \stdClass;
            $data->rendah = $lowRisk;
            $data->sederhana = $mediumRisk;
            $data->tinggi = $highRisk;

            return response()->json([
                'message' => 'success',
                'data' => $data
            ], 200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'Error Message' => $e->getMessage()
            ], 400);

        }
    }

    public function count(){

        try{

            $data = DB::table('trees')->count();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ], 200);

        }catch(Exception $e){

            return response()->json([
                'message' => 'error',
                'data' => $data,
                'Error Message' => $e->getMessage()
            ], 400);

        }
    }

    public function sumPerzone(Request $request){

        try{

            /* QUERY TREE DATA IN STORED RECORD */
            $data = Tree::select('zone')
                ->when($request->year, function($query) use($request){
                    $query->whereYear('created_at', $request->year);
                })
                ->when($request->month, function($query) use($request){
                    $query->whereMonth('created_at', $request->month);
                })->get()->groupBy('zone')->map->count();

            /* PLACE QUERY RESULT TO OBJECT */
            $items = [];
            $sum = 0;
            foreach($data as $key => $val){
                $newItem = new stdClass;
                $newItem->zone = (string)$key;
                $newItem->totalCount = $val;
                $items[] = $newItem;
                $sum += $val;
            }

            /* RETURN YEAR / MONTH INSTEAD OF NUMBER */
            $months = config('month');
            $month = $request->month ? $months[$request->month - 1] : 'All';
            $request->merge(['month' => $month, 'year' => $request->year ?: 'All']);

            return response()->json([
                'message' => 'success',
                'data' => $items,
                'year' => $request->year,
                'month' => $request->month,
                'total' => $sum
            ], 200);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'Error Message' => $ex->getMessage()
            ], 400);

        }
    }

    public function sumPermonth(Request $request, $zone){

        try{

            $query = DB::table('trees')->selectRaw("COUNT(*) as totalTree, MONTH(trees.created_at) as month")
                    ->when($request->year, function($q) use($request){
                        return $q->whereYear('created_at', $request->year);
                    })
                    ->where('zone', $zone)
                    ->groupBy('month')
                    ->get();

            $queryCount = [];
            $queryArr = [];
            $sum = $query->sum('totalTree');

            $pluck = $query->pluck('totalTree', 'month');

            foreach ($pluck as $key => $value) {
                $queryCount[(int)$key] = $value;
            }

            for($i = 1; $i <= 12; $i++){
                if(!empty($queryCount[$i])){
                    $queryArr[$i] = $queryCount[$i];
                }else{
                    $queryArr[$i] = 0;
                }
            }

            $month = config('month');
            $data = [];
            foreach($queryArr as $key => $val){
                $response = new stdClass;
                $response->month = $month[$key - 1];
                $response->count = $val;
                array_push($data, $response);
            }

            return response()->json([
                'message' => 'success',
                'zone' => $zone,
                'total' => $sum,
                'data' => $data
            ], 200);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'error',
                'data' => 'No Data',
                'Error Message' => $ex->getMessage()
            ], 400);

        }
    }
}
