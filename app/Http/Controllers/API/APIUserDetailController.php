<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class APIUserDetailController extends Controller
{
    public function __invoke(Request $request)
    {
        $count = new \stdClass;
        $count->kejur = DB::table('log_kejurs')->whereUsername($request->username)->count();
        $count->tree = DB::table('maintances')->whereUsername($request->username)->count();

        return response()->json([
            'message' => 'Success',
            'maintainance_count' => $count
        ]);
    }
}
