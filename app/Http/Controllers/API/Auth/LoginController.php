<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notification;
use App\UserDetail;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'username'  => 'required',
            'password'  => 'required',
        ]);

        if( filter_var($request->username, FILTER_VALIDATE_EMAIL) ) {

            $user = user::where('email', $request->username)
                                ->first();
        }
        else
        {

            $user = User::where('ic_number', $request->username)
                                ->first();
        }

        if( ! $user instanceof User ) {
            return response()->json([
                'message' => 'User Not Found.'
            ], 422);
        }

        if( ! Hash::check($request->password, $user->password) ) {
            return response()->json([
                'message' => 'Incorrect Password.'
            ], 422);
        }

        if( $token = Auth::guard('api')->login($user) ) {

            $input['user_id'] = $user->user_id;
            $input['onesignal_id'] = $request->onesignal_id;

            $userDetail = UserDetail::whereIcNumber($user->ic_number)->first();

            Notification::create($input);

            return response()->json([
                'access_token' => $token,
                'user' => $user,
                'user_detail' => $userDetail
            ], 200);
        }

        return response()->json([
            'message' => 'Unauthorized.'
        ], 401);

    }
}
