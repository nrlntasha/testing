<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notification;
use App\User;
use Exception;

class LogoutController extends Controller
{
    public function __invoke(Request $request)
    {
        try{

            $user_id = User::whereUsername($request->username)->first()->user_id;
            $onesignal_id = $request->onesignal_id;

            Notification::where([
                'user_id'        => $user_id,
                'onesignal_id'   => $onesignal_id
            ])->delete();

            return response()->json([
                'Status' => 'Success',
                'Message' => 'Successfully logged out!'
            ], 200);

        }catch(Exception $ex){

            return response()->json([
                'Status' => 'Error',
                'Message' => $ex->getMessage()
            ], 400);

        }
    }
}
