<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'username'   => 'required',
            'email'      => 'required',
            'password'   => 'required',
            'ic_number'  => 'required'
        ]);

        $user = User::where('username', $request->username)
                      ->orWhere('email', $request->email)
                      ->orWhere('ic_number', $request->ic_number)
                      ->first();

        if ( $user instanceof User ) {
            return response()->json([
                'message' => 'User already exists.'
            ], 422);
        }

        $request->merge([
            'password' => bcrypt($request->password),
            'user_id' => uniqid()
            ]);

        $user = User::create($request->all());

        if( $token = Auth::guard('api')->login($user) ) {
            return response()->json([
                // 'access_token' => $token,
                'user' => $user
            ], 200);
        }


        return response()->json([
            'message' => 'Unauthorized.'
        ], 401);
    }
}
