<?php

namespace App\Http\Controllers;

use App\TreeSpecies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AutocompleteController extends Controller
{
    public function nama_saintifik(Request $request){

        $data = TreeSpecies::where("scientific_name", "LIKE", "%{$request->get("term")}%")->groupBy("scientific_name")->pluck('scientific_name');

        return response()->json($data);
    }

    public function nama_biasa(Request $request){

        $data = TreeSpecies::where("local_name", "LIKE", "%{$request->get("term")}%")->groupBy("local_name")->pluck('local_name');

        return response()->json($data);
    }

    public function inventory_no(Request $request){
        $data = DB::table("trees")
                    ->select("inventory_no")
                    ->where("inventory_no", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("inventory_no")
                    ->get();

        $datas = array();

        foreach($data as $name){
            $datas[] = $name->inventory_no;
        }

        return response()->json($datas);
    }

    public function lokasi(Request $request){
        $data = DB::table("trees")
                    ->selectRaw("location, latitude, longitude")
                    ->where("location", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("location")
                    ->get();

        return $data->toArray();
    }

    public function zon(Request $request){
        $data = DB::table("trees")
                    ->select("zone")
                    ->where("zone", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("zone")
                    ->get();

        $datas = array();

        foreach($data as $name){
            $datas[] = $name->zone;
        }

        return response()->json($datas);
    }

    public function kejur_zon(Request $request){

        $data = DB::table("kejurs")
                    ->where("zon", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("zon")
                    ->pluck('zon');

        return response()->json($data);
    }

    public function kejur_lokasi(Request $request){
        $data = DB::table("kejurs")
                    ->select("location")
                    ->where("location", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("location")
                    ->get();

        $datas = array();

        foreach($data as $name){
            $datas[] = $name->location;
        }

        return response()->json($datas);

    }

    public function kejur_inventory(Request $request){
        $data = DB::table("kejurs")
                    ->select("inventory_no")
                    ->where("inventory_no", "LIKE", "%{$request->get("term")}%")
                    ->groupBy("inventory_no")
                    ->get();

        $datas = array();

        foreach($data as $name){
            $datas[] = $name->inventory_no;
        }

        return response()->json($datas);

    }

    public function autoFill(Request $request){
        $data = TreeSpecies::where("local_name", "LIKE", "%{$request->get('query')}%")->first();

        return response()->json($data);
    }

}
