<?php

namespace App\Http\Controllers;

use App\Tree;
use Illuminate\Http\Request;

class DataTablesController extends Controller
{
    public function treeListDataMaintance(Request $request){
        $columns = array(
            0 => 'treeID',
            1 => 'local_name',
            2 => 'saintifik_name',
            3 => 'location',
            4 => 'zone',
            5 => 'inventory_no'
        );
        $totalData = Tree::count();
        // $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $zone = isset($request->zone) ? $request->zone : "0";
        $location = isset($request->location) ? $request->location : "0";
        $name = isset($request->name) ? $request->name : "0";
        $risk = isset($request->risk) ? $request->risk : "0";
        $category = isset($request->category) ? $request->category : "0";
        $silara = isset($request->silara) ? $request->silara : "0";

        if(empty($request->input('search.value'))){

            $trees = Tree::offset($start)->limit($limit)->orderBy($order, $dir)
                        ->when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('local_name', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })->get();

            $totalFiltered = Tree::when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('local_name', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })->count();

        }else{

            $search = $request->input('search.value');

            $trees = Tree::where(function($q) use($search){
                        $q->where('treeID', 'LIKE', "%{$search}%")
                        ->orWhere('local_name', 'LIKE', "%{$search}%")
                        ->orWhere('saintifik_name', 'LIKE', "%{$search}%")
                        ->orWhere('location', 'LIKE', "%{$search}%")
                        ->orWhere('zone', 'LIKE', "%{$search}%");
                        })
                        ->when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('local_name', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })
                        ->offset($start)
                        ->limit($limit)->get();

            $totalFiltered = Tree::where(function($q) use($search){
                            $q->where('treeID', 'LIKE', "%{$search}%")
                            ->orWhere('local_name', 'LIKE', "%{$search}%")
                            ->orWhere('saintifik_name', 'LIKE', "%{$search}%")
                            ->orWhere('location', 'LIKE', "%{$search}%")
                            ->orWhere('zone', 'LIKE', "%{$search}%");
                            })
                            ->when($zone != "0", function($q) use($zone){
                                $q->where('zone', $zone);
                            })
                            ->when($location != "0", function($q) use($location){
                                $q->where('location', $location);
                            })
                            ->when($name != "0", function($q) use($name){
                                $q->where('local_name', $name);
                            })
                            ->when($risk != "0", function($q) use($risk){
                                $q->whereIn('risk', $risk);
                            })
                            ->when($category != "0", function($q) use($category){
                                $q->whereIn('category', $category);
                            })
                            ->when($silara != "0", function($q) use($silara){
                                $q->whereIn('silara', $silara);
                            })
                            ->count();

        }

        $data = array();
        if(!empty($trees)){
            foreach($trees as $tree ){
                // $nestedData['treeID'] = $tree->treeID;
                $nestedData['local_name'] = $tree->local_name;
                $nestedData['saintifik_name'] = $tree->saintifik_name;
                $nestedData['location'] = $tree->location;
                $nestedData['zone'] = $tree->zone;
                $nestedData['inventory_data'] = $tree->inventory_no;
                // $nestedData['action'] = '<a href="maintance-pokok/'.$tree->treeID.'" class="btn btn-outline-success waves-effect waves-light"><i class="fas fa-cogs"></i></a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}
