<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use App\SystemLogs;

class DateSessionController extends Controller
{
    public function index(Request $request,$id){

        $start_today = date('Y-m-d 00:00:00');
        $end_today = date('Y-m-d 23:58:59');

        $start_yesterday = date('Y-m-d 00:00:00', strtotime('-1 days'));
        $end_yesterday = date('Y-m-d 23:59:59', strtotime('-1 days'));

        $start_7days = date('Y-m-d 00:00:00', strtotime('-7 days'));
        $end_7days = date('Y-m-d 23:59:59');

        $start_lastmonth = date('Y-m-01 00:00:00', strtotime('-1 month'));
        $end_lastmonth = date('Y-m-t 23:59:59', strtotime('-1 month'));

        $custom = explode(' - ', "12/05/2020 - 19/06/2020");
        $start_customMonth = date_format(date_create($custom[0]),'Y-m-d 00:00:00');
        $end_customMonth = date_format(date_create(str_replace('/', '-', $custom[1])),'Y-m-d 23:58:59');

        if($id=="today"){
            SESSION::put('start_date', $start_today);
            SESSION::put('end_date', $end_today);
            SESSION::put('days', 'Harian');
        }else if($id=="yesterday"){
            SESSION::put('start_date', $start_yesterday);
            SESSION::put('end_date', $end_yesterday);
            SESSION::put('days', 'Kelmarin');
        }else if($id=="7days"){
            SESSION::put('start_date', $start_7days);
            SESSION::put('end_date', $end_7days);
            SESSION::put('days', 'Mingguan');
        }else if($id=="lastmonth"){
            SESSION::put('start_date', $start_lastmonth);
            SESSION::put('end_date', $end_lastmonth);
            SESSION::put('days', 'Bulanan');
        }else if($id=="custom"){
            SESSION::put('start_date', $start_customMonth);
            SESSION::put('end_date', $end_customMonth);
            SESSION::put('days', 'Custom');
        }

        SystemLogs::create([
            'username' => session::get('username'),
            'comments' => "Tarikh telah diubah. Daripada " . session::get('start_date') . " hingga " . session::get('end_date'),
            'category' => 'Tarikh'
        ]);

        return redirect()->back();

    }

    public function customDate(Request $request){

        $parseStart = $request->dateStart;
        $parseEnd = $request->dateEnd;

        $dateStart = Carbon::parse($parseStart)->format('Y-m-d 00:00:00');
        $dateEnd = Carbon::parse($parseEnd)->format('Y-m-d 23:59:59');

        SESSION::put('start_date', $dateStart);
        SESSION::put('end_date', $dateEnd);
        SESSION::put('days', 'Custom');

        SystemLogs::create([
            'username' => session::get('username'),
            'comments' => "Tarikh telah diubah. Daripada " . session::get('start_date') . " hingga " . session::get('end_date'),
            'category' => 'Tarikh'
        ]);

        $data = ["status" => "success"];

        return response()->json($data);
    }
}
