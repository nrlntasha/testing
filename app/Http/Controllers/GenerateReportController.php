<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Registries\ReporterRegistry;

class GenerateReportController extends Controller
{
    protected $registry;

    public function __construct(ReporterRegistry $registry) {
        $this->registry = $registry;
    }

    public function report(Request $request) {
        $reportType = $request->type; // kejur, lembut

        if(!$this->registry->has($reportType)) return abort(404);

        $reporter = $this->registry->get($reportType);

        return $reporter->handle($request->all());
    }

    public function test($id){

        $kejur = DB::table('kejurs')
                    ->where('kejurID', $id)
                    ->first();

        $log_kejurs = DB::table('log_kejurs')
                        ->where('log_kejurs.kejurID', $id)
                        ->get();

        // $kejur_media = DB::table('kejur_media')
        //                 ->where('kejurID', $id)
        //                 ->orderBy('nama_alatan')
        //                 ->get();

        $collection = DB::table('kejur_media')
                            ->where('kejurID', $id)
                            ->get();

        $kejur_medias = $collection->groupBy('nama_alatan');

        // return view('superadmin.laporan.kejur.full', compact('kejur', 'log_kejurs', 'kejur_medias'));

        $pdf = \PDF::loadView('superadmin.laporan.kejur.full', compact('kejur', 'log_kejurs', 'kejur_medias'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        // $pdf->setOption('enable-smart-shrinking', true);
        // $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->download('test.pdf');

    }
}
