<?php

namespace App\Http\Controllers;

use Str;
use File;
use Illuminate\Http\Request;
use App\Services\KejurDashboard;
use Illuminate\Support\Facades\DB;
use App\Services\Registries\KejurReportTemplateRegistry;
use Illuminate\Support\Carbon;
use App\Kejur;
use App\LogKejur;
use Illuminate\Support\Facades\Session;

class KejurController extends Controller
{
    protected $kejurDashboard, $templateRegistry, $created_at;

    public function __construct(KejurDashboard $kejurDashboard, KejurReportTemplateRegistry $templateRegistry){
        $this->kejurDashboard   = $kejurDashboard;
        $this->templateRegistry = $templateRegistry;
    }

    public function created_at(){
        return $created_at = date('Y-m-d H:i:s');
    }

    public function index(){
        try{

            $kejurs = DB::table('kejurs')
                        ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                        ->get();

            $kejurss = DB::table('kejurs')
                        ->get();


            $bilAmbilalih = $this->kejurDashboard->bilAmbilalih('Telah Diambilalih');
            $bilBelumAmbilalih = $this->kejurDashboard->bilAmbilalih('Belum Diambilalih');
            $bilTidakAmbilalih = $this->kejurDashboard->bilAmbilalih('Tidak Akan Diambilalih');
            $bilLengkapKemudahan = $this->kejurDashboard->bilLengkapKemudahan();
            $bilTidakKemudahan = $this->kejurDashboard->bilTidakKemudahan();
            $bilPerwartaan = $this->kejurDashboard->bilPerwartaan();
            $bilTidakPerwartaan = $this->kejurDashboard->bilTidakPerwartaan();

            return view('superadmin.landskap.kejur.index',
                    compact(
                        'kejurs',
                        'kejurss',
                        'bilAmbilalih',
                        'bilBelumAmbilalih',
                        'bilTidakAmbilalih',
                        'bilLengkapKemudahan',
                        'bilTidakKemudahan',
                        'bilPerwartaan',
                        'bilTidakPerwartaan'
                    ));

        }catch(Exception $e){

        }
    }

    public function new(){
        try{
            return view('superadmin.landskap.kejur.new');
        }catch(Exception $e){

        }
    }

    public function store(Request $request){

        try{

            $kejurID = Kejur::orderBy('kejurID', 'desc')->first();

            if(is_null($kejurID)){
                $kejurIDs = 'KJ0000000';
            }else{
                $kejurID = Str::substr($kejurID -> kejurID, 2);
                $kejurIDs = sprintf('KJ%07d',$kejurID + 1);
            }

            $input = $request->all();
            $input['kejurID'] = $kejurIDs;

            if(is_null($input['no_perwartaan'])){
                unset($input['no_perwartaan']);
            }

            Kejur::create($input);

            if($request->session()->get('role')=='Superadmin'){
                return redirect('/superadmin/kejur/gambar/'.$kejurIDs);
            }else if($request->session()->get('role')=='Kejur'){
                return redirect('/kejur/kejur/gambar/'.$kejurIDs);
            }else{
                return redirect('/logout');
            }


            // if($request->hasfile('gambar_kejur')){

            //     //dd($request->file('gambar_pokok'));
            //     foreach($request->file('gambar_kejur') as $file){

            //         $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            //         $file->move(public_path().'/kejur/', $imgName);
            //         //$data[$file] = $name;

            //         DB::table('media')
            //             ->insert([
            //                 'treeID' => $kejurIDs,
            //                 'image_name' => $imgName,
            //                 'img_ext' => $file->getClientOriginalExtension(),
            //                 'tags' => 'Kejur'
            //             ]);
            //     }

            // }

            // return redirect('/superadmin/kejur')->with('success', 'Data Kejur Disimpan');

        }catch(Exception $e){
            return back()->with(['error', 'Intenal Server Error', 'message' => $e]);
            // dd($e);
        }
    }

    public function detail($id){

        try{

            $kejur = Kejur::find($id);

            $log_kejurs = DB::table('log_kejurs')
                            ->where('log_kejurs.kejurID', $id)
                            ->get();

            // $kejur_media = DB::table('kejur_media')
            //                 ->where('kejurID', $id)
            //                 ->orderBy('nama_alatan')
            //                 ->get();

            $collection = DB::table('kejur_media')
                                ->where('kejurID', $id)
                                ->get();

            $kejur_medias = $collection->groupBy('nama_alatan');

            return view('superadmin.landskap.kejur.detail', compact('kejur', 'log_kejurs', 'kejur_medias'));

        }catch(Exception $e){

            dd($e);

        }

    }

    public function detail2($id){

        try{
            $kejur = Kejur::find($id);

            return view('superadmin.landskap.kejur.update', compact('kejur'));
        }catch(Exception $e){

        }

    }

    public function update(Request $request, $kejurID){

        try{

            DB::table('kejurs')
                ->where('kejurID', $kejurID)
                ->update([
                    'zone' => $request -> zone,
                    'location' => $request -> location,
                    'inventory_no' => $request -> inventory_no,
                    'maintance_status' => $request -> maintance_status,
                    'value' => $request -> value,
                    'notes' => $request -> notes,
                    'area' => $request -> area,
                    'area_type' => $request -> area_type,
                    'intake_status' => $request -> intake_status,
                    'intake_date' => $request -> intake_date
                ]);

            if($request->session()->get('role')=='Superadmin'){
                return redirect('/superadmin/kejur')->with('success', 'Data Dikemaskini');
            }else if($request->session()->get('role')=='Kejur'){
                return redirect('/kejur/kejur')->with('success', 'Data Dikemaskini');
            }else{
                return redirect('/logout');
            }

        }catch(Exception $e){
            return back()->with('error', 'Internal Server Error');
        }

    }

    public function saveUpdate(Request $request, $id){

        try{

            DB::table('kejurs')
                ->where('KejurID', $id)
                ->update([
                    'location' => $request->location,
                    'no_inventori' => $request->inventory_no,
                    'keluasan' => $request->acres,
                    'maintance_status' => $request->maintance_status,
                    'intake_date' => $request->intake_date,
                    'catatan' => $request->notes,
                    'updated_at' => Carbon::now()
                ]);

            if($request->session()->get('role')=='Superadmin'){
                return redirect('/superadmin/kejur/detail/'.$id)->with('success', 'Data Dikemaskini');
            }else if($request->session()->get('role')=='Kejur'){
                return redirect('/kejur/kejur/detail/'.$id)->with('success', 'Data Dikemaskini');
            }else{

            }

        }catch(Exception $e){
            dd($e);
        }

    }

    public function destroy(Request $request, $kejurID){

        try{

            $medias = DB::table('kejur_media')
                        ->where('kejurID', $kejurID)
                        ->get();

            foreach ($medias as $media) {
                File::delete('kejur/'.$media -> image_name);
            }

            DB::table('kejurs')
                ->where('kejurID', $kejurID)
                ->delete();


            DB::table('kejur_media')
                ->where('kejurID', $kejurID)
                ->delete();

            if($request->session()->get('role')=='Superadmin'){
                return redirect('/superadmin/kejur')->with('success', 'Data Dipadam');
            }else if($request->session()->get('role')=='Kejur'){
                return redirect('/kejur/kejur')->with('success', 'Data Dipadam');
            }else{

            }

        }catch(Exception $e){
            return back()->withErrors('error', 'Internal Data Error');
        }

    }

    public function gambar($id){

        $kejur = Kejur::find($id);

        $log_kejurs = DB::table('log_kejurs')
                        ->where('log_kejurs.kejurID', $id)
                        ->get();

        // $kejur_media = DB::table('kejur_media')
        //                 ->where('kejurID', $id)
        //                 ->orderBy('nama_alatan')
        //                 ->get();

        $collection = DB::table('kejur_media')
                            ->where('kejurID', $id)
                            ->get();

        $kejur_medias = $collection->groupBy('nama_alatan');

        $properties = config('AlatanLandskapKejur');

        // dd($kejur_medias);

        // foreach ($kejur_medias as $key=>$medias) {
        //     echo $key;
        //     foreach ($medias as $media) {
        //         echo $media->image_name;
        //     }
        // }

        return view('superadmin.landskap.kejur.gambar', compact('kejur', 'log_kejurs', 'kejur_medias', 'properties'));
    }

    public function upload(Request $request, $id){

        try{

            if($request->hasfile('gambar_alatan')){

                //dd($request->file('gambar_pokok'));
                foreach($request->file('gambar_alatan') as $file){

                    $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
                    $file->move(public_path().'/kejur/', $imgName);
                    //$data[$file] = $name;

                    DB::table('kejur_media')
                        ->insert([
                            'kejurID' => $id,
                            'nama_alatan' => $request -> nama_alatan,
                            'image_name' => $imgName,
                            'img_ext' => $file->getClientOriginalExtension(),
                            'tags' => 'Kejur'
                        ]);
                }

                $input = $request->all();
                $input['kejurID'] = $id;
                $input['username'] = Session::get('username');

                LogKejur::create($input);

            }

            return redirect()->back()->with('success', 'Gambar Disimpan');

        }catch(Exception $e){
            // return redirect()->back()->withErrors('Internal Server Error');
            dd($e);
        }

    }

    public function deleteGambar(Request $request, $id){

            // dd($request->query('id'));
        try{

            $medias = DB::table('kejur_media')
                        ->where('kejurID', $id)
                        ->where('nama_alatan', $request->query('id'))
                        ->get();

            // dd($medias);

            foreach ($medias as $media) {
                File::delete('kejur/'.$media->image_name);
            }

            DB::table('kejur_media')
                ->where('kejurID', $id)
                ->where('nama_alatan', $request->query('id'))
                ->delete();

            DB::table('log_kejurs')
                ->where('kejurID', $id)
                ->where('nama_alatan', $request->query('id'))
                ->delete();

            return redirect()->back()->with('success', 'Gambar Dipadam');

        }catch(Exception $e){

            dd($e);

        }

    }

    public function fulldetail($id){

        try{

            $kejur = Kejur::find($id);
            $kejur->status = $kejur->kejurStatus()->pluck('status')->first();

            $log_kejurs = LogKejur::where('log_kejurs.kejurID', $id)
                        ->get();

            $collection = DB::table('kejur_media')
                                ->where('kejurID', $id)
                                ->get();

            $kejur_medias = $collection->groupBy('nama_alatan');

            return view('superadmin.landskap.kejur.full', compact('kejur', 'log_kejurs', 'kejur_medias'));

        }catch(Exception $e){

            dd($e);

        }

    }

    public function deleteAll($id){

        try{

            $medias = DB::table('kejur_media')
                        ->where('kejurID', $id)
                        ->get();

            foreach ($medias as $media) {
                File::delete('kejur/'.$media -> image_name);
            }

            DB::table('kejur_media')
                ->where('kejurID', $id)
                ->delete();

            DB::table('log_kejurs')
                ->where('kejurID', $id)
                ->delete();

            DB::table('kejurs')
                ->where('kejurID', $id)
                ->delete();

            return redirect()->back()->with('success', 'Data Berjaya Dipadam');

        }catch(Exception $e){

            dd($e);

        }

    }

    public function export($id){

        $detail = DB::table('kejurs')->where('kejurID', $id)->first();
        $items = DB::table('log_kejurs')->where('kejurID', $id)->get();
        $kejurProps = config('AlatanLandskapKejur');
        $iteration = 1;

        return view('superadmin.export.kejurProperties', compact('detail', 'items', 'kejurProps', 'iteration'));
    }

}
