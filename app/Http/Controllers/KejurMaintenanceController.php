<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\KejurMaintenance;

class KejurMaintenanceController extends Controller
{
    protected $kejurMaintenance;

    public function __construct(KejurMaintenance $kejurMaintenance)
    {
        $this->kejurMaintenance = $kejurMaintenance;
    }

    public function index(){

        $kejurBaik          = $this->kejurMaintenance->kejurBaik();
        $kejurNaikTaraf     = $this->kejurMaintenance->kejurNaikTaraf();
        $kejurSegera        = $this->kejurMaintenance->kejurSegera();
        $kejurSelenggara    = $this->kejurMaintenance->kejurSelenggara();

        // dd($kejurBaik, $kejurNaikTaraf, $kejurSegera, $kejurSelenggara);

        return view('superadmin.landskap.kejur.report', 
                compact(
                    'kejurBaik', 
                    'kejurNaikTaraf', 
                    'kejurSegera', 
                    'kejurSelenggara'
                ));

    }
}
