<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\KejurDashboard;

class KejurReportController extends Controller
{
    protected $kejurDashboard;

    public function __construct(KejurDashboard $kejurDashboard)
    {
        $this->kejurDashboard = $kejurDashboard;
    }

    public function index(){

        /* CHART STATUS AMBIL ALIH */
        $bilAmbilalih = $this->kejurDashboard->bilAmbilalih('Telah Diambilalih');
        $bilBelumAmbilalih = $this->kejurDashboard->bilAmbilalih('Belum Diambilalih');
        $bilTidakAmbilalih = $this->kejurDashboard->bilAmbilalih('Tidak Akan Diambilalih');

        /* */
        $bilLengkapKemudahan = $this->kejurDashboard->bilLengkapKemudahan();
        $bilTidakKemudahan = $this->kejurDashboard->bilTidakKemudahan();
        $bilPerwartaan = $this->kejurDashboard->bilPerwartaan();
        $bilTidakPerwartaan = $this->kejurDashboard->bilTidakPerwartaan();
        $bilKawasanLapang = $this->kejurDashboard->bilKawasanLapang();
        $bilBukanLapang = $this->kejurDashboard->bilBukanLapang();
        $bilTaman = $this->kejurDashboard->bilTaman();
        $bilBukanTaman = $this->kejurDashboard->bilBukanTaman();
        $bilPenampan = $this->kejurDashboard->bilPenampan();
        $bilBukanPenampan = $this->kejurDashboard->bilBukanPenampan();
        $bilPadangBola = $this->kejurDashboard->bilPadangBola();
        $bilBukanPadangBola = $this->kejurDashboard->bilBukanPadangBola();
        $bilDewan = $this->kejurDashboard->bilDewan();
        $bilBukanDewan = $this->kejurDashboard->bilBukanDewan();

        return view('superadmin.laporan.kejur.full',
                compact (
                    'bilAmbilalih',
                    'bilBelumAmbilalih',
                    'bilTidakAmbilalih',
                    'bilLengkapKemudahan',
                    'bilTidakKemudahan',
                    'bilPerwartaan',
                    'bilTidakPerwartaan',
                    'bilKawasanLapang',
                    'bilBukanLapang',
                    'bilTaman',
                    'bilBukanTaman',
                    'bilPenampan',
                    'bilBukanPenampan',
                    'bilPadangBola',
                    'bilBukanPadangBola',
                    'bilDewan',
                    'bilBukanDewan'
                ));

    }
}
