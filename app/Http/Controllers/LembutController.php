<?php

namespace App\Http\Controllers;

use App\Tree;
use App\Services\TreeList;
use App\Services\TreeReportData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\CustomReportData;
use App\DataTables\TreeDataTable;
use App\TreeCategory;
use App\TreeRisk;
use App\TreeSilara;
use Barryvdh\DomPDF\Facade as PDF;

class LembutController extends Controller
{

    protected $treeList;
    protected $treeReportData;

    public function __construct(TreeList $treeList, TreeReportData $treeReportData)
    {
        $this->treeList = $treeList;
        $this->treeReportData = $treeReportData;
    }

    public function index(CustomReportData $customData){

        /* GET TREE BASED ON SELECTED DATE */
        $trees = Tree::getTreeDetail()->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])->get();

        /* MAINTAINANCE LINE CHART */
        $tinggiReport = $customData->tinggiThreeMonth();
        $sederhanaReport = $customData->sederhanaThreeMonth();
        $rendahReport = $customData->rendahThreeMonth();
        $chartData = $customData->getMonthRange();

        /* TREE TYPE PIE CHART */
        $bilPokokRenek = $this->treeList->bilPokokRenek();
        $bilPokokTeduhan = $this->treeList->bilPokokTeduhan();
        $bilPokokPalma = $this->treeList->bilPokokPalma();

        /* CONFIG FOR FILTER OPTION */
        $treeTypeCheckbox = TreeSilara::all();
        $treeCategoryCheckbox = TreeCategory::all();
        $treeRiskCheckbox = TreeRisk::all();

        /* POPULATE ZONE DROPDOWN FILTER */
        $zoneLists = $this->treeReportData->zoneList();

        return view('superadmin.landskap.lembut.index',
                compact(
                    'trees',
                    'tinggiReport',
                    'sederhanaReport',
                    'rendahReport',
                    'bilPokokRenek',
                    'bilPokokTeduhan',
                    'bilPokokPalma',
                    'zoneLists',
                    'chartData',
                    'treeTypeCheckbox',
                    'treeCategoryCheckbox',
                    'treeRiskCheckbox'
                ));
    }

    public function customData(CustomReportData $customData){

        echo $customData->tinggiThreeMonth();
        echo $customData->sederhanaThreeMonth();
        echo $customData->rendahThreeMonth();
    }

    public function treeListData(Request $request){

        $columns = array(
            0 => 'treeID',
            1 => 'local_name',
            2 => 'saintifik_name',
            3 => 'location',
            4 => 'zone',
            5 => 'inventory_no'
        );

        $totalData = Tree::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $zone = isset($request->zone) ? $request->zone : "0";
        $location = isset($request->location) ? $request->location : "0";
        $name = isset($request->name) ? $request->name : "0";
        $risk = isset($request->risk) ? $request->risk : "0";
        $category = isset($request->category) ? $request->category : "0";
        $silara = isset($request->silera) ? $request->silera : "0";

        if(empty($request->input('search.value'))){

            $trees = Tree::offset($start)->limit($limit)
                        ->when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('species_id', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })
                        ->with('species', 'getCategory', 'getRisk', 'getSilara')
                        ->orderBy('created_at', 'desc')
                        ->get();

            $totalFiltered = Tree::when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('species_id', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })->count();

        }else{

            $search = $request->input('search.value');

            $trees = Tree::join('tree_species', 'species_id', 'id')->where(function($q) use($search){
                        $q->where('treeID', 'LIKE', "%{$search}%")
                        ->orWhere('tree_species.local_name', 'LIKE', "%{$search}%")
                        ->orWhere('location', 'LIKE', "%{$search}%")
                        ->orWhere('zone', 'LIKE', "%{$search}%");
                        })
                        ->when($zone != "0", function($q) use($zone){
                            $q->where('zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('species_id', $name);
                        })
                        ->when($risk != "0", function($q) use($risk){
                            $q->whereIn('risk', $risk);
                        })
                        ->when($category != "0", function($q) use($category){
                            $q->whereIn('category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('silara', $silara);
                        })
                        ->with('getRisk', 'getCategory', 'getRisk', 'getSilara')
                        ->offset($start)
                        ->limit($limit)->get();

            $totalFiltered = Tree::join('tree_species', 'species_id', 'id')
                                ->where(function($q) use($search){
                                    $q->where('treeID', 'LIKE', "%{$search}%")
                                    ->orWhere('tree_species.local_name', 'LIKE', "%{$search}%")
                                    ->orWhere('location', 'LIKE', "%{$search}%")
                                    ->orWhere('zone', 'LIKE', "%{$search}%");
                                })
                                ->when($zone != "0", function($q) use($zone){
                                    $q->where('zone', $zone);
                                })
                                ->when($location != "0", function($q) use($location){
                                    $q->where('location', $location);
                                })
                                ->when($name != "0", function($q) use($name){
                                    $q->where('species_id', $name);
                                })
                                ->when($risk != "0", function($q) use($risk){
                                    $q->whereIn('risk', $risk);
                                })
                                ->when($category != "0", function($q) use($category){
                                    $q->whereIn('category', $category);
                                })
                                ->when($silara != "0", function($q) use($silara){
                                    $q->whereIn('silara', $silara);
                                })
                                ->count();

        }

        $data = array();
        if(!empty($trees)){
            foreach($trees as $tree ){
                $nestedData['local_name'] = $tree->species->local_name;
                $nestedData['saintifik_name'] = $tree->species->scientific_name;
                $nestedData['location'] = $tree->location;
                $nestedData['zone'] = $tree->zone;
                $nestedData['category'] = $tree->getCategory->category;
                $nestedData['silara'] = $tree->getSilara->name;
                $nestedData['risk'] = $tree->getRisk->name;
                $nestedData['inventory_data'] = $tree->inventory_no;
                $nestedData['action'] = '<a href="detail/'.$tree->treeID.'" class="btn btn-outline-success waves-effect waves-light" data-toggle="tooltip" title="Papar Maklumat"><i class="fas fa-eye"></i></a>
                <a href="detail-pokok/'.$tree->treeID.'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Kemaskini"®><i class="fas fa-edit"></i></a>
                <a href="maintance-pokok/'.$tree->treeID.'" class="btn btn-outline-warning waves-effect waves-light" data-toggle="tooltip" title="Selenggara"><i class="fas fa-cogs"></i></a>
                <a href="delete-pokok/'.$tree->treeID.'" class="btn btn-outline-danger waves-effect waves-light" data-toggle="tooltip" title="Padam"><i class="fas fa-trash"></i></a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function exportPDF(Request $request){

        $data = $this->treeReportData->export($request);
        return view('superadmin.export.softScapeMaintainance', compact('data'));
    }

}
