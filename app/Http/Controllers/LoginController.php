<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\SystemLogs;

class LoginController extends Controller
{
    public function index(){
        return view('login.index');
    }

    public function check_login(Request $request){

        if(is_null($request->old_ic)){
            $attempt = 0;
        }

        $request -> validate([
            'ic_number' => 'required',
            'password' => 'required'
        ]);

        $checkLogin = User::where('ic_number', $request->ic_number)
                            ->first();

        if(!$checkLogin instanceof User){
            return redirect()->back()->withInput()->withErrors('Kad Pengenalan tidak didaftarkan pada sistem.');
        }else{
            if($request->ic_number == Session::get('ic_number')){
                session::put('ic_number', $request->ic_number);
            }else{
                session::put('ic_number', $request->ic_number);
                Session::put('attempt', 0);
            }
        }

        if(!Hash::check($request->password, $checkLogin -> password)){
            $attempt = session::get('attempt');
            $attempt++;
            Session::put('attempt', $attempt);
            if($attempt > 5){

                SystemLogs::create([
                    'username'  => $checkLogin->username,
                    'comments'   => 'Akaun pengguna telah dibekukan kerana memberikan katalaluan yang salah melebihi 5 kali',
                    'category'  => 'Log Masuk'
                ]);

                DB::table('users')->where('ic_number', Session::get('ic_number'))->update(['is_suspended' => 1]);
                return redirect()->back()->withInput()->withErrors('Akaun anda telah dibekukan kerana memberikan katalaluan yang salah melebihi 5 kali.');
            }
            return redirect()->back()->withInput()->withErrors('Katalaluan atau Kad Pengenalan salah.');
        }

        $request->session()->put([
            'user_id'  => $checkLogin->user_id,
            'username' => $checkLogin->username,
            'role'     => $checkLogin->role
        ]);

        $this->log_system($checkLogin -> username);

        if($checkLogin -> role == 'Superadmin' || $checkLogin -> role == 'Admin'){

            $this->date_session($request);

            return redirect('/superadmin');

        }else if($checkLogin -> role == 'Landskap'){

            $this->log_system($checkLogin->username);

            return redirect('/landskap');

        }else if($checkLogin->role == 'Kejur'){

            $this->log_system($checkLogin->username);

            return redirect('/kejur');

        }else if($checkLogin->role == 'Intern'){

            $this->log_system($checkLogin->username);

            return redirect('/intern');

        }else{

            return redirect('/logout');

        }
    }

    public function logout(){

        // SystemLogs::create([
        //     'username' => session::get('username'),
        //     'comments' => "Pengguna telah log keluar ke sistem.",
        //     'category' => 'Log Keluar'
        // ]);

        Notification::where([
            'user_id' => Session::get('user_id'),
            'onesignal_id' => Session::get('onesignal_id')
        ])->delete();

        Session::flush();
        return redirect('/');
    }

    public function log_system($username){

        $system_logs = new SystemLogs;

        $system_logs->username = $username;
        $system_logs->comments = "Pengguna telah log masuk ke sistem.";
        $system_logs->category = "Log Masuk";

        $system_logs->save();

    }

    public function date_session($request){

        $start_today = date('Y-m-d 00:00:00');
        $end_today = date('Y-m-d 23:58:59');

        session()->put('start_date', $start_today);
        session()->put('end_date', $end_today);
        $request->session()->put('days', 'Harian');

    }
}
