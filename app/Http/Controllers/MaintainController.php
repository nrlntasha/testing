<?php

namespace App\Http\Controllers;

use Str;
use Exception;
use App\SystemLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\MaintainCategory;
use App\Services\TreeReportData;
use App\MaintainanceActivity;
use App\MaintainanceStatus;
use App\Maintance;
use App\TreeRisk;
use App\Type;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

class MaintainController extends Controller
{
    protected $maintainCategory, $created_at, $treeReportData, $maintainance;

    public function __construct(MaintainCategory $maintainCategory, TreeReportData $treeReportData, Maintance $maintainance)
    {
        $this->maintainCategory = $maintainCategory;
        $this->treeReportData = $treeReportData;
        $this->maintainance = $maintainance;
    }

    public function created_at(){
        return $date = date('Y-m-d H:i:s');
    }

    public function pokok(){

        $maintancess = $this->maintainance->getMaintainance();

        $zoneLists = $this->treeReportData->zoneList();

        $riskCheckbox = config('tree-risk');

        $categoryCheckbox = config('tree-category');

        $silaraCheckbox = config('silara');

        return view('superadmin.maintain.pokok.index', compact(
                                                            'maintancess',
                                                            'zoneLists',
                                                            'riskCheckbox',
                                                            'categoryCheckbox',
                                                            'silaraCheckbox'
                                                        )
                                                    );
    }

    public function detail_pokok($treeID){

        $tree = DB::table('trees')
                    ->where('treeID', $treeID)
                    ->first();

        $maintainanceActivity = MaintainanceActivity::isTree()->get();

        $maintainanceRisk = TreeRisk::all();

        $maintainanceStatus = MaintainanceStatus::all();

        if( is_null( $tree ) ) {
            return redirect('/superadmin')->with('error', 'Tree Not Found');
        }

        return view('superadmin.maintain.pokok.new', compact('tree', 'maintainanceActivity', 'maintainanceRisk', 'maintainanceStatus'));

    }

    public function save_pokok(Request $request){

        try{
            $mainID = DB::table('maintances')
                        ->orderBy('maintainID', 'desc')
                        ->first();

            $type_id = Type::where('type', 'Penyelenggaraan')->select('id')->first();

            if(is_null($mainID)){
                $mainIDs = 'MT0000000';
            }else{
                $mainID = Str::substr($mainID -> maintainID, 2);
                $mainIDs = sprintf('MT%07d',$mainID + 1);
            }

            $dateCurrent = $request->maintain_date;
            $newDateFormat = date('Y-m-d H:i:s', strtotime($dateCurrent));

            $input = $request->all();
            $input['maintainID'] = $mainIDs;
            $input['username'] = Session::get('username');
            $input['maintain_date'] = $newDateFormat;
            $input['reference_id'] = $request->treeID;
            unset($input['treeID']);

            Maintance::create($input);

            if($request->hasfile('gambar_sebelum')){

                foreach($request->file('gambar_sebelum') as $file){

                    $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();

                    Image::make($file)->save(public_path()."/maintance/sebelum/{$imgName}", 35);

                    DB::table('media')
                        ->insert([
                            'type_id' => $type_id->id,
                            'reference_id' => $mainIDs,
                            'image_name' => $imgName,
                            'img_ext' => $file->getClientOriginalExtension(),
                            'tags' => 'Maintance Sebelum'
                        ]);
                }
            }

            if($request->hasfile('gambar_selepas')){

                foreach($request->file('gambar_selepas') as $selepas){

                    $imgName = md5($selepas->getClientOriginalName()).'.'.$selepas->getClientOriginalExtension();

                    Image::make($selepas)->save(public_path()."/maintance/selepas/{$imgName}", 35);

                    DB::table('media')
                        ->insert([
                            'type_id' => $type_id->id,
                            'reference_id' => $mainIDs,
                            'image_name' => $imgName,
                            'img_ext' => $selepas->getClientOriginalExtension(),
                            'tags' => 'Maintance Selepas'
                        ]);
                }
            }

            $system_logs = new SystemLogs;

            $system_logs->username = $request->session()->get('username');
            $system_logs->comments = "Simpan data penyenggelaraan";
            $system_logs->category = "Penyenggelaraan";

            $system_logs->save();

            $role = $request->session()->get('role');
            if($role == 'Superadmin'){
                return redirect('/superadmin/maintance-pokok')->with('success', 'Selenggara Disimpan');
            }else if($role == 'Landskap'){
                return redirect('/landskap/maintance-pokok')->with('success', 'Selenggara Disimpan');
            }else{
                return redirect('/logout');
            }

        }catch(QueryException $e){
            dd($e);
        }

        // $mainID = DB::table('maintances')
        //             ->orderBy('maintainID', 'desc')
        //             ->first();

        // if(is_null($mainID)){
        //     $mainIDs = 'MT0000';
        // }else{
        //     $mainID = Str::substr($mainID -> maintainID, 2);
        //     $mainIDs = sprintf('MT%04d',$mainID + 1);
        // }

        // DB::table('maintances')
        //     ->insert([
        //         'maintainID' => $mainIDs,
        //         'treeID' => $request->treeID,
        //         'username' => session()->get('username'),
        //         'risk_level' => $request->risk_level,
        //         'type_maintance' => $request->type_maintance,
        //         'status_maintance' => $request->status_maintance,
        //         'maintain_date' => $request->maintain_date,
        //         'notes' => $request->notes
        //     ]);

        // if($request->hasfile('gambar_sebelum')){

        //     //dd($request->file('gambar_pokok'));
        //     foreach($request->file('gambar_sebelum') as $file){

        //         $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
        //         $file->move(public_path().'/maintance/sebelum/', $imgName);
        //         //$data[$file] = $name;

        //         DB::table('media')
        //             ->insert([
        //                 'treeID' => $mainIDs,
        //                 'image_name' => $imgName,
        //                 'img_ext' => $file->getClientOriginalExtension(),
        //                 'tags' => 'Maintance Sebelum'
        //             ]);
        //     }
        // }

        // if($request->hasfile('gambar_selepas')){

        //     //dd($request->file('gambar_pokok'));
        //     foreach($request->file('gambar_selepas') as $selepas){

        //         $imgName = md5($selepas->getClientOriginalName()).'.'.$selepas->getClientOriginalExtension();
        //         $selepas->move(public_path().'/maintance/selepas/', $imgName);
        //         //$data[$file] = $name;

        //         DB::table('media')
        //             ->insert([
        //                 'treeID' => $mainIDs,
        //                 'image_name' => $imgName,
        //                 'img_ext' => $selepas->getClientOriginalExtension(),
        //                 'tags' => 'Maintance Selepas'
        //             ]);
        //     }
        // }

        // // $system_logs = new SystemLogs;

        // // $system_logs->username = $request->session()->get('username');
        // // $system_logs->comments = "Simpan data penyenggelaraan";
        // // $system_logs->category = "Penyenggelaraan";

        // // $system_logs->save();

        // return redirect('/superadmin/maintance-pokok')->with('success', 'Selenggara Disimpan');
    }

    public function update_pokok($maintainID){

        $maintain = DB::table('maintances')
                        ->where('maintainID', $maintainID)
                        ->first();

        $sebelum = DB::table('media')
                        ->where('reference_id', $maintainID)
                        ->whereIn('tags', ['Maintance Sebelum', 'Sebelum Penyelenggaraan'])
                        ->get();

        $selepas = DB::table('media')
                        ->where('reference_id', $maintainID)
                        ->whereIn('tags', ['Maintance Selepas', 'Selepas Penyelenggaraan'])
                        ->get();

        $activityDropdown = MaintainanceActivity::isTree()->get();
        $currentActivity = MaintainanceActivity::isTree()->whereId($maintain -> type_maintance)->first();

        $statusDropdown = MaintainanceStatus::all();
        $currentStatus = MaintainanceStatus::whereId($maintain -> status_maintance)->first();

        $riskDropdown = TreeRisk::all();
        $currentRisk = TreeRisk::whereId($maintain -> risk_level)->first();

        return view('superadmin.maintain.pokok.detail',
                compact(
                    'maintain',
                    'sebelum',
                    'selepas',
                    'activityDropdown',
                    'currentActivity',
                    'statusDropdown',
                    'currentStatus',
                    'riskDropdown',
                    'currentRisk'
                ));
    }

    public function save_update(Request $request){

        $type_id = Type::where('type', 'Penyelenggaraan')->select('id')->first();

        DB::table('maintances')
            ->where('maintainID', $request->maintainID)
            ->update([
                'username' => session()->get('username'),
                'risk_level' => $request->risk_level,
                'type_maintance' => $request->type_maintance,
                'status_maintance' => $request->status_maintance,
                'maintain_date' => Carbon::parse($request->maintain_date)->format('Y-m-d H:i:s'),
                'notes' => $request->notes
            ]);

        $system_logs = new SystemLogs;

        $system_logs->username = $request->session()->get('username');
        $system_logs->comments = "Kemaskini penyenggelaraan";
        $system_logs->category = "Penyenggelaraan";

        $system_logs->save();

        $role = $request->session()->get('role');
        if($role == 'Superadmin'){
            return redirect('/superadmin/maintance-pokok')->with('success', 'Selenggara Dikemaskini');
        }else if($role == 'Landskap'){
            return redirect('/landskap/maintance-pokok')->with('success', 'Selenggara Dikemaskini');
        }else{
            return redirect('/logout');
        }

    }

    public function delete(Request $request, $maintainID){

        DB::table('maintances')
            ->where('maintainID', $maintainID)
            ->delete();

        $system_logs = new SystemLogs;

        $system_logs->username = $request->session()->get('username');
        $system_logs->comments = "Padam Penyenggelaraan";
        $system_logs->category = "Penyenggelaraan";

        $system_logs->save();

        $role = $request->session()->get('role');
        if($role == 'Superadmin'){
            return redirect('/superadmin/maintance-pokok')->with('success', 'Selenggara Dipadam');
        }else if($role == 'Landskap'){
            return redirect('/landskap/maintance-pokok')->with('success', 'Selenggara Dipadam');
        }else{
            return redirect('/logout');
        }

    }

    public function allCategory(){

        try{
            $perCategorys = $this->maintainCategory->perCategory();

            return view('superadmin.maintain.pokok.category',
                    compact(
                        'perCategorys'
                    ));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }

    }

    public function allZone(){

        try{

            $perZones = $this->maintainCategory->perZone();

            return view('superadmin.maintain.pokok.zone',
                    compact(
                        'perZones'
                    ));

        }catch(Exception $e){
            dd($e);
        }

    }

    public function perCategory(Request $request) {

        try{
            $zonCategory = $this->maintainCategory->zonPerCategory($request->status_maintance);
            $categoryType = $request->status_maintance;

            return view('superadmin.maintain.pokok.perCategory', compact('zonCategory', 'categoryType'));

        }catch (Exception $e){
            // dd($e);
            return redirect()->back();
        }

    }

    public function listPerZone(Request $request){

        try{
            $listPerZone = $this->maintainCategory->listPerZone($request->zone_name);
            $zone_name = $request->zone_name;

            return view('superadmin.maintain.pokok.perZone', compact('listPerZone', 'zone_name'));
        }catch(Exception $e){
            return redirect()->back();
        }
    }

    public function printById($id){

        $data = Maintance::where('maintainID', $id)->with('images', 'getActivity', 'getTree')->first();
        // $zone = $data->get_tree;
        // dd($data->images);
        $user = User::where('username', $data->username)->first();

        $treeDetail = $data->getTree;
        $activityList = MaintainanceActivity::pluck('activity_name');
// dd($activityList->toArray());
        return view('superadmin.export.maintainanceReport', compact('data', 'user', 'treeDetail', 'activityList'));
        // return response()->json([
        //     'message' => 'Success',
        //     'data' => $data,
        //     'user' => $user
        // ], 200);
    }

    // public function datatables(TreeDataTable $treeDataTable, Request $request){
    //     if($request->has('action')){
    //         return $treeDataTable->printPreview();
    //     }else{
    //         return $treeDataTable->ajax();
    //     }
    // }

    // public function exportPDF(Request $request){

    //     $data = $this->treeReportData->export($request);
    //     $pdf = PDF::loadView('superadmin.export.softScapeMaintainance', compact('data'));
    //     return $pdf->download('export.pdf');
    // }

}
