<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Services\CustomReportData;

class ReportController extends Controller
{

    protected $customReportData;

    public function __construct(CustomReportData $customReportData){
        $this->customReportData = $customReportData;
    }

    public function pokok(){

        $zones = DB::table('trees')
                    ->select('zone')
                    ->groupBy('zone')
                    ->get();

        $zoneList = '';

        foreach ($zones as $zone) {
            $zoneList .= "'".$zone->zone."',";
        }
        $zoneList = substr($zoneList, 0, -1);

        return view('superadmin.laporan.pokok.index', compact('zones'));
    }

    public function byMonth($first, $second){
        // dd($this->customReportData);
        $byMonth = $this->customReportData->byMonth($first, $second);
        dd($byMonth);
    }

}
