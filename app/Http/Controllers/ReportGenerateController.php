<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Registries\KejurReportTemplateRegistry;

class ReportGenerateController extends Controller
{
    protected $templateRegistry;

    public function __construct(KejurReportTemplateRegistry $templateRegistry)
    {
        $this->templateRegistry = $templateRegistry;
    }

    public function detailKejur($id){

        if(!request()->has('template')) {
            request()->merge(['template' => 'detail']);
        }

        $kejur = DB::table('kejurs')
                    ->where('kejurID', $id)
                    ->first();

        $log_kejurs = DB::table('log_kejurs')
            ->where('log_kejurs.kejurID', $id)
            ->get();

        // $kejur_media = DB::table('kejur_media')
        //                 ->where('kejurID', $id)
        //                 ->orderBy('nama_alatan')
        //                 ->get();

        $collection = DB::table('kejur_media')
                ->where('kejurID', $id)
                ->get(); 

        $kejur_medias = $collection->groupBy('nama_alatan');

        $data = [
            'kejur' => $kejur,
            'log_kejurs' => $log_kejurs,
            'kejur_medias' => $kejur_medias
        ];

        $template = $this->templateRegistry->get(request()->template);

        return $template->getTemplate($data);

    }
    
}
