<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;
Use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class SettingController extends Controller
{
    public function settingPage() {

        $user = User::where('username', session()->get('username'))->first();

        if( ! $user instanceof User ) return redirect('/logout');

        $userDetails = $user->details;

        return view('settings.setting-page', compact('user', 'userDetails'));
    }

    public function update(Request $request)
    {
        $user = User::find(session()->get('username'));

        if( ! $user instanceof User ) return redirect('/logout');

        $userDetails = $user->details;

        try{

            Schema::disableForeignKeyConstraints();
            $user->update($request->all());
            $userDetails->update($request->all());
            Schema::enableForeignKeyConstraints();

            return redirect('/superadmin')->with('success', 'Maklumat Berjaya Dikemaskini.');

        }catch(QueryException $e){

            return response()->json([
                'status' => 'error',
                'details' => $e->getMessage()
            ]);

        }

    }

    public function updatePassword(Request $request)
    {


        $newPassword = $request->password;
        $user = User::find(session()->get('username'));
        $existingPassword = $user->password;


        // return response()->json();

        if($request->type == 'validate'){
            // validate

            $status = Hash::check($newPassword, $existingPassword) ? 'Verified' : 'Wrong Password';

            return response()->json(['status' => $status]);

        }else if($request->type == 'update'){
            // Update

            try{

                $user->update(['password' => bcrypt($newPassword)]);

                return response()->json(['status' => 'Updated']);

            }catch(Exception $e){

                return response()->json(['status' => 'errror', 'details' => $e->getMessage()]);

            }

        }else{

            return redirect()->json('Unexpected Error');

        }

    }
}
