<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class LogAuditController extends Controller
{
    public function index(){

        try{
            $log_audits = DB::table('system_logs')
                                ->orderByDesc('created_at')
                                ->get();

            return view('superadmin.audit.index', compact('log_audits'));
        }catch(QueryException $e){
            dd($e);
        }
    }
}
