<?php

namespace App\Http\Controllers\Superadmin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class LembutController extends Controller
{
    public function category(){
        try{
            $categorys = DB::table('config_categories')
                            ->get();
            
            return view('superadmin.tetapan.lembut.index', compact('categorys'));
        }catch(QueryException $e){
            dd($e);
        }
    }
}
