<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function getCategory(){
        $categorys = DB::table('config_categories')
                        ->get();
        
        return $categorys;
    }
    
    public function index(){
        // $client = $this->getClientTable();
        // $user   = $this->getUserTable();

        $categorys = $this->getCategory();


        return view('superadmin.tetapan.index', compact('categorys'));
    }

    // public function getUserTable() {
    //     return data table user;
    // }

    // public function getClientTable() {
    //     return data client;
    // }

}
