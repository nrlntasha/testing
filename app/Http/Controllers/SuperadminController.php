<?php

namespace App\Http\Controllers;

use App\Maintance;
use App\Services\TreeList;
use App\Services\DashboardData;
use App\TreeSpecies;
use Illuminate\Support\Facades\DB;
use App\Tree;
use Illuminate\Http\Request;
use App\Notification;
use Exception;
use Illuminate\Support\Facades\Session;
use Str;

class SuperadminController extends Controller
{
    protected $dashboardData, $treeList;

    public function __construct(DashboardData $dashboardData, TreeList $treeList){
        $this->dashboardData = $dashboardData;
        $this->treeList = $treeList;
    }

    public function index(){

        $treeCount = 0;
        $tainCount = 0;
        $hardCount = 0;

        $month = config('month');

        $treeCount = DB::table('trees')
                        ->select(DB::raw("(SELECT COUNT(*) FROM trees) as treeCount"))
                        ->first();

        $tainCount = DB::table('maintances')
                        ->select(DB::raw("(SELECT COUNT(*) FROM maintances) as tainCount"))
                        ->first();

        $hardCount = DB::table('kejurs')
                        ->select(DB::raw("(SELECT COUNT(*) FROM kejurs) as hardCount"))
                        ->first();

        $bilPokokTinggi = $this->dashboardData->bilPokokTinggi();
        $bilPokokSederhana = $this->dashboardData->bilPokokSederhana();
        $bilPokokRendah = $this->dashboardData->bilPokokRendah();
        $log_sistem = $this->dashboardData->log_sistem();
        $bilPokokRenek = $this->dashboardData->bilPokokRenek();
        $bilPokokPalma = $this->dashboardData->bilPokokPalma();
        $bilPokokTeduhan = $this->dashboardData->bilPokokTeduhan();


        $pokokRisikoRendah = $this->treeList->pokokRisikoRendah();

        $treeChart = DB::table('trees')->selectRaw("risk, tree_risk.name as risk_name, COUNT(*) as totalTree, MONTH(trees.created_at) as month")
                                        ->join('tree_risk', 'risk', 'tree_risk.id')
                                       ->groupBy('risk')
                                       ->groupBy('month')
                                       ->get()
                                       ->groupBy('risk_name');

        $log_audits = DB::table('system_logs')
                            ->get();

        return view('superadmin.index',
                compact(
                    'treeCount',
                    'tainCount',
                    'hardCount',
                    'bilPokokTinggi',
                    'bilPokokSederhana',
                    'bilPokokRendah',
                    'log_sistem',
                    'bilPokokRenek',
                    'bilPokokPalma',
                    'bilPokokTeduhan',
                    'treeChart',
                    'log_audits',
                    'month'
                ));
    }

    public function dashboardGraph(Request $request){

        if($request->category == 'tree'){

            $data = DB::table('trees')->selectRaw('zone, count(*) as count')
                    ->groupBy('zone')
                    ->when($request->month, function($query) use($request){
                        $query->whereMonth('created_at', $request->month);
                    });

        }else{

            $data = DB::table('maintances')->join('trees', 'maintances.reference_id', '=', 'trees.treeID')
                    ->selectRaw('trees.zone, count(*) as count')->groupBy('zone')
                    ->when($request->month, function($query) use($request){
                        $query->whereMonth('maintances.created_at', $request->month);
                    });

        }

        $data = $data->get()->sortBy('zone', SORT_NATURAL, true)
                ->mapWithKeys(function($data){
                    return [$data->zone => $data->count];
                });

        $zone = [];
        $value = [];
        foreach($data as $key => $val){
            array_push($zone, $key);
            array_push($value, $val);
        }

        return response()->json([
            'zone' => $zone,
            'value' => $value
        ]);
    }

    public function getCoordinate()
    {
        $tree = DB::table('trees')
                // ->select('local_name','saintifik_name','type','category','height','silara','location','zone','latitude','longitude','risk')
                ->select('type','category','height','silara','location','zone','latitude','longitude','risk')
                ->whereNotNull('latitude')
                ->whereNotNull('longitude')
                ->get();

        return response()->json($tree,200);
    }

    public function getMaintainanceCoordinate()
    {
        $data = DB::table('maintances')
                ->join('trees', 'reference_id', '=', 'treeID')
                ->whereNotNull('latitude')
                ->whereNotNull('longitude')
                ->get();

        return response()->json($data, 200);
    }

    public function storeOnesignal(Request $request)
    {
        try{

            $input['user_id'] = Session::get('user_id');
            $input['onesignal_id'] = $request->onesignal_id;

            $exist = Notification::where($input)->first();

            if(!$exist){

                Notification::create($input);

                Session::put('onesignal_id', $input['onesignal_id']);

                return response()->json([
                    'message' => 'Success',
                    'data' => 'Successfully created'
                ]);

            }

            return response()->json([
                'message' => 'Success',
                'data' => 'existed'
            ]);

        }catch(Exception $ex){

            return response()->json([
                'message' => 'Error',
                'data' => 'No Data',
                'description' => $ex->getMessage()
            ]);

        }
    }

}
