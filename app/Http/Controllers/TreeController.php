<?php

namespace App\Http\Controllers;

use DataTables;
use App\SystemLogs;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Services\TreeList;
use App\TreeCategory;
use App\Tree;
use App\TreeMedia;
use App\TreeRisk;
use App\TreeSilara;
use App\TreeSpecies;
use App\Type;
use Throwable;

class TreeController extends Controller
{
    protected $treeListTable, $created_at;

    public function __construct(TreeList $treeListTable)
    {
        $this->treeListTable = $treeListTable;
    }

    public function created_at(){
        return $date = date('Y-m-d H:s:i');
    }

    public function index(){

        /* DROPDOWN OPTION */
        $categoryDropdown = TreeCategory::all();
        $riskDropdown = TreeRisk::all();
        $silaraDropdown = TreeSilara::all();

        return view('superadmin.landskap.lembut.new', compact('categoryDropdown', 'riskDropdown', 'silaraDropdown'));
    }

    public function save(Request $request){

        try{

            /* DIFFERENTIATE BEETWEEN SCIENTIFIC NAME AND LOCAL NAME; IF FALSE, INSERT NEW SPECIES ELSE IGNORE */

            $query = TreeSpecies::where(['local_name' => $request->nama_biasa, 'scientific_name' => $request->nama_saintifik]);
            $count = $query->count();

            if($count == 0){
                try{
                    /* GET AFTER CREATED */
                    $speciesID = TreeSpecies::create([
                                    'local_name' => $request->nama_biasa,
                                    'scientific_name' => $request->nama_saintifik,
                                    'genus' => $request->jenis
                                ])->id;
                }catch(Throwable $th){
                    return $th;
                }
            }else{
                $fetch = $query->first();
                /* CHECK GENUS; IF GENUS FILLED, UPDATE GENUS */
                if($fetch->genus == NULL){
                    TreeSpecies::whereId($fetch->id)->update(['genus' => $request->jenis]);
                }
                $speciesID = $fetch->id;
            }

            /* GET LAST INSERTED TREEID */

            $treeID = DB::table('trees')
                        ->orderBy('treeID', 'desc')
                        ->first();

            if(is_null($treeID)){
                $treeIDs = 'TR0000000';
            }else{
                $treeID = Str::substr($treeID -> treeID, 2);
                $treeIDs = sprintf('TR%07d',$treeID + 1);
            }

            $inventoryNo = 'MBPJ/'.$treeIDs;

            DB::table('trees')
                ->insert([
                    'treeID' => $treeIDs,
                    'species_id' => $speciesID,
                    'category' => $request->kategori,
                    'height' => $request->tinggi,
                    'silara' => $request->silara,
                    'location' => $request->lokasi,
                    'zone' => $request->zon,
                    'inventory_no' => $inventoryNo,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'risk' => $request->risiko,
                    'notes' => $request->notes,
                    'created_at' => $this->created_at()
                ]);

            if($request->hasFile('gambar_pokok')){

                $gambar_pokoks = $request->file('gambar_pokok');
                $type_id = Type::where('type', 'Pokok')->select('id')->first();

                foreach($gambar_pokoks as $gambar_pokok){

                    $imgName = md5($gambar_pokok->getClientOriginalName()).'.'.$gambar_pokok->getClientOriginalExtension();

                    $gambar_pokok->move(public_path('pokok'), $imgName);

                    DB::table('media')
                        ->insert([
                            'type_id' => $type_id->id,
                            'reference_id' => $treeIDs,
                            'image_name' => $imgName,
                            'img_ext' => $gambar_pokok->getClientOriginalExtension(),
                            'tags' => 'Pokok'
                        ]);
                }
            }

            $system_logs = new SystemLogs;

            $system_logs->username = $request->session()->get('username');
            $system_logs->comments = "Pemyimpanan data pokok baru";
            $system_logs->category = "Lembut";

            $system_logs->save();

            $role = $request->session()->get('role');
            if($role == 'Superadmin'){
                return redirect('/superadmin/lembut');
            }else if($role == 'Landskap'){
                return redirect('/landskap/lembut');
            }else{
                return redirect('/logout');
            }

        }catch(Exception $e){

            dd($e->getMessage());

        }

    }

    public function detail($treeID){

        $trees = Tree::getTreeDetail()->with('media')->find($treeID);
        $medias = $trees->media;

        $silaraDropdown = TreeSilara::all();
        $categoryDropdown = TreeCategory::all();
        $riskDropdown = TreeRisk::all();

        return view('superadmin.landskap.lembut.detail', compact('trees', 'medias', 'silaraDropdown', 'categoryDropdown', 'riskDropdown'));
    }

    public function update(Request $request){

        $query = TreeSpecies::where(['local_name' => $request->nama_biasa, 'scientific_name' => $request->nama_saintifik]);
        $count = $query->count();

        if($count > 0){
            $data = $query->first();
            $speciesID = $data->id;
        }else{
            $speciesID = TreeSpecies::create([
                'local_name' => $request->nama_biasa,
                'scientific_name' => $request->nama_saintifik,
                'genus' => $request->jenis
            ])->id;
        }

        DB::table('trees')
            ->where('treeID', $request->treeID)
            ->update([
                'species_id' => ($speciesID),
                'category' => $request->kategori,
                'height' => $request->tinggi,
                'silara' => $request->silara,
                'location' => $request->lokasi,
                'zone' => $request->zon,
                'inventory_no' => $request->inventory_no,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'risk' => $request->risiko,
                'notes' => $request->notes
            ]);

        $system_logs = new SystemLogs;

        $system_logs->username = $request->session()->get('username');
        $system_logs->comments = "Kemaskini maklumat pokok";
        $system_logs->category = "Lembut";

        $system_logs->save();

        $role = $request->session()->get('role');
        if($role == 'Superadmin'){
            return redirect('/superadmin/lembut')->with('success', 'Pokok Dikemaskini');
        }else if($role == 'Landskap'){
            return redirect('/landskap/lembut')->with('success', 'Pokok Dikemaskini');
        }else{
            return redirect('/logout');
        }

    }

    public function delete(Request $request, $treeID){

        $medias = TreeMedia::where('reference_id', $treeID)->get();

        foreach($medias as $media){
            File::delete('pokok/'.$media->image_name);
        }

        TreeMedia::where('reference_id', $treeID)->delete();

        Tree::where('treeID', $treeID)->delete();

        $system_logs = new SystemLogs;

        $system_logs->username = $request->session()->get('username');
        $system_logs->comments = "Padam Data pokok";
        $system_logs->category = "Lembut";

        $system_logs->save();

        $role = $request->session()->get('role');
        if($role == 'Superadmin'){
            return redirect('/superadmin/lembut')->with('success', 'Pokok Dipadam');
        }else if($role == 'Landskap'){
            return redirect('/landskap/lembut')->with('success', 'Pokok Dipadam');
        }else{
            return redirect('/logout');
        }

    }

    public function detail2($treeID){

        $trees = Tree::getTreeDetail()->find($treeID);

        $medias = $trees->media;

        return view('superadmin.landskap.lembut.full', compact('trees', 'medias'));

    }

}
