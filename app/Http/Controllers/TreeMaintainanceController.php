<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tree;
use Illuminate\Support\Facades\DB;
use App\Maintance;

class treeMaintainanceController extends Controller
{
    public function index()
    {

        $month = config('month');
        $years = DB::table('trees')->distinct()->selectRaw('year(created_at) as year')->pluck('year');

        return view('superadmin.laporan.penyelenggaraan.treeMaintainanceReport', compact('month', 'years'));

    }

    public function report(Request $request)
    {

        $activities = DB::table('maintainance_activity')->get();

        $count = Maintance::with(['getTree'])
                ->when($request->year, function($query) use($request){
                    $query->whereYear('created_at', $request->year);
                })
                ->when($request->month, function($query) use($request){
                    $query->whereMonth('created_at', $request->month);
                })
                ->get()->groupBy(['getTree.zone'])
                ->map(function($item, $key){
                    return $item->groupBy('maintainance_activity')->map->count();
                });

        $maintenance = DB::table('maintances')->join('maintainance_activity', 'type_maintance', 'id')
                            ->join('trees', 'maintances.reference_id', 'treeID')
                            ->when($request->year, function($query) use($request){
                                $query->whereYear('maintances.maintain_date', $request->year);
                            })
                            ->when($request->month, function($query) use($request){
                                $query->whereMonth('maintances.maintain_date', $request->month);
                            })
                            ->where('zone', '!=', 'USJ')
                            ->get()
                            ->groupBy('activity_name')->map->count();

        $year = $request->year;

        /* FOR OUTPUT DEBUGGING */
        // return response()->json($maintenance);
        /* END HERE */

        $zones = [];
        for($i=1;$i<=24;$i++){
            array_push($zones, $i);
        }

        $zones2 = [];
        for($i=1;$i<=24;$i++){
            array_push($zones2, 'ZON '.$i);
        }

        return view('superadmin.export.report', compact('zones', 'zones2', 'activities', 'count', 'maintenance', 'year'));

    }
}
