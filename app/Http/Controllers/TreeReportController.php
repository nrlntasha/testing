<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TreeReportData;
use Illuminate\Support\Facades\Session;

class TreeReportController extends Controller
{

    protected $treeReportData;

    public function __construct(TreeReportData $treeReportData)
    {
        $this->treeReportData = $treeReportData;
    }

    public function index(){

        $zoneCounts = $this->treeReportData->zoneCount();
        $treeCounts = $this->treeReportData->treeCount();
        $locationCounts = $this->treeReportData->locationCount();
        $treeTypes = $this->treeReportData->treeType();
        $zoneLists = $this->treeReportData->zoneList();

        return view('superadmin.laporan.pokok.index',
                compact(
                    'zoneCounts',
                    'treeCounts',
                    'locationCounts',
                    'treeTypes',
                    'zoneLists'
                ));

    }

    public function getLocation(Request $request){

        $getLocation = $this->treeReportData->getLocation($request);

        // dd(json_encode($getLocation));

        return json_encode($getLocation);
    }

    public function getTreeType(Request $request){

        $getTreeType = $this->treeReportData->getTreeType($request);

        return json_encode($getTreeType);

    }

}
