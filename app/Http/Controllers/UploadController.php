<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function index(){
        return view('superadmin.akrib.index');
    }
    public function getDownload($filename)
    {
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/pdf/'.$filename.'.pdf";

        $headers = [
            'Content-Type' => 'application/pdf',
         ];

        return response()->download($file, ''.$filename.'.pdf', $headers);

    }
}
