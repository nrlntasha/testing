<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\UserDetail\Landskap;

class UserController extends Controller
{
    protected $landskap, $kejur, $intern;

    public function __construct(Landskap $landskap)
    {
        $this->landskap = $landskap;
    }

    public function showUser(Request $request){

        // dd($request->user);
        $landskaps = $this->landskap->index($request->user);
        $user = $request->user;

        return view('superadmin.user.index', compact('landskaps', 'user'));

    }

    public function formUser(){

        return view('superadmin.user.form');

    }

    public function storeUser(Request $request){

        try{
            $response = $this->landskap->store($request);

            return redirect("superadmin/user/")->with('success', $response);

        }catch(Exception $e){

            return redirect()->back()->withErrors('Pengguna Telah Wujud')->withInput($request->input());

        }

    }


    public function updateUser(Request $request, $role){

        try{

            $ic = decrypt($request->_id);

            $response = $this->landskap->update($ic, $request, $role);

            return redirect("/settings")->with('success', $response);

        }catch(Exception $e){

            dd($e);

            return redirect()->back()->withErrors('Pengguna Telah Wujud')->withInput($request->input());

        }

    }

    public function deleteUser($ic_number){

        try{

            $reponse = $this->landskap->delete($ic_number);

            return redirect()->back()->with('success', $reponse);

        }catch(Exception $e){

            return redirect()->back()->withErrors('Pengguna Gagal Dipadam');

        }

    }

}
