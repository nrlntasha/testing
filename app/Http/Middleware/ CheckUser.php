<?php

namespace App\Http\Middleware;

// use App\SystemLogs;
use Illuminate\Support\Facades\DB;

use Closure;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = DB::table('users')->where('ic_number', $request->ic_number)->first();
        // dd($request->all());

        // SystemLogs::create([
        //     'username' => $user->username,
        //     'comments' => "User has attempt to login and this account has been suspended",
        //     'category' => 'Login'
        // ]);

        if($user && $user->is_suspended){
            return redirect('/')->withErrors('This account has been suspended. Please contact admin for support.');
        }

        return $next($request);
    }
}
