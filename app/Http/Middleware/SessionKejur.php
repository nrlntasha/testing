<?php

namespace App\Http\Middleware;

use Closure;

class SessionKejur
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->get('role')=='Kejur'){
            return redirect('logout');
        }
        return $next($request);
    }
}
