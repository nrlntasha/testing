<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kejur extends Model
{
    protected $table = 'kejurs';

    protected $primaryKey = 'kejurID';

    public $incrementing = false;

    public $timestamps = true;

    protected $fillable = [
        'kejurID',
        'zon',
        'kawasan',
        'panel_id',
        'no_inventori',
        'maintance_status',
        'status_pengambilan',
        'location_notes',
        'location',
        'catatan',
        'keluasan',
        'intake_date',
        'status_pengambilan',
        'kemudahan',
        'perwartaan',
        'kawasan_lapang',
        'taman',
        'penampan',
        'padang',
        'dewan',
        'dijumpai',
        'created_by',
        'created_at',
        'updated_at',
        'no_perwartaan'
    ];

    protected $casts = [
        'keluasan' => 'string',
        'no_inventori' => 'string'
    ];

    protected $appends = [
        'no_inventori',
    ];

    public function kejurProperty()
    {
        return $this->hasMany(LogKejur::class, 'kejurID', 'kejurID');
    }

    public function kejurStatus()
    {
        return $this->hasOne(KejurStatus::class, 'id', 'status_pengambilan');
    }

    public function getNoInventoriAttribute(){
        return !is_null($this->attributes['no_inventori']) ? $this->attributes['no_inventori'] : "null";
    }

    public function getCatatanAttribute(){
        return !is_null($this->attributes['catatan']) ? $this->attributes['catatan'] : "";
    }
}
