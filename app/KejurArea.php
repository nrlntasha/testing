<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KejurArea extends Model
{
    protected $table = "kejur_area";

    protected $fillable = [
        'status_pengambilan',
        'taman',
        'penampan',
        'padang',
        'dewan',
        'zon',
        'kawasan',
        'no_inventori',
        'lokasi',
        'keluasan',
        'catatan'
    ];
}
