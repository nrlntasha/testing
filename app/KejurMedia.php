<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KejurMedia extends Model
{
    protected $table = 'kejur_media';

    protected $fillable = [
        'kejurID',
        'nama_alatan',
        'image_name',
        'img_ext',
        'tags'
    ];

    protected $primaryKey = 'id';

    protected $appends = ['image_link'];

    public $timestamps = true;

    public $incrementing = true;

    public function logKejur(){
        return $this->belongsTo(LogKejur::class, 'kejurID', 'kejurID');
    }

    public function getImageLinkAttribute(){
        $host = request()->getSchemeAndHttpHost();
        return $host.'/kejur/'.$this->attributes['image_name'];
    }
}
