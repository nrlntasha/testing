<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KejurStatus extends Model
{
    protected $table = 'kejur_status';

    protected $fillable = ['status'];

    public $incrementing = true;

    public $timestamps = true;

    // public function
}
