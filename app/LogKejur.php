<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogKejur extends Model
{
    protected $table = 'log_kejurs';

    protected $fillable = ['kejurID', 'nama_alatan', 'kuantiti_baik', 'kuantiti_rosak', 'kuantiti_semasa', 'username'];

    public $timestamps = true;

    public $incrementing = true;

    // public $appends = ['detail_kejur'];

    public function kejur(){
        return $this->belongsTo(Kejur::class, 'kejurID', 'kejurID');
    }

    public function images(){
        return $this->hasMany(KejurMedia::class, 'kejurID', 'kejurID');
    }

    // public function getDetailKejurAttribute(){
    //     return $this->kejur;
    // }
}
