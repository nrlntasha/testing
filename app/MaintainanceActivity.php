<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainanceActivity extends Model
{
    protected $table = 'maintainance_activity';

    protected $fillable = [
        'maintainance_type_id',
        'activity_name',
        'created_at',
        'updated_at'
    ];

    public function scopeIsTree($query){
        return $query->where('maintainance_type_id', 1);
    }

    public function maintainance(){
        return $this->belongsTo(Maintance::class, 'id', 'type_maintance');
    }
}
