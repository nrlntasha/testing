<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainanceStatus extends Model
{
    protected $table = 'maintainance_status';

    protected $primaryKey = 'id';

    protected $fillable = ['status', 'created_at'];

    public function maintainance(){
        return $this->belongsTo(Maintance::class);
    }
}
