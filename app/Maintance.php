<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Maintance extends Model
{
    protected $table = 'maintances';

    protected $fillable = [
        'maintainID',
        'reference_id',
        'username',
        // 'img_before',
        // 'img_after',
        'risk_level',
        'type_maintance',
        'status_maintance',
        'maintain_date',
        'notes',
        'created_at',
        'updated_at'
    ];

    protected $primaryKey = 'maintainID';

    protected $hidden = [
        'img_before',
        'img_after',
        'getStatus',
        'getActivity'
    ];

    protected $appends = [
        'status',
        'maintainance_activity'
    ];

    public $incrementing = false;

    public function getTree(){
        return $this->hasOne(Tree::class, 'treeID', 'reference_id');
    }

    public function getSpecies(){
        return $this->belongsTo(Tree::class, 'species_id', 'id');
    }

    public function getRisk(){
        return $this->hasOne('App\TreeRisk', 'id', 'risk_level');
    }

    public function getActivity(){
        return $this->hasOne(MaintainanceActivity::class, 'id', 'type_maintance');
    }

    public function getStatus(){
        return $this->hasOne(MaintainanceStatus::class, 'id', 'status_maintance');
    }

    public function images(){
        return $this->hasMany(TreeMedia::class, 'reference_id', 'maintainID');
    }

    public function getStatusAttribute($key)
    {
        return $this->getStatus->status;
    }

    public function getMaintainanceActivityAttribute(){
        return $this->getActivity->activity_name;
    }

    public static function getMaintainance(){
        return self::with('getTree', 'getRisk', 'getActivity', 'getStatus')
                    ->whereBetween('maintain_date', [Session::get('start_date'), Session::get('end_date')])
                    ->get();
    }
}
