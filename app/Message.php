<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $fillable = [
        'subject',
        'message'
    ];

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = true;

    public function detail(){
        return $this->hasMany(MessageDetail::class);
    }
}
