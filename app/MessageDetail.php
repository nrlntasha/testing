<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
    protected $table = 'message_details';

    protected $fillable = [
        'from',
        'to',
        'message_id',
    ];

    protected $appends = ['message_text', 'subject'];

    protected $hidden = ['message'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    public $incrementing = true;

    public function sender(){
        return $this->belongsTo(User::class, 'from', 'user_id');
    }

    public function receipient(){
        return $this->belongsTo(User::class, 'to', 'user_id');
    }

    public function message(){
        return $this->belongsTo(Message::class);
    }

    public function getSubjectAttribute()
    {
        return $this->message->subject;
    }

    public function getMessageTextAttribute()
    {
        return $this->message->message;
    }

    public function scopeUnread($query){
        return $query->whereIsRead(false);
    }
}
