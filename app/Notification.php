<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notifications";

    protected $primaryKey = "id";

    protected $fillable = [
        'user_id',
        'onesignal_id',
    ];

    public $timestamps = true;

    public $incrementing = true;

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
