<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CustomReportData extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Service\CustomReportData', function($app){
            return new CustomReportData();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
