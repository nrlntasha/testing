<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DashboardData extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('App\Service\DashboardData', function($app){
        //     return new DashboardData();
        // });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
