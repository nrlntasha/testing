<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\Templates\KejurTemplate;
use App\Services\Registries\KejurReportTemplateRegistry;

class KejurTemplateServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(KejurReportTemplateRegistry::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $templates = config('kejur-templates');

        if(is_array($templates)) $this->registerTemplates($templates);
    }

    protected function registerTemplates(array $templates = []){

        foreach ($templates as $key => $template) {

            $templateObject = $this->app->make($template);

            if($templateObject instanceof KejurTemplate){
                $this->app->make(KejurReportTemplateRegistry::class)->register(
                    $key, $templateObject
                );
            }

        }
    }
}