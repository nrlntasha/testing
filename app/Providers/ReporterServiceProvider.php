<?php

namespace App\Providers;

use App\Contracts\Reporter\Reporter;
use Illuminate\Support\ServiceProvider;
use App\Services\Registries\ReporterRegistry;

class ReporterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $reporters = config('reporter');

        if(is_array($reporters)) $this->registerReporters($reporters);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ReporterRegistry::class);
    }

    /**
     * Regiter All Reporters.
     * 
     * @param  array $reporters
     * @return void
     */
    protected function registerReporters(array $reporters = []) {
        foreach($reporters as $key => $reporter) {

            $reporterObject = $this->app->make($reporter);

            if($reporterObject instanceof Reporter) {
                $this->app->make(ReporterRegistry::class)->register(
                    $key, $reporterObject
                );
            }

        }
    }
}
