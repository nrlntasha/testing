<?php

namespace App\Services;

use DB;
use DateTime;
use DateInterval;

class CustomReportData{

  public function tinggiThreeMonth(){

    // constructor accepts all the formats from strtotime function
    $startdate = new DateTime('first day of january this year');
    // without a value it returns current date
    $enddate = new DateTime('last day of december this year');

    // all possible formats for DateInterval are in manual
    // but basically you need to start with P indicating period
    // and then number of days, months, seconds etc
    $interval = new DateInterval('P3M');
    // $chart_data = '';
    $reportData = '';
    while($startdate < $enddate){

      $periodstart = clone $startdate;
      $startdate->add($interval);
      $periodend = clone $startdate;
      $periodend->modify('-1 month');

      $start = $periodstart->format('m/d/Y');
      $end = $periodend->format('m/t/Y');
      // $chart_data .="'".$periodstart->format('F')."-".$periodend->format('F')."',";

      $reportData .= DB::table('maintances')
                        ->whereBetween('maintain_date', [$start, $end])
                        ->where('risk_level', '3')
                        ->count().",";
    }

    $reportData = substr($reportData, 0, -1);

    return $reportData;

  }

  public function sederhanaThreeMonth(){

    // constructor accepts all the formats from strtotime function
    $startdate = new DateTime('first day of january this year');
    // without a value it returns current date
    $enddate = new DateTime('last day of december this year');

    // all possible formats for DateInterval are in manual
    // but basically you need to start with P indicating period
    // and then number of days, months, seconds etc
    $interval = new DateInterval('P3M');
    // $chart_data = '';
    $reportData = '';
    while($startdate < $enddate){

      $periodstart = clone $startdate;
      $startdate->add($interval);
      $periodend = clone $startdate;
      $periodend->modify('-1 month');

      $start = $periodstart->format('m/d/Y');
      $end = $periodend->format('m/t/Y');
      // $chart_data .="'".$periodstart->format('F')."-".$periodend->format('F')."',";

      $reportData .= DB::table('maintances')
                        ->whereBetween('maintain_date', [$start, $end])
                        ->where('risk_level', 2)
                        ->count().",";
    }

    $reportData = substr($reportData, 0, -1);

    return $reportData;

  }

  public function rendahThreeMonth(){

    // constructor accepts all the formats from strtotime function
    $startdate = new DateTime('first day of january this year');
    // without a value it returns current date
    $enddate = new DateTime('last day of december this year');

    // all possible formats for DateInterval are in manual
    // but basically you need to start with P indicating period
    // and then number of days, months, seconds etc
    $interval = new DateInterval('P3M');
    // $chart_data = '';
    $reportData = '';
    while($startdate < $enddate){

      $periodstart = clone $startdate;
      $startdate->add($interval);
      $periodend = clone $startdate;
      $periodend->modify('-1 month');

      $start = $periodstart->format('m/d/Y');
      $end = $periodend->format('m/t/Y');
      // $chart_data .="'".$periodstart->format('F')."-".$periodend->format('F')."',";

      $reportData .= DB::table('maintances')
                        ->whereBetween('maintain_date', [$start, $end])
                        ->where('risk_level', 1)
                        ->count().",";
    }

    $reportData = substr($reportData, 0, -1);

    return $reportData;

  }


    public function getMonthRange(){
        // constructor accepts all the formats from strtotime function
        $startdate = new DateTime('first day of january this year');
        // without a value it returns current date
        $enddate = new DateTime('last day of december this year');

        // all possible formats for DateInterval are in manual
        // but basically you need to start with P indicating period
        // and then number of days, months, seconds etc
        $interval = new DateInterval('P3M');
        $chart_data = '';
        while($startdate < $enddate){

            $periodstart = clone $startdate;
            $startdate->add($interval);
            $periodend = clone $startdate;
            $periodend->modify('-1 month');
            $chart_data .="'".$periodstart->format('F')."-".$periodend->format('F')."',";

        }
        return $chart_data = substr($chart_data, 0, -1);
    }

    public function byMonth($first, $second){

        $maintanence_list = DB::table('maintances')
                            ->whereMonth('maintain_date', '>=', $first)
                            ->whereMonth('maintain_date', '<=', $second)
                                // ->select(DB::raw("SELECT * FROM maintances WHERE month(maintain_date) >= :first"),array('first' => $first,))
                            // ->where('maintain_date', '<', $second)
                            // ->Where('maintain_date', '>', $first)
                            // ->where('maintain_date', '=<', $second)
                            // ->get();
                            ->toSql();

        return $maintanence_list;
    }

}

