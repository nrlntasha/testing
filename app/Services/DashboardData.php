<?php

namespace App\Services;

use App\Tree;
use DB;

class DashboardData{

  public function bilPokokTinggi(){

    $bilPokokTinggi = Tree::where('risk', 3)->count();

    return $bilPokokTinggi;

  }

  public function bilPokokSederhana(){

    $bilPokokSederhana = Tree::where('risk', 2)->count();

    return $bilPokokSederhana;

  }

  public function bilPokokRendah(){

    $bilPokokRendah = Tree::where('risk', 1)->count();

    return $bilPokokRendah;

  }

  public function log_sistem(){

    $log_sistem = DB::table('system_logs')
                    ->whereYear('created_at', '>=', '2019')
                    ->orderBy('created_at', 'ASC')
                    ->limit(100)
                    ->get();

    return $log_sistem;

  }

  public function bilPokokRenek(){

    $bilPokokRenek = Tree::where('category', 1)->count();

    return $bilPokokRenek;

  }

  public function bilPokokPalma(){

    $bilPokokPalma = Tree::where('category', 2)->count();

    return $bilPokokPalma;

  }

  public function bilPokokTeduhan(){

    $bilPokokTeduhan = Tree::where('category', 3)->count();

    return $bilPokokTeduhan;

  }

}
