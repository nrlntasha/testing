<?php

namespace App\Services;
use App\Kejur;
use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Session;

class KejurDashboard{

  public function bilAmbilalih($status){

    return Kejur::whereHas('kejurStatus', function($query) use($status){

        return $query->where('status', $status);

    })->whereBetween('kejurs.created_at', [Session::get('start_date'), Session::get('end_date')])
    ->count();

  }

  public function bilLengkapKemudahan(){

    $bilLengkapKemudahan = DB::table('kejurs')
                            ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE kemudahan='Lengkap Kemudahan' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilKemudahan"))
                            ->first();

    if(is_null($bilLengkapKemudahan)){
      $bilLengkapKemudahan = 0;
    }else{
      $bilLengkapKemudahan = $bilLengkapKemudahan->bilKemudahan;
    }

    return $bilLengkapKemudahan;

  }

  public function bilTidakKemudahan(){

    $bilTidakKemudahan = DB::table('kejurs')
                            ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE kemudahan='Tidak Lengkap Kemudahan' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilKemudahan"))
                            ->first();

    if(is_null($bilTidakKemudahan)){
      $bilTidakKemudahan = 0;
    }else{
      $bilTidakKemudahan = $bilTidakKemudahan->bilKemudahan;
    }

    return $bilTidakKemudahan;

  }

  public function bilPerwartaan(){

    $bilPerwartaan = DB::table('kejurs')
                        ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE perwartaan='1' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilWartaan"))
                        ->first();

    if(is_null($bilPerwartaan)){
      $bilPerwartaan = 0;
    }else{
      $bilPerwartaan = $bilPerwartaan->bilWartaan;
    }

    return $bilPerwartaan;

  }

  public function bilTidakPerwartaan(){

    $bilTidakPerwartaan = DB::table('kejurs')
                            ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE perwartaan='0' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilWartaan"))
                            ->first();

    if(is_null($bilTidakPerwartaan)){
      $bilTidakPerwartaan = 0;
    }else{
      $bilTidakPerwartaan = $bilTidakPerwartaan->bilWartaan;
    }

    return $bilTidakPerwartaan;

  }

  public function bilKawasanLapang(){

    $bilKawasanLapang = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE kawasan_lapang='Ya' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilKawasanLapang"))
                          ->first();

    if(is_null($bilKawasanLapang)){
      $bilKawasanLapang = 0;
    }else{
      $bilKawasanLapang = $bilKawasanLapang->bilKawasanLapang;
    }

    return $bilKawasanLapang;

  }

  public function bilBukanLapang(){

    $bilBukanLapang = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE kawasan_lapang='Tidak' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilBukanLapang"))
                          ->first();

    if(is_null($bilBukanLapang)){
      $bilBukanLapang = 0;
    }else{
      $bilBukanLapang = $bilBukanLapang->bilBukanLapang;
    }

    return $bilBukanLapang;

  }

  public function bilTaman(){

    $bilTaman = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE taman='1' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilTaman"))
                          ->first();

    if(is_null($bilTaman)){
      $bilTaman = 0;
    }else{
      $bilTaman = $bilTaman->bilTaman;
    }

    return $bilTaman;

  }

  public function bilBukanTaman(){

    $bilBukanTaman = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE taman='0' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilBukanTaman"))
                          ->first();

    if(is_null($bilBukanTaman)){
      $bilBukanTaman = 0;
    }else{
      $bilBukanTaman = $bilBukanTaman->bilBukanTaman;
    }

    return $bilBukanTaman;

  }

  public function bilPenampan(){

    $bilPenampan = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE penampan='1' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilPenampan"))
                          ->first();

    if(is_null($bilPenampan)){
      $bilPenampan = 0;
    }else{
      $bilPenampan = $bilPenampan->bilPenampan;
    }

    return $bilPenampan;

  }

  public function bilBukanPenampan(){

    $bilBukanPenampan = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE penampan='0' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilBukanPenampan"))
                          ->first();

    if(is_null($bilBukanPenampan)){
      $bilBukanPenampan = 0;
    }else{
      $bilBukanPenampan = $bilBukanPenampan->bilBukanPenampan;
    }

    return $bilBukanPenampan;

  }

  public function bilPadangBola(){

    $bilPadangBola = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE padang='1' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilPadangBola"))
                          ->first();

    if(is_null($bilPadangBola)){
      $bilPadangBola = 0;
    }else{
      $bilPadangBola = $bilPadangBola->bilPadangBola;
    }

    return $bilPadangBola;

  }

  public function bilBukanPadangBola(){

    $bilBukanPadangBola = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE padang='0' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilBukanPadangBola"))
                          ->first();

    if(is_null($bilBukanPadangBola)){
      $bilBukanPadangBola = 0;
    }else{
      $bilBukanPadangBola = $bilBukanPadangBola->bilBukanPadangBola;
    }

    return $bilBukanPadangBola;

  }

  public function bilDewan(){

    $bilDewan = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE dewan='1' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilDewan"))
                          ->first();

    if(is_null($bilDewan)){
      $bilDewan = 0;
    }else{
      $bilDewan = $bilDewan->bilDewan;
    }

    return $bilDewan;

  }

  public function bilBukanDewan(){

    $bilBukanDewan = DB::table('kejurs')
                          ->select(DB::raw("(SELECT COUNT(*) FROM kejurs WHERE dewan='0' AND created_at>='".session()->get('start_date')."' AND created_at<='".session()->get('end_date')."') as bilBukanDewan"))
                          ->first();

    if(is_null($bilBukanDewan)){
      $bilBukanDewan = 0;
    }else{
      $bilBukanDewan = $bilBukanDewan->bilBukanDewan;
    }

    return $bilBukanDewan;

  }
}
