<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\DB;

class KejurMaintenance{

  public function kejurBaik(){

    try{

      $kejurBaik = DB::table('kejurs')
                      ->where('maintance_status', 'Baik')
                      ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                      ->get();

      return $kejurBaik;

    }catch(Exception $e){
      return $e;
    }

  }

  public function kejurNaikTaraf(){

    try{

      $kejurNaikTaraf = DB::table('kejurs')
                          ->where('maintance_status', 'Naik Taraf')
                          ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                          ->get();

      return $kejurNaikTaraf;

    }catch(Exception $e){

      return $e;

    }

  }

  public function kejurSegera(){

    try{

      $kejurSegera = DB::table('kejurs')
                        ->where('maintance_status', 'Kerja Segera')
                        ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                        ->get();

      return $kejurSegera;

    }catch(Exception $e){
      return $e;
    }
  }

  public function kejurSelenggara(){

    try{
      $kejurSelenggara = DB::table('kejurs')
                            ->where('maintance_status', 'Penyelenggaran')
                            ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                            ->get();

      return $kejurSelenggara;
    }catch(Exception $e){
      return $e;
    }
  }

}
