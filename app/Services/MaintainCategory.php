<?php

namespace App\Services;

use Hamcrest\Core\HasToString;
use Illuminate\Support\Facades\DB;

class MaintainCategory{

  /**
   *
   * @return tebangPokokAkar
   * @return
   */

  public function allCategory(){

    $maintainCategory = DB::table('maintances')
                          ->groupBy('status_maintance')
                          ->get(['status_maintance']);

    return $maintainCategory;

  }

  public function perCategory(){

    $perCategory = DB::select("SELECT activity_name as status_maintance,COUNT(*) as dataCount FROM maintances JOIN maintainance_activity ON type_maintance = id GROUP BY activity_name");

    return $perCategory;

  }

  public function perZone(){

    // $perZone = DB::table('trees')
    //                 ->select('zone')
    //                 ->groupBy('zone')
    //                 ->get();

    $perZone = DB::table('trees')->selectRaw('zone, COUNT(*) as dataCount')
                                 ->join('maintances', 'maintances.reference_id', '=', 'trees.treeID')
                                 ->groupBy('zone')
                                 ->get()
                                 ->toArray();


    return $perZone;

  }

  public function perCountCategory($status){

    $perCountCategory = DB::table('maintances')
                          ->where('status_maintance', $status)
                          ->count();

    return $perCountCategory;

  }

  public function tebangPokokAkar(){

    $tebangPokokAkar = DB::table('maintances')
                            ->where('status_maintance', 'Tebang Pokok Dengan Mencabut Akar')
                            ->get();

    return $tebangPokokAkar;

  }

  public function tebangPokokTanpaAkar(){

    $tebangPokokTanpaAkar = DB::table('maintances')
                              ->where('status_maintance', 'Tebang Pokok Tanpa Mencabut Akar')
                              ->get();

    return $tebangPokokTanpaAkar;

  }

  public function pokokTumbang(){

    $pokokTumbang = DB::table('maintances')
                      ->where('status_maintance', 'Membersih Pokok Tumbang')
                      ->get();

    return $pokokTumbang;

  }

  public function bongkarTunggul(){

    $bongkarTunggul = DB::table('maintances')
                        ->where('status_maintance', 'Bongkar Tunggul')
                        ->get();

    return $bongkarTunggul;

  }

  public function merincihTunggul(){

    $merincihTunggul = DB::table('maintances')
                          ->where('status_maintance', 'Merincih Tunggul')
                          ->get();

    return $merincihTunggul;

  }

  public function cantasan(){

    $cantasan = DB::table('maintances')
                  ->where('status_maintance', 'Cantasan / Pemangkasan')
                  ->get();

    return $cantasan;

  }

  public function membersihSemakSamun(){

    $membersihSemakSamun = DB::table('maintances')
                              ->where('status_maintance', 'Membersih Semak Samun')
                              ->get();

    return $membersihSemakSamun;

  }

  public function menyiram(){

    $menyiram = DB::table('maintances')
                  ->where('status_maintance', 'Menyiram')
                  ->get();

    return $menyiram;

  }

  public function membersihMengembur(){

    $membersihMengembur = DB::table('maintances')
                            ->where('status_maintance', 'Membersih dan Mengembur')
                            ->get();

    return $membersihMengembur;

  }

  public function memangkasMenjarang(){

    $memangkasMenjarang = DB::table('maintances')
                            ->where('status_maintance', 'Memangkas dan Menjarang')
                            ->get();

    return $memangkasMenjarang;

  }

  public function mencatas(){

    $mencatas = DB::table('maintances')
                  ->where('status_maintance', 'Mencatas')
                  ->get();

    return $mencatas;

  }

  public function membaja(){

    $membaja = DB::table('maintances')
                ->where('status_maintance', 'Membaja')
                ->get();

    return $membaja;

  }

  public function menyungkup(){

    $menyungkup = DB::table('maintances')
                    ->where('status_maintance', 'Menyungkup')
                    ->get();

    return $menyungkup;

  }

  public function meracun(){

    $meracun = DB::table('maintances')
                  ->where('status_maintance', 'Meracun')
                  ->get();

    return $meracun;

  }

  public function kawasanBerumput(){

    $kawasanBerumput = DB::table('maintances')
                          ->where('status_maintance', 'Penyelenggaraan Kawasan Berumput')
                          ->get();

    return $kawasanBerumput;

  }

  public function sampahKebun(){

    $sampahKebun = DB::table('maintances')
                      ->where('status_maintance', 'Membuang Sampah Kebun')
                      ->get();

    return $sampahKebun;
  }

  public function zonPerCategory($category) {

    $zonCategory = DB::table('maintances')
                    ->join('trees', 'maintances.reference_id', '=', 'trees.treeID')
                    ->join('tree_risk', 'maintances.risk_level', '=', 'tree_risk.id')
                    ->join('maintainance_status', 'maintances.status_maintance', '=', 'maintainance_status.id')
                    ->join('maintainance_activity', 'maintances.type_maintance', '=', 'maintainance_activity.id')
                    ->where('maintainance_activity.activity_name', $category)
                    ->select('tree_risk.name as risk_level',
                            'maintainance_activity.activity_name as type_maintance',
                            'maintainance_status.status as status_maintance',
                            'trees.zone',
                            'maintances.maintain_date',
                            'maintances.maintainID')
                    ->get();

    return $zonCategory;
  }

  public function listPerZone($zone) {

    $listPerZone = DB::table('maintances')
                    ->join('trees', 'maintances.reference_id', '=', 'trees.treeID')
                    ->join('tree_risk', 'maintances.risk_level', '=', 'tree_risk.id')
                    ->join('maintainance_status', 'maintances.status_maintance', '=', 'maintainance_status.id')
                    ->join('maintainance_activity', 'maintances.type_maintance', '=', 'maintainance_activity.id')
                    ->where('trees.zone', '=', $zone)
                    ->select('tree_risk.name as risk_level',
                            'maintainance_activity.activity_name as type_maintance',
                            'maintainance_status.status as status_maintance',
                            'trees.zone',
                            'maintances.maintain_date',
                            'maintances.maintainID')
                    ->get();

    return $listPerZone;

  }

}
