<?php

namespace App\Services\Registries;

use App\Contracts\Templates\KejurTemplate;
use App\Exceptions\KejurReportTemplateNotFoundException;

class KejurReportTemplateRegistry{

  protected $templates = [];

  public function register($templateType, KejurTemplate $kejurTemplate, $override = false){

    if($this->has($templateType)){

      $this->templates[$templateType] = ($override) ? $kejurTemplate : $this->templates[$templateType];

    }else{

      $this->templates[$templateType] = $kejurTemplate;

    }

  }

  public function has($templateType){
    return isset($this->templates[$templateType]);
  }

  public function get($templateType){
    if($this->has($templateType)) return $this->templates[$templateType];

    throw new KejurReportTemplateNotFoundException($templateType);
    // throw new ReporterNotFoundException($templateType);
  }

  public function all(){
    return $this->templates;
  }

}