<?php 

namespace App\Services\Registries;

use App\Contracts\Reporter\Reporter;
use App\Exceptions\ReporterNotFoundException;

class ReporterRegistry {

  /**
   * Registered Reporter.
   * 
   * @var array
   */
  protected $reporters = [];

  /**
   * Register New Reporter.
   * 
   * @param  string   $key
   * @param  Reporter $reporter
   * @param  bool     $override
   * @return void
   */
  public function register($key, Reporter $reporter, $override = false) {
    if($this->has($key)) {
      $this->reporters[$key] = ($override) ? $reporter : $this->reporters[$key];
    }
    else 
    {
      $this->reporters[$key] = $reporter;
    }
  }

  /**
   * Determine wheteher the reporter with the given key exists.
   * 
   * @param  string $key
   * @return bool
   */
  public function has($key) {
    return isset($this->reporters[$key]);
  }

  /**
   * Get the reporter based on the given key.
   * 
   * @param  string   $key
   * @return Reporter
   * 
   * @throws ReporterNotFoundException if the reporter with the given key does not exists.
   */
  public function get($key) {
    if($this->has($key)) return $this->reporters[$key];

    throw new ReporterNotFoundException($key);
  }

  /**
   * Get all registered reporter.
   * 
   * @return array
   */
  public function all() {
    return $this->reporters;
  }

}