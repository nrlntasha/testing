<?php

namespace App\Services\Templates;

use App\Contracts\Templates\KejurTemplate;

class KejurSimplifyTemplate implements KejurTemplate {
  
  public function getTemplate(array $data = []){

    $view = view('superadmin.landskap.kejur.simplify');

    foreach($data as $var => $value) {
      $view->with($var, $value);
    }

    return $view;

  }

}