<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class TreeList{

  /**
   *
   * @return pokokRisikoRendah
   * @return pokokRisikoSederhana
   * @return pokokRisikoTinggi
   * @return pokokRenek
   * @return pokokPalma
   * @return pokokTeduhan
   * @return bilPokokPalma
   * @return bilPokokTeduhan
   * @return bilPokokRenek
   * @return bilPokokRisikoRendah
   * @return bilPokokRisikoSederhana
   * @return bilPokokRisikoTinggi
   *
   */

    // public function bilPokok() {
    //     $bilPokok = DB::table('trees')
    //                 // ->count();
    //                 ->toSql();

    //     if(is_null($bilPokok)){
    //         $bilPokok = 0;
    //     }
    //     dd($bilPokok);

    //     return $bilPokok;
    // }

  public function pokokRisikoRendah(){

    $pokokRisikoRendah = DB::table('trees')
                            ->where('risk', 1)
                            ->get();

    return $pokokRisikoRendah;

  }

  public function pokokRisikoSederhana(){

    $pokokRisikoSederhana = DB::table('trees')
                              ->where('risk', 2)
                              ->get();

    return $pokokRisikoSederhana;

  }

  public function pokokRisikoTinggi(){

    $pokokRisikoTinggi = DB::table('trees')
                            ->where('risk', 3)
                            ->get();

    return $pokokRisikoTinggi;

  }

  public function pokokRenek(){

    $pokokRenek = DB::table('trees')
                    ->where('category', 3)
                    ->get();

    return $pokokRenek;

  }

  public function pokokPalma(){

    $pokokPalma = DB::table('trees')
                    ->where('category', 2)
                    ->get();

    return $pokokPalma;

  }

  public function pokokTeduhan(){

    $pokokTeduhan = DB::table('trees')
                      ->where('category', 1)
                      ->get();

    return $pokokTeduhan;

  }

  public function bilPokokRenek(){

    $bilPokokRenek = DB::table('trees')
                        ->where('category', 3)
                        ->count();

    return $bilPokokRenek;

  }

  public function bilPokokTeduhan(){

    $bilPokokTeduhan = DB::table('trees')
                          ->where('category', 1)
                          ->count();

    return $bilPokokTeduhan;

  }

  public function bilPokokPalma(){

    $bilPokokPalma = DB::table('trees')
                        ->where('category', 2)
                        ->count();

    return $bilPokokPalma;

  }

  public function bilPokokRisikoRendah($id){

    if($id == 'Daily'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y');

    }else if($id == 'Weekly'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y', strtotime('-7 days'));

    }else if($id == 'Monthly'){

        $start_today = date('m/01/Y', strtotime('-1 month'));
        $end_today = date('m/d/Y', strtotime('-1 month'));

    }else{
        $start_today = date('m/d/Y', strtotime('first day of january this year'));
        $end_today = date('m/d/Y', strtotime('last day of december this year'));
    }

    $bilPokokRisikoRendah = DB::table('maintances')
                                ->where('risk_level', 'Rendah')
                                ->whereBetween('maintain_date', [$start_today, $end_today])
                                ->count();

    $bilMaintain = $this->maintainCount();

    if($bilPokokRisikoRendah==0){
        $percentRendah = 0.0;
    }else{
        $percentRendah = ($bilPokokRisikoRendah/$bilMaintain)*100;
    }

    return $percentRendah;

  }

  public function bilPokokRisikoSederhana($id){

    if($id == 'Daily'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y');

    }else if($id == 'Weekly'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y', strtotime('-7 days'));

    }else if($id == 'Monthly'){

        $start_today = date('m/01/Y', strtotime('-1 month'));
        $end_today = date('m/d/Y', strtotime('-1 month'));

    }else{
        $start_today = date('m/d/Y', strtotime('first day of january this year'));
        $end_today = date('m/d/Y', strtotime('last day of december this year'));
    }

    $bilPokokRisikoSederhana = DB::table('maintances')
                                ->where('risk_level', 'Sederhana')
                                ->whereBetween('maintain_date', [$start_today, $end_today])
                                ->count();

    $bilMaintain = $this->maintainCount();

    if($bilPokokRisikoSederhana==0){
        $percentSederhana = 0.0;
    }else{
        $percentSederhana = ($bilPokokRisikoSederhana/$bilMaintain)*100;
    }

    return $percentSederhana;

  }

  public function bilPokokRisikoTinggi($id){

    if($id == 'Daily'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y');

    }else if($id == 'Weekly'){

        $start_today = date('m/d/Y');
        $end_today = date('m/d/Y', strtotime('-7 days'));

    }else if($id == 'Monthly'){

        $start_today = date('m/01/Y', strtotime('-1 month'));
        $end_today = date('m/d/Y', strtotime('-1 month'));

    }else{
        $start_today = date('m/d/Y', strtotime('first day of january this year'));
        $end_today = date('m/d/Y', strtotime('last day of december this year'));
    }

    $bilPokokRisikoTinggi = DB::table('maintances')
                                ->where('risk_level', 'Tinggi')
                                ->whereBetween('maintain_date', [$start_today, $end_today])
                                ->count();

    $bilMaintain = $this->maintainCount();

    if($bilPokokRisikoTinggi==0){
        $percentTinggi = 0.0;
    }else{
        $percentTinggi = ($bilPokokRisikoTinggi/$bilMaintain)*100;
    }

    return $percentTinggi;

  }

  public function maintainCount(){

    $bilPokokRisikoTinggi = DB::table('maintances')
                                ->count();

    return $bilPokokRisikoTinggi;
  }

}
