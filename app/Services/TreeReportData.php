<?php

namespace App\Services;

use App\Tree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TreeReportData
{
    public function zoneCount(){

        $zoneCount = DB::select('SELECT COUNT(*) as zoneCount FROM (SELECT zone FROM trees WHERE created_at>="'.session()->get('start_date').'" AND created_at<="'.session()->get('end_date').'" GROUP BY zone) as zoneCount');

        return $zoneCount;

    }

    public function treeCount(){

        $treeCount = DB::table('trees')
                        ->whereBetween('created_at', [session()->get('start_date'), session()->get('end_date')])
                        ->count();

        return $treeCount;

    }

    public function locationCount(){

        $locationCount = DB::select('SELECT COUNT(*) as locationCount FROM (SELECT location FROM trees  WHERE created_at>="'.session()->get('start_date').'" AND created_at<="'.session()->get('end_date').'" GROUP BY location) as locationCount');

        return $locationCount;

    }

    public function treeType(){
        $localTreeTypes = DB::table('trees')->join('tree_species', 'species_id', '=', 'id')->select('local_name as name', DB::raw('count(*) as count'))->whereBetween('trees.created_at', [SESSION::get('start_date'), SESSION::get('end_date')])->where('local_name', '!=', '')->groupBy('local_name');
        $saintifikTreeTypes = DB::table('trees')->join('tree_species', 'species_id', '=', 'id')->select('scientific_name as name', DB::raw('count(*) as count'))->whereBetween('trees.created_at', [SESSION::get('start_date'), SESSION::get('end_date')])->where('scientific_name', '!=', '')->groupBy('scientific_name');

        /* JOINING BOTH BY USING UNION AND CONVERT COLLECTION TO ARRAY */
        $treeType = $localTreeTypes->union($saintifikTreeTypes)->get()->toArray();

        return $treeType;
    }

    public function zoneList(){

        $zoneList = DB::table('trees')
                        ->select('zone')
                        ->groupBy('zone')
                        ->get();

        return $zoneList;

    }

    public function getLocation(Request $request){

        $getLocation = DB::table('trees')
                        ->select('location')
                        ->where('zone', request()->zone)
                        ->groupBy('location')
                        ->pluck('location');

        return $getLocation;

    }

    public function getTreeType(Request $request){

        $getTreeType = DB::table('trees')
                        ->select('local_name', 'species_id')
                        ->join('tree_species', 'species_id', 'id')
                        ->where('zone', request()->zone)
                        ->where('location', request()->lokasi)
                        ->groupBy('local_name')
                        ->pluck('local_name', 'species_id');

        return $getTreeType;

    }

    public function export(Request $request){

        $risks = isset($request->risk) ? $request->risk : "0" ;
        $category = isset($request->category) ? $request->category : "0" ;
        $silara = isset($request->silera) ? $request->silera : "0" ;
        $location = isset($request->lokasi) ? $request->lokasi : "0" ;
        $name = isset($request->nama_biasa) ? $request->nama_biasa : "0" ;
        $zone = $request->zone;

        $data = Tree::with('species', 'getCategory', 'getRisk', 'getSilara')
                        ->when($request->zone != "0", function($q) use($zone){
                            $q->where('trees.zone', $zone);
                        })
                        ->when($location != "0", function($q) use($location){
                            $q->where('trees.location', $location);
                        })
                        ->when($name != "0", function($q) use($name){
                            $q->where('tree_species.local_name', $name);
                        })
                        ->when($risks != "0", function($q) use($risks){
                            $q->whereIn('trees.risk', $risks);
                        })
                        ->when($category != "0", function($q)use($category){
                            $q->whereIn('trees.category', $category);
                        })
                        ->when($silara != "0", function($q) use($silara){
                            $q->whereIn('trees.silara', $silara);
                        })
                        // ->whereBetween('a.created_at', [SESSION::get('start_date'), SESSION::get('end_date')])
                        ->get();

                        return $data;

    //     return view('superadmin.export.softScape', compact('data'));
    }

}
