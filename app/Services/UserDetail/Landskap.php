<?php

namespace App\Services\UserDetail;

use Illuminate\Support\Facades\DB;

class Landskap{

    protected $created_at;

    public function created_at(){
        return $date = date('Y-m-d H:i:s');
    }

  public function index($user){

    $landskap = DB::table('user_details')
                //   ->where('staff', $user)
                  ->get();

    return $landskap;

  }

  public function store($detail){

    $file = $detail->profile_img;

    $imgName = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
    $file->move(public_path().'/profile_image/', $imgName);


    DB::table('user_details')
        ->insert([
          'ic_number' => $detail->ic_number,
          'full_name' => $detail->full_name,
          'phone_number' => $detail->phone_number,
          'staff' => $detail->role,
          'created_at' => $this->created_at()
        ]);

    DB::table('users')
      ->insert([
        'user_id' => uniqid(),
        'username' => $detail->username,
        'password' => bcrypt($detail->password),
        'email' => $detail->email,
        'profile_img' => '/profile_image/'.$imgName,
        'ic_number' => $detail->ic_number,
        'role' => $detail->role,
        'created_at' => $this->created_at()
      ]);

    return "Berjaya Disimpan";

  }


  public function update($ic, $detail, $role){

    DB::table('user_details')
        ->where('ic_number', $ic)
        ->updateOrInsert(
          [
            'ic_number' => $ic
          ],
          [
            'ic_number' => $detail->ic_number,
            'full_name' => $detail->full_name,
            'phone_number' => $detail->phone_number,
            'staff' => $role,
            'created_at' => $this->created_at()
        ]);

    DB::table('users')
       ->where('ic_number', $ic)
       ->updateOrInsert(
           [
               'ic_number' => $ic,
               'email' => $detail->email,
               'username' => $detail->username
           ],
           [
            'username' => $detail->username,
            'password' => bcrypt($detail->password),
            'email' => $detail->email,
            'ic_number' => $detail->ic_number,
            'role' => $role,
            'created_at' => $this->created_at()
      ]);

    return "Berjaya Disimpan";

  }


  public function delete($ic_number){

    DB::table('user_details')
        ->where('ic_number', $ic_number)
        ->delete();

    DB::table('users')
      ->where('ic_number', $ic_number)
      ->delete();

    return "Berjaya Dipadam";

  }

}
