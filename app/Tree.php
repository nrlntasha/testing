<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    protected $primaryKey = 'treeID';

    protected $table = 'trees';

    public $incrementing = false;

    public $timestamps = true;

    protected $fillable = [
        'treeID',
        'species_id',
        'category',
        'risk',
        'silara',
        'height',
        'inventory_no',
        'latitude',
        'longitude',
        'location',
        'zone',
        'notes'
    ];

    protected $appends = [
        'silara_name',
        'category_name',
        'risk_level',
        'scientific_name',
        'local_name',
        'genus',
        'inventory_no'
    ];

    protected $hidden = [
        'silara',
        'risk',
        'category',
        'species_id',
        'getSilara',
        'getCategory',
        'getRisk',
        'species'
    ];

    protected $casts = [
        'height' => 'string',
    ];

    public function getCategory(){
        return $this->hasOne('App\TreeCategory', 'id', 'category');
    }

    public function getRisk(){
        return $this->hasOne(TreeRisk::class, 'id', 'risk');
    }

    public function getSilara(){
        return $this->hasOne('App\TreeSilara', 'id', 'silara');
    }

    public function media(){
        return $this->hasMany(TreeMedia::class, 'reference_id', 'treeID');
    }

    public function maintainance(){
        return $this->hasMany(Maintance::class, 'reference_id', 'treeID');
    }

    public function species(){
        return $this->hasOne(TreeSpecies::class, 'id', 'species_id');
    }

    public static function getTreeDetail(){
        return self::with('getCategory', 'getRisk', 'getSilara', 'species');
    }

    public function getSilaraNameAttribute(){
        return $this->getSilara->name;
    }

    public function getCategoryNameAttribute(){
        return $this->getCategory->category;
    }

    public function getRiskLevelAttribute(){
        return $this->getRisk->name;
    }

    public function getScientificNameAttribute(){
        return $this->species->scientific_name;
    }

    public function getLocalNameAttribute(){
        return $this->species->local_name;
    }

    public function getGenusAttribute(){
        return $this->species->genus;
    }

    public function getInventoryNoAttribute(){

        if(empty($this->attributes['inventory_no'])){
            return "MBPJ/".$this->attributes['treeID'];
        }else{
            return $this->attributes['inventory_no'];
        }
    }

}
