<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TreeCategory extends Model
{
    protected $table = 'tree_category';

    protected $fillable = [
        'category',
        'created_at',
        'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public $timestamps = true;

    public $incrementing = true;

    public function tree(){
        return $this->belongsTo(Tree::class);
    }
}
