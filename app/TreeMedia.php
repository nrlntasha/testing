<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeMedia extends Model
{
    protected $table = 'media';

    protected $fillable = ['type_id', 'reference_id', 'image_name', 'img_ext', 'tags'];

    protected $hidden = ['created_at', 'updated_at'];

    public $incrementing = false;

    public $timestamps = true;

    public $appends = ['image_link'];

    public function getImageLinkAttribute(){

        $host = request()->getSchemeAndHttpHost();

        if($this->tags == 'Sebelum Penyelenggaraan' || $this->tags == 'Maintance Sebelum'){
            return $host."/maintance/sebelum/".$this->image_name;
        }else if($this->tags == 'Selepas Penyelenggaraan' || $this->tags == 'Maintance Selepas'){
            return $host."/maintance/selepas/".$this->image_name;
        }else{
            return $host."/pokok/".$this->image_name;
        }
    }

}
