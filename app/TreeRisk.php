<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeRisk extends Model
{
    protected $table = 'tree_risk';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'created_at'];

    protected $hidden = ['created_at', 'updated_at'];

    public $timestamps = true;

    public $incrementing = true;

    public function tree(){
        return $this->belongsTo(Tree::class);
    }

    public function maintainance(){
        return $this->belongsTo(Maintance::class);
    }
}
