<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeSilara extends Model
{
    protected $table = 'tree_silara';

    protected $fillable = ['name', 'created_at'];

    protected $hidden = ['created_at', 'updated_at'];

    public $timestamps = true;

    public $incrementing = true;

    public function tree(){
        return $this->belongsTo(Tree::class);
    }
}
