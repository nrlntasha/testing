<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeSpecies extends Model
{
    protected $table = 'tree_species';

    protected $fillable = ['local_name', 'scientific_name', 'genus'];

    protected $hidden = ['created_at', 'updated_at'];

    public $incrementing = true;

    public $timestamps = true;

    public function treeSpecies()
    {
        return $this->belongsTo(Tree::class, 'species_id', 'id');
    }

    public function scopeScientificName(){
        return $this->where('scientific_name', '!=', '')->whereNotNull('scientific_name')->orderBy('scientific_name', 'ASC')->select('scientific_name as name')->get();
    }
}
