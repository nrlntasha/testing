<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{

    public $incrementing = false;

    protected $primaryKey = 'username';

    public $appends = ['image_link'];

    protected $fillable = [
        'user_id',
        'username',
        'password',
        'email',
        'ic_number',
        'role',
        'profile_img'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function details() {
        return $this->belongsTo(UserDetail::class, 'ic_number', 'ic_number');
    }

    public function onesignal()
    {
        return $this->hasMany(Notification::class, 'user_id', 'user_id');
    }

    public function getImageLinkAttribute(){

        if(!is_null($this->attributes['profile_img'])){
            $host = request()->getSchemeAndHttpHost();
            return $host.$this->attributes['profile_img'];
        }

        return null;
    }
}
