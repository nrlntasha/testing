<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';

    protected $primaryKey = 'id';

    protected $fillable = [
        'ic_number',
        'full_name',
        'phone_number',
        'staff'
    ];

    public $timestamps = true;

    public $incrementing = true;
}
