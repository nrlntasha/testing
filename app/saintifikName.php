<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saintifikName extends Model
{
    protected $table = 'saintifik_name_tree';

    protected $fillable = ['name'];

    public function scopeNotNull($q){
        $q->whereNotNull('name')->where('name', '!=', '');
    }
}
