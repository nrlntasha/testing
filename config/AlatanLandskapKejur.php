<?php
    return [
        'SET ALAT PERMAINAN BERSEPADU' => [
            0 => 'BUAIAN',
            1 => 'LANTAI EPDM/RUBBER MAT',
            2 => 'ALAT SENAMAN LUAR (OUTDOOR GYM)',
            3 => 'GELANGGANG BOLA KERANJANG',
            4 => 'GELANGGANG BADMINTON/TAKRAW',
            5 => 'MEJA PIKNIK',
            6 => 'BANGKU TAMAN',
            7 => 'TONG SAMPAH',
            8 => 'LALUAN MASUK MESRA OKU',
            9 => 'BOLLARD',
            10 => 'REFLEKSOLOGI',
            11 => 'GUARDRAIL',
            12 => 'HANDRAIL',
            13 => 'PINTU GERBANG',
            14 => 'LALUAN PEJALAN KAKI',
            15 => 'PAPAN TANDA AKTA',
            16 => 'PAPAN TANDA LARANGAN MEMANCING',
            17 => 'PAPAN TANDA PADANG CERGAS',
            18 => 'PAPAN TANDA KENYATAAN',
            19 => 'PAPAN TANDA KENYATAAN',
            20 => 'PAPAN TANDA PENGGUNAAN OUTDOOR GYM',
            21 => 'TANGGA'
        ],
        'SET PENCAHAYAAN' => [
            0 => 'SPORT LIGHT',
            1 => 'LAMPU TAMAN',
            2 => 'HIGHMASS'
        ], 'LAIN-LAIN' => [
            0 => 'PONDOK REHAT',
            1 => 'SKATEBOARD'
        ]
        ];
