<?php 

return [
  'detail' => \App\Services\Templates\KejurDetailTemplate::class,
  'test' => \App\Services\Templates\KejurTestTemplate::class,
  'simplify' => \App\Services\Templates\KejurSimplifyTemplate::class,
];