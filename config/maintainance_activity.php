<?php
    return [
        1 => [
            'Tebang Pokok Tanpa Mencabut Akar',
            'Tebang Pokok Dengan Mencabut Akar',
            'Membersih Pokok Tumbang',
            'Bongkar Tunggul',
            'Merincih Tunggul',
            'Cantasan / Pemangkasan',
            'Membersih Semak Samun',
            'Menyiram',
            'Membersih dan Mengembur',
            'Memangkas dan Menjarang',
            'Mencatas Salur Air / Dahan Rendah / Dahan Mati',
            'Membaja',
            'Menyungkup',
            'Meracun',
            'Pangakas',
            'Tebang',
            'Tumbang',
            'Patah',
            'Penyelenggaraan Kawasan Berumput',
            'Mambuang Sampah Kebun'
        ],
        2 => []
    ];
    ?>
