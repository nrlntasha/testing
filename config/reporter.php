<?php

return [
  'kejur' => \App\Services\Reporters\KejurReporter::class,
  'lembut' => \App\Services\Reporters\LembutReporter::class,
];