<?php
    return [
        0 => "Bulat / Globur",
        1 => "Bujur Songsan / Obvoid",
        2 => "Mengurai / Weeping",
        3 => "Bujur Telur / Oval",
        4 => "Silinder / Cylinder",
        5 => "Kon / Conical",
        6 => "Rampak / Irregular",
        7 => "Memanjang / Collunar",
        8 => "Mendatar / Payung / Horizontal",
    ];
