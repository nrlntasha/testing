<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('user_id', '191')->unique();
            $table->string('username', '191');
            $table->string('password', '150');
            $table->string('email', '191');
            $table->string('ic_number', '12')->unique();
            $table->foreign('ic_number')->references('ic_number')->on('user_details');
            // $table->string('full_name', '255');
            $table->string('role', '20');
            $table->string('profile_img', '191')->nullable();
            $table->boolean('is_suspended')->default(0);
            $table->timestamps();
            $table->primary(['username', 'email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
