<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Nullable;

class CreateTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trees', function (Blueprint $table) {
            $table->string('treeID', '20');
            $table->bigInteger('species_id')->unsigned();
            $table->foreign('species_id')->references('id')->on('tree_species');
            $table->string('type', '50');
            $table->integer('category')->unsigned();
            $table->foreign('category')->references('id')->on('tree_category');
            $table->double('height', '20', '2');
            $table->bigInteger('silara')->unsigned();
            $table->foreign('silara')->references('id')->on('tree_silara');
            $table->string('location', '191');
            $table->string('zone', '191');
            $table->string('inventory_no', '100')->nullable();
            $table->string('latitude', '50');
            $table->string('longitude', '100');
            $table->string('logging', '100')->nullable();
            $table->string('logging_date', '20')->nullable();
            $table->integer('risk')->unsigned();
            $table->foreign('risk')->references('id')->on('tree_risk');
            $table->string('notes', '191')->nullable();
            $table->string('created_by')->description('Username who created this a record');
            $table->foreign('created_by')->references('username')->on('users');
            $table->timestamps();
            $table->primary(['treeID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trees');
    }
}
