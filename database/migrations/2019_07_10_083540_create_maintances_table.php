<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintances', function (Blueprint $table) {
            $table->string('maintainID', '20');
            $table->string('reference_id', '20');
            $table->foreign('reference_id')->references('treeID')->on('trees');
            $table->string('username', '20');
            $table->string('img_before', '100')->nullable();
            $table->string('img_after', '100')->nullable();
            $table->integer('risk_level')->unsigned();
            $table->foreign('risk_level')->references('id')->on('tree_risk');
            $table->bigInteger('type_maintance')->unsigned();
            $table->foreign('type_maintance')->references('id')->on('maintainance_activity');
            $table->bigInteger('status_maintance')->unsigned();
            $table->foreign('status_maintance')->references('id')->on('maintainance_status');
            $table->string('maintain_date', '30');
            $table->string('notes', '191')->nullable();
            $table->timestamps();
            $table->primary(['maintainID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintances');
    }
}
