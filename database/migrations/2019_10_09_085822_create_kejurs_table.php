<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKejursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kejurs', function (Blueprint $table) {
            $table->string('kejurID');
            $table->string('no_inventori', 100)->nullable();
            $table->string('maintance_status', 100);
            $table->string('zon', 50);
            $table->string('kawasan', 100);
            $table->string('location', 100);
            $table->text('catatan')->nullable();
            $table->double('keluasan', 8, 3);
            $table->string('intake_date', 191);
            $table->integer('status_pengambilan')->unsigned();
            $table->foreign('status_pengambilan')->references('id')->on('kejur_status');
            $table->string('kemudahan', 50);
            $table->boolean('perwartaan');
            $table->string('no_perwartaan', 30)->nullable();
            $table->boolean('kawasan_lapang');
            $table->boolean('taman');
            $table->boolean('penampan');
            $table->boolean('padang');
            $table->boolean('dewan');
            $table->boolean('dijumpai');
            $table->string('created_by')->description('Username who created this a record');
            $table->foreign('created_by')->references('username')->on('users');
            $table->timestamps();
            $table->primary(['kejurID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kejurs');
    }
}
