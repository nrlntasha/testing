<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKejurMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kejur_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kejurID');
            $table->foreign('kejurID')->references('kejurID')->on('kejurs');
            $table->string('nama_alatan');
            $table->text('image_name');
            $table->string('img_ext', 20);
            $table->string('tags', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kejur_media');
    }
}
