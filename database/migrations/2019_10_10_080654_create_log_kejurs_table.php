<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogKejursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_kejurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kejurID');
            $table->foreign('kejurID')->references('kejurID')->on('kejurs');
            $table->string('nama_alatan');
            $table->string('kuantiti_baik');
            $table->string('kuantiti_rosak');
            $table->string('kuantiti_semasa');
            $table->string('username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_kejurs');
    }
}
