<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Schema::disableForeignKeyConstraints();
        $this->call([
            UsersTableSeeder::class,
            TreesTableSeeder::class,
            TreeRiskTableSeeder::class,
            TreeSilaraTableSeeder::class,
            TreeCategoryTableSeeder::class,
            TypeTableSeeder::class,
            MaintainanceStatusTableSeeder::class,
            MaintainanceActivityTableSeeder::class,
            TreeSpeciesTableSeeder::class,
            KejurStatusTableSeeder::class,
            KejursTableSeeder::class,
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
