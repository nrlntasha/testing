<?php

use Illuminate\Database\Seeder;

class KejurStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'kejur_status';

        DB::table($table)->truncate();

        $data = config('status_kejur');

        foreach($data as $val){
            DB::table($table)->insert([
                'status' => $val
            ]);
        }
    }
}
