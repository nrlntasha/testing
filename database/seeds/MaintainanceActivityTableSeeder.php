<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MaintainanceActivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'maintainance_activity';

        $datas = config('maintainance_activity');

        DB::table($table)->truncate();

        foreach($datas as $key => $values){
            if(!empty($values)){
                foreach($values as $value){
                    DB::table($table)->insert([
                        'maintainance_type_id' => $key,
                        'activity_name' => $value,
                        'created_at' => Carbon::now()
                    ]);
                }
            }
        }
    }
}
