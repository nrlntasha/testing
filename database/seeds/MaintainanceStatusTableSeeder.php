<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MaintainanceStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'maintainance_status';

        $datas = config('maintainance_status');

        DB::table($table)->truncate();

        foreach($datas as $data){
        DB::table($table)->insert([
                                'status' => $data,
                                'created_at' => Carbon::now()
                            ]);
        }
    }
}
