<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TreeCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'tree_category';

        $datas = config('tree-category');

        DB::table($table)->truncate();

        foreach($datas as $data){
            DB::table($table)->insert([
                'category' => $data,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
