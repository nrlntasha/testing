<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class TreeRiskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = config('tree-risk');
        DB::table('tree_risk')->truncate();

        foreach($datas as $data){
            DB::table('tree_risk')->insert([
                'name' => $data,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
