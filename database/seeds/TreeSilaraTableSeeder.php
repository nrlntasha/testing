<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class TreeSilaraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = config('silara');
        $table = 'tree_silara';

        DB::table($table)->truncate();

        foreach($datas as $data){
            DB::table($table)->insert([
                'name' => $data,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
