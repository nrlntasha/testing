<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TreeSpeciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'tree_species';

        $datas = config('tree_species');

        DB::table($table)->truncate();

        foreach($datas as $key => $val){
            DB::table($table)->insert([
                'local_name' => ucwords($key),
                'scientific_name' => ucwords($val),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
