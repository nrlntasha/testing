<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class TreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('trees')->insert([
            'treeID' => 'TR000000',
            'species_id' => 1,
            // 'local_name' => 'local name',
            'type' => 'test',
            'category' => 'Teduhan',
            'height' => '123',
            'silara' => 'bulat',
            'location' => 'petaling',
            'inventory_no' => '9828',
            'latitude' => '3.10667',
            'longitude' => '101.60833',
            'logging_date' => '',
            'risk' => 'sederhana',
            'notes' => 'lol',
        ]);
    }
}
