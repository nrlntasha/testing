<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'type';

        $datas = ['Pokok', 'Kejur', 'Penyelenggaraan'];

        DB::table($table)->truncate();

        foreach($datas as $data){
            DB::table($table)->insert(['type' => $data]);
        }
    }
}
