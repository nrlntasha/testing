<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserDetail;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                'user_id' => uniqid(),
                'username' => 'admin',
                'password' => bcrypt('Admin123'),
                'email' => 'admin@admin.com',
                'ic_number' => '999999',
                'role' => 'Superadmin'
            ]);
        User::create([
                'user_id' => uniqid(),
                'username' => 'landskap',
                'password' => bcrypt('Landskap123'),
                'email' => 'landskap@admin.com',
                'ic_number' => 'landskaptest',
                'role' => 'Landskap'
            ]);
        User::create([
                'user_id' => uniqid(),
                'username' => 'kejur',
                'password' => bcrypt('Kejur123'),
                'email' => 'kejur@admin.com',
                'ic_number' => 'kejurtest',
                'role' => 'Kejur'
            ]);

        UserDetail::create([
            'staff' => 'Landskap',
            'phone_number' => Str::random(10),
            'ic_number' => 'landkaptest',
            'full_name' => 'Landskap Fullname'
        ]);
        UserDetail::create([
            'staff' => 'Kejur',
            'phone_number' => Str::random(10),
            'ic_number' => 'kejurTest',
            'full_name' => 'Kejur Fullname'
        ]);
        UserDetail::create([
            'staff' => 'Admin',
            'phone_number' => Str::random(10),
            'ic_number' => '999999',
            'full_name' => 'Admin Fullname'
        ]);
    }
}
