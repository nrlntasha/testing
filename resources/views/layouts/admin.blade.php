@php
 header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> @yield('title') - e-RupaBumi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
  window.OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
        appId: "{{config('onesignal.app_id')}}",
        autoRegister: true,
        notifyButton: {
          enable: true
      }
    });

    OneSignal.on('subscriptionChange', function (isSubscribed) {

        console.log("The user's subscription state is now:", isSubscribed);

        OneSignal.push(function() {

            OneSignal.getUserId(function(userId) {
            // console.log("OneSignal User ID:", userId);

            var data = {
                "_token": "{{ csrf_token() }}",
                "onesignal_id": userId
            };
            console.log(userId);

            $.post(`superadmin/ajax/onesignal/store`, data, ((response) => {
            }));

            });

      });

    });
  });
</script>

    <!-- plugin css -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
    </link>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
        integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
        integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
        crossorigin=""></script>
    <link rel="stylesheet" href="{{ asset('vendors/leaflet-cluster/dist/MarkerCluster.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/leaflet-cluster/dist/MarkerCluster.Default.css') }}">
    <script src="{{ asset('vendors/leaflet-cluster/dist/leaflet.markercluster.js') }}"></script>
    <link href="{{ asset('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet"
        type="text/css" />
    {{--
    <link href="{{ asset('assets/libs/lightpick/lightpick.css') }}" rel="stylesheet" type="text/css" />
    --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.all.min.js"
        integrity="sha256-aakU0ciz46DahtBruJmV8isJWXw6TELTYFPcSVVcoFU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.min.css"
        integrity="sha256-Y1xbDv1PElD7e/rh4zaYBqDCZDlr95GGqybJoQO4Nww=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    @stack('datepickercss')
    @stack('lightbox')
    @stack('customPageCSS')

    <!-- Jquery Toast css -->
    <link href="/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/custom/animateDelay.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />


    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

    <!-- Include turf for view fitting -->
    <script src="https://npmcdn.com/@turf/turf/turf.min.js"></script>

    <!--  Include targomo core -->
    <script src="https://cdn.jsdelivr.net/npm/@targomo/core@0.2.19/targomo-core.umd.min.js"
        integrity="sha256-MzN8A3y4M7BKvZ717+OP2p/iuN9VA3UN7RzjsBJIm6o=" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css"
        integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous" />


    <style type="text/css" media="screen">
        .marker {
            background-image: url('marker.png');
            background-size: cover;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            cursor: pointer;
        }

    </style>

</head>

<body>

    <!-- Navigation Bar-->
    <header id="topnav">

        <!-- Topbar Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <ul class="list-unstyled topnav-menu float-right mb-0">

                    <li class="dropdown notification-list">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    <li class="animated bounceInDown slow dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#"
                            role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="pro-user-name ml-1">
                                {{ session()->get('username') }}<i class="mdi mdi-chevron-down"></i>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="m-0">
                                    Selamat Datang
                                </h5>
                            </div>

                            <!-- item-->

                            <div class="dropdown-divider"></div>

                            <a href="/settings" class="dropdown-item notify-item">
                                <i class="fe-user"></i>
                                <span>Profil</span>
                            </a>

                            <!-- item-->
                            <a href="/logout" class="dropdown-item notify-item">
                                <i class="fe-log-out"></i>
                                <span>Log Keluar</span>
                            </a>

                        </div>

                    </li>

                </ul>

                <!-- LOGO -->
                <div class="animated zoomIn slow logo-box">
                    <a href="#" class="logo text-center">
                        <span class="logo-lg">
                            <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="30">
                            <!-- <span class="logo-lg-text-dark">Xeria</span> -->
                        </span>
                        <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">X</span> -->
                            <img src="{{ asset('assets/images/logo-sm.png') }}" alt="" height="18">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

                    <li class="dropdown d-none d-lg-block">
                        <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="false" aria-expanded="false">
                            {{ session()->get('days') }}
                            <i class="mdi mdi-chevron-down"></i>
                        </a>
                        <div class="dropdown-menu">
                            <!-- item-->
                            <a href="/superadmin/date-session/today" class="dropdown-item">
                                Harian
                            </a>

                            <!-- item-->
                            {{-- <a href="/superadmin/date-session/yesterday" class="dropdown-item">
                                Kelmarin
                            </a> --}}

                            <!-- item-->
                            <a href="/superadmin/date-session/7days" class="dropdown-item">
                                Mingguan
                            </a>

                            <!-- item-->
                            <a href="/superadmin/date-session/lastmonth" class="dropdown-item">
                                Bulanan
                            </a>

                            <!-- item-->
                            <a href="#" data-target="#customDatepicker" data-toggle="modal" class="dropdown-item">
                                Custom
                            </a>

                        </div>
                    </li>

                </ul>

                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end Topbar -->

        <div class="topbar-menu">
            <div class="container-fluid">
                <div id="navigation">

                    @if(session()->get('role') == 'Superadmin' ||
                        session()->get('role') == 'Admin')
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="animated menuDashboard fadeInLeft submenu">
                                <a href="/superadmin">
                                    <i class="la la-dashboard"></i>Dashboards</a>
                            </li>

                            <li class="animated menuLandskap fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Landskap <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/lembut">Lembut</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuSelenggara fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Penyelenggaraan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/maintance-pokok">Pokok</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/maintance-kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuLaporan fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Laporan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    {{-- <li>
                                        <a href="/superadmin/report-pokok">Pokok</a>
                                    </li> --}}
                                    <li>
                                        <a href="/superadmin/report-maintainance-pokok">Penyelenggaraan Pokok</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/report-kejur">Kejur</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/log-audit">Log Audit</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuAkrib fadeInLeft has-submenu">
                                <a href="/superadmin/akrib">
                                    <i class="la la-clipboard"></i>Arkib</a>
                            </li>

                            <li class="animated duration3s fadeInLeft has-submenu">
                                <a href="/superadmin/user">
                                    <i class="la la-dashboard"></i>Pengguna</a>
                                {{-- <ul class="submenu">
                                    <li> --}}
                                        {{-- <a href="/superadmin/user?user=Landskap">Landskap</a>
                                        --}}
                                        {{-- </li> --}}
                                    {{-- <li>
                                        <a href="/superadmin/user?user=Kejur">Kejur</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/user?user=Intern">Internship</a>
                                    </li> --}}
                                    {{--
                                </ul> --}}
                            </li>

                            {{-- <li class="has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Tetapan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/tetapan/lembut/1">Lembut</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/laporan-kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li> --}}

                        </ul>
                        <!-- End navigation menu -->
                    @elseif (session()->get('role')=='Landskap')
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="animated menuDashboard fadeInLeft submenu">
                                <a href="/landskap">
                                    <i class="la la-dashboard"></i>Dashboards</a>
                            </li>

                            <li class="animated menuLandskap fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Landskap <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/landskap/lembut">Lembut</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuSelenggara fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Penyelenggaraan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/landskap/maintance-pokok">Pokok</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuLaporan fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Laporan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/landskap/category-maintain">Pokok</a>
                                    </li>
                                    <li>
                                        <a href="/landskap/log-audit">Log Audit</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuAkrib fadeInLeft has-submenu">
                                <a href="/landskap/akrib">
                                    <i class="la la-clipboard"></i>Akrib</a>
                            </li>

                        </ul>
                        <!-- End navigation menu -->
                    @elseif (session()->get('role')=='Kejur')
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="animated menuDashboard fadeInLeft submenu">
                                <a href="/kejur">
                                    <i class="la la-dashboard"></i>Dashboards</a>
                            </li>

                            <li class="animated menuLandskap fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Landskap <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/kejur/kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuSelenggara fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Penyelenggaraan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/kejur/maintance-kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuLaporan fadeInLeft has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Laporan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/kejur/report-kejur">Kejur</a>
                                    </li>
                                    <li>
                                        <a href="/kejur/log-audit">Log Audit</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuAkrib fadeInLeft has-submenu">
                                <a href="/kejur/akrib">
                                    <i class="la la-clipboard"></i>Akrib</a>
                            </li>

                        </ul>
                        <!-- End navigation menu -->
                    @else

                    @endif



                    <div class="clearfix"></div>
                </div>
                <!-- end #navigation -->
            </div>
            <!-- end container -->
        </div>
        <!-- end navbar-custom -->

    </header>
    <!-- End Navigation Bar-->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="wrapper">
        <div class="container-fluid">

            @yield('content')
            @include('superadmin.datepicker-modal')


        </div> <!-- end container -->
    </div>
    <!-- end wrapper -->

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    2019 &copy; All Right Reserve <b>MBPJ</b>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->


    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>
    {{-- @include('superadmin.datepicker-modal') --}}
    <!-- Vendor js -->
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

    <!-- Third Party js-->
    <script src="{{ asset('assets/libs/peity/jquery.peity.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    --}}
    <script src="{{ asset('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-vectormap/jquery-jvectormap-us-merc-en.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    {{-- <script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('assets/libs/lightpick/lightpick.js') }}"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts@latest"></script> --}}
    {{-- <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.17/js/bootstrap-select.min.js.map"
        integrity="sha384-9SpEmTJk41ng2+uscU+8zt2T2nW2tBpFou8PTPzcronjBLM6oUoM/6vUbf06pC3p" crossorigin="anonymous">
    </script> --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"
        integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>

    <!-- Tost-->
    <script src="/assets/libs/jquery-toast/jquery.toast.min.js"></script>

    <!-- toastr init js-->
    <script src="/assets/js/pages/toastr.init.js"></script>

    <!-- Init js-->
    <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

    {{-- <script src="{{ asset('assets/js/pages/apexcharts.init.js') }}"></script> --}}
    @stack('chartjs')
    @stack('datepicker')
    @stack('lightboxscript')
    @stack('autocomplete')
    @stack('datatables')
    @stack('apexchart')
    @stack('searchZone')
    @stack('customPageJS')
    <!-- Datatables init -->
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>

    <!-- Modal-Effect -->
    <script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

    <!-- Dashboard init -->
    {{-- <script src="{{ asset('assets/js/pages/dashboard-1.init.js') }}"></script> --}}

    <!-- App js-->
    <script src="{{ asset('assets/js/app.min.js') }}"></script>

</body>

</html>
