<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>- Smart PJ Landskap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
          window.OneSignal = window.OneSignal || [];
          OneSignal.push(function() {
            OneSignal.init({
              appId: "{{config('onesignal.app_id')}}",
              allowLocalhostAsSecureOrigin: true,
              autoRegister: true,
              notifyButton: {
                  enable: false
              }
            });
          });
        </script>

        <!-- plugin css -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
        <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>
        <link rel="stylesheet" href="{{ asset('vendors/leaflet-cluster/dist/MarkerCluster.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/leaflet-cluster/dist/MarkerCluster.Default.css') }}">
        <script src="{{ asset('vendors/leaflet-cluster/dist/leaflet.markercluster.js') }}"></script>
        <link href="{{asset('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/libs/custombox/custombox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.all.min.js" integrity="sha256-aakU0ciz46DahtBruJmV8isJWXw6TELTYFPcSVVcoFU=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.min.css" integrity="sha256-Y1xbDv1PElD7e/rh4zaYBqDCZDlr95GGqybJoQO4Nww=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

        @stack('datepickercss')
        @stack('lightbox')
        @stack('customPageCSS')

        <link href="{{asset('assets/css/custom/animateDelay.css')}}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        <li class="animated bounceInDown slow dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <span class="pro-user-name ml-1">
                                        {{ session()->get('username') }}<i class="mdi mdi-chevron-down"></i>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5 class="m-0">
                                            Welcome !
                                        </h5>
                                    </div>

                                    <!-- item-->

                                    <div class="dropdown-divider"></div>

                                    <!-- item-->
                                    <a href="/logout" class="dropdown-item notify-item">
                                        <i class="fe-log-out"></i>
                                        <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="animated zoomIn slow logo-box">
                        <a href="#" class="logo text-center">
                            <span class="logo-lg">
                                <img src="{{asset('assets/images/logo-dark.png')}}" alt="" height="30">
                                <!-- <span class="logo-lg-text-dark">Xeria</span> -->
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-sm-text-dark">X</span> -->
                                <img src="{{asset('assets/images/logo-sm.png')}}" alt="" height="18">
                            </span>
                        </a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="animated menuDashboard fadeInLeft submenu">
                                <a href="/superadmin">
                                <i class="la la-dashboard"></i>Dashboards</a>
                            </li>

                            <li class="animated menuLandskap fadeInLeft has-submenu">
                                <a href="#">
                                <i class="la la-dashboard"></i>Landskap <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/lembut">Lembut</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuSelenggara fadeInLeft has-submenu">
                                <a href="#">
                                <i class="la la-dashboard"></i>Penyelenggaraan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/maintance-pokok">Pokok</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuLaporan fadeInLeft has-submenu">
                                <a href="#">
                                <i class="la la-dashboard"></i>Laporan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/category-maintain">Pokok</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/log-audit">Log Audit</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="animated menuAkrib fadeInLeft has-submenu">
                                <a href="/superadmin/akrib">
                                <i class="la la-clipboard"></i>Arkib</a>
                            </li>

                            {{-- <li class="has-submenu">
                                <a href="#">
                                <i class="la la-dashboard"></i>Tetapan <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/superadmin/tetapan/lembut/1">Lembut</a>
                                    </li>
                                    <li>
                                        <a href="/superadmin/laporan-kejur">Kejur</a>
                                    </li>
                                </ul>
                            </li> --}}

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                @yield('content')

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        2019 &copy; All Right Reserve <b>MBPJ</b>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->


        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('assets/js/vendor.min.js')}}"></script>

        <!-- Third Party js-->
        <script src="{{asset('assets/libs/peity/jquery.peity.min.js')}}"></script>
        <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
        <script src="{{asset('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('assets/libs/jquery-vectormap/jquery-jvectormap-us-merc-en.js')}}"></script>
        <script src="{{asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts@latest"></script>

        <!-- Init js-->
        <script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>
        {{-- <script src="{{asset('assets/js/pages/apexcharts.init.js')}}"></script> --}}
        @stack('chartjs')
        @stack('datepicker')
        @stack('lightboxscript')
        @stack('autocomplete')
        @stack('datatables')
        @stack('apexchart')
        @stack('searchZone')
        @stack('customPageJS')
        <!-- Datatables init -->
        <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>

        <!-- Modal-Effect -->
        <script src="{{asset('assets/libs/custombox/custombox.min.js')}}"></script>

        <!-- Dashboard init -->
        <script src="{{asset('assets/js/pages/dashboard-1.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('assets/js/app.min.js')}}"></script>

    </body>
</html>
