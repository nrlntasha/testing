<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>MBPJ</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="Smart Tree MBPJ" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css" rel="stylesheet')}}" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

        <style>

            body{
                background: linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) ), url("{{asset('assets/images/83940.jpg')}}");
                -webkit-background-size: cover;
                -moz-background-size:  cover;
                -o-background-size: cover;
                background-size: cover;
            }

        </style>

    </head>
<body class="">
    {{-- <body class="authentication-bg authentication-bg-pattern"> --}}

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{asset('assets/images/mbpj.png')}}" alt="" height="150"></span>
                                    </a>
                                    <h3 class="text-bold">e-Rupabumi@PJ</h3>
                                    <p class="text-muted mb-4 mt-3">Enter your IC number and password to access admin panel.</p>
                                </div>

                                <h5 class="auth-title">Sign In</h5>
                                @if ($errors -> count() > 0)
                                    <div class="alert alert-danger bg-danger text-white border-0" role="alert">
                                        <strong>{{ $errors->getBag('default')->first() }}</strong> {{-- Not Match !! --}}
                                    </div>
                                @endif
                                <form action="/" class="needs-validation" method="post">
                                    @csrf
                                    <div class="loginForm">
                                        <div class="form-group mb-3 md-form">
                                            <label for="ic_number">IC Number</label>
                                            <input class="form-control" type="text" id="ic_number" name="ic_number" placeholder="Enter your IC number" autocomplete="off" required>
                                            <input type="hidden" id="old_ic" name="old_ic" value="{{ old('ic_number') }}">
                                        </div>

                                        <div class="form-group mb-3 md-form">
                                            <label for="password">Password</label>
                                            <input class="form-control" type="password" name="password" id="password" placeholder="Enter your password" autocomplete="off" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-success btn-block" type="submit"> Log In </button>
                                    </div>

                                </form>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            2019 &copy; All Right Reserve <b>MBPJ</b>
        </footer>

        <!-- Vendor js -->
        <script src="{{asset('assets/js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('assets/js/app.min.js')}}"></script>

    </body>
</html>
