@extends('layouts.admin')

@section('title', 'Settings')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Setting</a></li>
                </ol>
            </div>
            <h4 class="page-title">Ubah Pengguna</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

@if($errors->any())
  <div class="animated pulse infinite alert alert-danger bg-danger text-white border-0" role="alert">
    @foreach($errors->getMessages() as $this_error)
        !!! {{$this_error[0]}}
    @endforeach
  </div>
@endif
        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pengguna {{ $user->role }}</u></h4>
                <hr>
                <form class="needs-validation" action="/superadmin/settings" method="POST" novalidate>
                  @csrf
                    <div class="form-row">

                        <input type="hidden" value="{{ encrypt(optional($user)->ic_number) }}" name="_id" id=""/>

                        <div class="form-group col-md-4">
                            <label for="full_name">Nama Penuh</label>
                            <input type="text" class="form-control" name="full_name" id="full_name" value="{{ optional($userDetails)->full_name }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ic_number">Nombor IC</label>
                            <input type="text" class="form-control" name="ic_number" id="ic_number" value="{{ optional($user)->ic_number }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="phone_number">Nombor Bimbit</label>
                            <input type="text" class="form-control" name="phone_number" id="phone_number" value="{{ optional($userDetails)->phone_number }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" value="{{ optional($user)->email }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username" value="{{ optional($user)->username }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="password">Kata Laluan</label>
                            <button type="button" class="btn btn-outline-info waves-effect waves-light form-control" id="password" data-toggle="modal" data-target="#updatePassword">Ubah Kata Laluan</button>
                            {{-- <input type="password" class="form-control" name="password" id="password" required> --}}
                        </div>

                    </div>

                    <button type="submit" name="submit" class="btn btn-outline-success waves-effect waves-light">Perbarui</button>
                    <button type="button" onclick="history.go(-1); return false;" class="btn btn-outline-danger waves-effect waves-light">Batal</button>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="labelHeader" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelHeader">Kemaskini Kata Laluan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="/update-password" method="POST">
            @csrf
            <div class="form-group validate">
                <label for="validate" class="col-form-label">Kata Laluan Lama:</label>
                <input type="password" class="form-control" id="validate" name="validate" placeholder="*********">
                <button type="button" id="validateBtn" class="btn btn-primary waves-effect waves-light mt-2" onclick="updatePassword('validate')">
                    Sahkan Kata Laluan
                </button>
                <small class="text-danger wrong">Sila Semak Semula Kata Laluan Anda</small>
                <button class="btn btn-primary mt-2" id="loadingBtn" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Sedang Memuat</button>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Kata Laluan Baharu:</label>
                    <input type="password" class="form-control" id="update" placeholder="*********">
                </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary" id="updateBtn" onclick="updatePassword('update')" disabled>Kemas Kini</button>
          <button class="btn btn-primary" id="loadingBtn2" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Sedang Memuat</button>
        </div>
      </div>
    </div>
  </div>
<!-- end row-->
@endsection

@push('customPageJS')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
    <script>
        $("#loadingBtn2").hide();
        $("#loadingBtn").hide();
        $(".wrong").hide();

        updatePassword = (type) => {

            var input = $("#"+type).val();

            checkPassword('{{csrf_token()}}', type, input);

        }

        checkPassword = (csrfToken, type, password) => {

            var body = {
                "_token": csrfToken,
                "type": type,
                "password": password
            };

            if(type == 'validate'){
                $("#loadingBtn").show();
                $("#validateBtn").hide();
                $(".wrong").hide();
            }else{
                $("#updateBtn").hide();
                $("#loadingBtn2").show();
            }

            $.post('settings/password', body, ((response) => {

                if(response.status == "Verified"){
                    // if verified

                    $("."+type).children().prop('disabled', true);
                    // $("#validateBtn").hide();
                    $("#updateBtn").prop('disabled', false);
                    $("#loadingBtn").hide();
                    $(".wrong").hide();
                    $('.collapse').collapse('show');

                }else if(response.status == 'Updated'){
                    // Updated

                    $("#updateBtn").show();
                    $("#loadingBtn2").hide();
                    $('.modal').modal('hide');
                    setTimeout(function() {
                        Swal.fire({
                            title: "Kata Laluan Berjaya Dikemaskini.",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }, 500);

                }else{
                    // wrong password
                    $("#loadingBtn").hide();
                    $("#validateBtn").show();
                    $('.wrong').show();
                }

            }));
        }

        $('.modal').on('hidden.bs.modal', function(e)
        {
            $("#validateBtn").show();
            $('.collapse').collapse('hide');
            $('.modal input').val('');
            $(".validate").children().prop('disabled', false);

        }) ;

    </script>
@endpush
