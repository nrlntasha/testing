@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Akrib</a></li>
                </ol>
            </div>
            <h4 class="page-title">Akrib</h4>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">

    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Borang</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th style="width:100px;">Action</th>
                        </tr>
                    </thead>


                    <tbody>

                        <tr>
                            <td>Borang Arahan Kerja Catasan</td>
                            <td>
                                <button type="button" name="1.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                            </td>
                        </tr>
                        <tr>
                          <td>Jadual Kerja-Kerja Selenggara</td>
                          <td>
                              <button type="button" name="2.pdf" onclick="window.location.href='download/jadual_kerja_kerja_selenggara.pdf'" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Laporan Kerja Harian</td>
                          <td>
                              <button type="button" name="3.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Borang Lawatan 15 Hari - 30 Hari</td>
                          <td>
                              <button type="button" name="4.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Borang Inventori</td>
                          <td>
                              <button type="button" name="5.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Borang Penalti</td>
                          <td>
                              <button type="button" name="6.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Laporan Bergambar Bagi Kerja Penyelenggaraan Landskap Rutin</td>
                          <td>
                              <button type="button" name="7.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Laporan Bergambar Bagi Kerja Penyelenggaraan PS</td>
                          <td>
                              <button type="button" name="8.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Borang Penilaian Prestasi Kontraktor</td>
                          <td>
                              <button type="button" name="8.pdf" class="btn btn-outline-info waves-effect waves-light">Download</button>
                          </td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->


</div>

<script>
(function() {
    $('#andre').click(function(e) {
        e.preventDefault()
        window.location.href = '/file/file.pdf';
    })

    $('[type=button]').click(function(e) {
        e.preventDefault()
        window.location.href = '/file/' + $(this).attr('name')
    })
})()
</script>

<!-- end row-->

@endsection
