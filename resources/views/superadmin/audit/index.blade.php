@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Log Audit</a></li>
                </ol>
            </div>
            <h4 class="page-title">Log Aktiviti</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Log</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Pengguna</th>
                            <th>Aktiviti</th>
                            <th>Kategori</th>
                            <th>Masa</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($log_audits as $log_audit)
                        <tr>
                            <td>{{ $log_audit -> username }}</td>
                            <td>{{ $log_audit -> comments }}</td>
                            <td>{{ $log_audit -> category }}</td>
                            <td>{{ $log_audit -> created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <script>
                    $('.deleteDevice').click(function() {
                        var treeID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-pokok/" + treeID;
                            }
                        })
                    });
                </script> --}}
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
</div>
<!-- end row-->
@endsection
