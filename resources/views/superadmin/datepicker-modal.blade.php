<link href="{{asset('assets/libs/lightpick/lightpick.css')}}" rel="stylesheet" type="text/css" />

<div class="modal fade" id="customDatepicker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Custom Date</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Tarikh yang dipilih : <span id="dateStr"></span>
            <input type="text" autocomplete="off" class="form-control form-control-sm" id="customDate"/>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-primary" id="btnPilih">Pilih</button>
        </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/libs/lightpick/lightpick.js')}}"></script>

<script>
    var dateStart = new Date;
    var dateEnd = new Date;

    var picker = new Lightpick({
        field: $('#customDate')[0],
        singleDate: false,
        selectForward: true,
        onSelect: function(start, end){
            var str = '';
            dateStart = start;
            dateEnd = end;

            str += start ? start.format('DD/MM/YYYY') + ' hingga ' : '';
            str += end ? end.format('DD/MM/YYYY') : '...';
            document.getElementById('dateStr').innerHTML = str;
        }
    });

    $("#btnPilih").on("click", function(){

        var body = {
            "_token": "{{ csrf_token() }}",
            "dateStart": dateStart.toString(),
            "dateEnd": dateEnd.toString()
        };

        $.post("{{ route('custom-date') }}", body, ((result) => {
            if(result.status == "success"){
                location.reload();
            }
        }));
    });
</script>
