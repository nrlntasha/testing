<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laporan Landskap Lembut</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style>
            body {
                margin: 20px;
                font-family: Helvetica;
                }
        </style>
    </head>
    <body>

        <div class="text-center">
            <img src="{{asset('assets/images/mbpj.png')}}" width="12%">
            <p>JABATAN LANDSKAP</p>
            <p>MAJLIS BANDARAYA PETALING JAYA</p>
            <p>LAPORAN INVENTORI LANDSKAP KEJUR BAGI JABATAN LANDSKAP MBPJ</p>
            <p>BAGI JABATAN LANDSKAP MBPJ</p>
        </div>
        <table class="table table-bordered table-condensed table-striped p">
            <tr>
                <th class="text-center">Zon</th>
                <th class="text-center">Zon Penampan</th>
                <th class="text-center">Kemaskini</th>
            </tr>
            <tr>
                <td class="text-center align-middle" rowspan="2" v-align="center">{{$detail->location}}</td>
                <td>NO. INVENTORI: {{$detail->no_inventori}}</td>
                <td class="text-center align-middle" rowspan="2">{{date('d/m/Y', strtotime($detail->updated_at))}}</td>
            </tr>
            <tr>
                <td>KELUASAN (EKAR): {{$detail->keluasan}}</td>
            </tr>
        </table>
        <table class="table table-bordered table-condensed table-striped p">
            <tr>
                <th colspan="6" class="text-center">ITEM</th>
            </tr>
            <tr class="text-center">
                <th class="text-center" rowspan="2">BIL</th>
                <th class="text-center" rowspan="2">BUTIRAN PERALATAN</th>
                <th class="text-center" colspan="3">STATUS INVENTORI</th>
                <th class="text-center" rowspan="2">CATATAN</th>
            </tr>
            <tr>
                <th class="text-center">BAIK</th>
                <th class="text-center">ROSAK</th>
                <th class="text-center">JUMLAH</th>
            </tr>
            @foreach($kejurProps as $key => $value)
                <tr>
                    <th colspan="6" class="text-center">{{$key}}</th>
                </tr>
                @foreach($value as $item)
                    <tr>
                        <td class="text-center">{{$iteration++}}</td>
                        <td>{{$item}}</td>
                        <td>{{$items->where('nama_alatan', $item)->pluck('kuantiti_baik')->first() ? : 0}}</td>
                        <td>{{$items->where('nama_alatan', $item)->pluck('kuantiti_rosak')->first() ? : 0}}</td>
                        <td>{{$items->where('nama_alatan', $item)->pluck('kuantiti_semasa')->first() ? : 0}}</td>
                        <td></td>
                    </tr>
                @endforeach
            @endforeach
            <tr>
                <td colspan="2">JUMLAH KESELURUHAN</td>
                <td>{{$items->sum('kuantiti_baik')}}</td>
                <td>{{$items->sum('kuantiti_rosak')}}</td>
                <td>{{$items->sum('kuantiti_semasa')}}</td>
                <td></td>
            </tr>
        </table>
    </body>
    <script>
        window.print();
    </script>
</html>
