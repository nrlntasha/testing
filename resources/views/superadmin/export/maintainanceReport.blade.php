<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Penyelenggaraan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="col-md-12">
            <p class="h6 text-right">MBPJ-MPK(U/PP)-01.L02</p>
        </div>
        <div class="col-md-12">
            <h5 class="text-center">JABATAN LANDSKAP</h5>
            <h5 class="text-center">MAJLIS BANDARAYA PETALING JAYA</h5>
        </div>
        <div class="col-md-12 mt-3">
            <h5 class="text-center">LAPORAN BERGAMBAR BAGI KERJA-KERJA PENYELENGGARAAN LANDSKAP RUTIN</h5>
        </div>

        <div class="row mt-5">
            {{-- <div class="col-md-12"> --}}
                <div class="col-md-4">
                    <p>Tajuk</p>
                </div>
                <div class="col-xs-1"><p>:</p></div>
                <div class="col-md-7">
                    <p>CADANGAN PERKHIDMATAN MENYELENGGARA LANDSKAP DAN KERJA-KERJA
                    BERKAITAN UNTUK MAJLIS BANDARAYA PETALING JAYA BAGI TEMPOH 2 TAHUN + 1
                    TAHUN</p>
                </div>
            {{-- </div> --}}
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Zon</p>
            </div>
            <div class="col-xs-1"><p>:</p></div>
            <div class="col-md-7">
                <p class="mt-1">{{$data->getTree->zone}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>No. Kontrak</p>
            </div>
            <div class="col-xs-1">
                <p>:</p>
            </div>
            <div class="col-md-7" style="border-bottom:dotted;">
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-md-4">
                <p>Kontraktor</p>
            </div>
            <div class="col-xs-1">
                <p>:</p>
            </div>
            <div class="col-md-7" style="border-bottom:dotted;">
            </div>
        </div>
        <div class="row  mt-1">
            <div class="col-md-4">
                <p>Tarikh Mula</p>
            </div>
            <div class="col-xs-1">
                <p>:</p>
            </div>
            <div class="col-md-7" style="border-bottom:dotted;">
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-md-4">
                <p>Tarikh Lawatan Tapak</p>
            </div>
            <div class="col-xs-1">
                <p>:</p>
            </div>
            <div class="col-md-7">
                <p class="mt-1">{{date('d/m/Y', strtotime($data->created_at))}}</p>
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-md-4">
                <p>Tarikh Tamat</p>
            </div>
            <div class="col-xs-1">
                <p>:</p>
            </div>
            <div class="col-md-7" style="border-bottom:dotted;">
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <p class="text-center">Gambar</p>
            </div>
            @foreach($data->images as $image)
            <div class="col-md-5 mx-auto">
                    {{-- <div class="p-3"> --}}
                @if ($loop->first)
                    {{-- This is the first iteration --}}
                    <img src="{{asset('/maintance/sebelum/'.$image -> image_name)}}" width="85%" alt="">
                {{-- </div> --}}
                <p class="text-center">Gambar Sebelum</p>
                @else
                    <img src="{{asset('/maintance/selepas/'.$image -> image_name)}}" width="85%" alt="">
                {{-- </div> --}}
                <p class="text-center">Gambar Selepas</p>
                @endif
            </div>&emsp;

            @endforeach
            {{-- <div class="col-md-6"></div> --}}
        </div>
        <div class="row mt-2">
            <div class="col-sm-2">
                <p>Lokasi</p>
            </div>
            <div class="col-xs-1">:</div>
            <div class="col-md-2" style="border-bottom: dotted;">{{$treeDetail->location}}</div>
            <div class="col-sm-2">
                <p class="float-right">Tarikh</p>
            </div>
            <div class="col-xs-1">:</div>
            <div class="col-md-2" style="border-bottom: dotted;"></div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-2">
                <p>Jenis Kerja</p>
            </div>
            <div class="col-xs-1">:</div>
            <div class="col-md-9 ml-3">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table-bordered">
                                <tbody>
                                    @foreach($activityList as $activity)
                                        <tr>
                                            @if($loop->iteration <= 11)
                                            {{-- <td>{{$loop->iteration}}</td> --}}
                                                <td width="90%">{!!$activity!!}</td>
                                                <td width="10%" class="text-center">{{$data->maintainance_activity == $activity ?"/": null}}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table-bordered">
                                <tbody>
                                    @foreach($activityList as $activity)
                                    <tr>
                                        @if($loop->iteration > 11)
                                            <td width="90%">{!!$activity!!}</td>
                                            <td width="10%" class="text-center">{{$data->maintainance_activity == $activity ?"/": null}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
