<!DOCTYPE html>
<html lang="en">

<head>
    <title>Cetak Laporan</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        table,
        th,
        td {
            border: 1px solid black;
        }

        .rotate {

            transform: rotate(-90deg);

            /* Legacy vendor prefixes that you probably don't need... */

            /* Safari */
            -webkit-transform: rotate(-90deg);

            /* Firefox */
            -moz-transform: rotate(-90deg);

            /* IE */
            -ms-transform: rotate(-90deg);

            /* Opera */
            -o-transform: rotate(-90deg);

            /* Internet Explorer */
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

        }

    </style>
</head>

<body>
    @php
    $i = 0;
    @endphp
    <div class="container-fluid">
        <div class="text-center">
            <img src="{{asset('assets/images/mbpj.png')}}" width="12%">
            <p>JABATAN LANDSKAP</p>
            <p>MAJLIS BANDARAYA PETALING JAYA</p>
            <p>LAPORAN STATISTIK PENYELENGGARAAN TAHUNAN POKOK</p>
            {{-- <p>{{date('d/m/Y', strtotime(session()->get('start_date')))}} HINGGA {{date('d/m/Y', strtotime(session()->get('end_date')))}}</p>
            <p>BAGI JABATAN LANDSKAP MBPJ</p> --}}
        </div>
        <div class="row">
            <div class="col-12 py-3">
                <div class="table-responsive">
                    <table class="table-bordered table-striped text-center" style="width: 100% !important;padding: 0 1rem !important;">
                        <thead>
                            <tr>
                                <th class="text-left" rowspan="2">AKTIVITI PENYELENGGARAAN</th>
                                <th colspan="24" class="text-center">ZON</th>
                                <th rowspan="2">JUMLAH</th>
                                <th class="text-center" rowspan="2">JUMLAH KESELURUHAN BAGI TAHUN {{$year}}</th>
                            </tr>
                            <tr>
                                @foreach ($zones as $zone)
                                    {!! "<th width='3%'>$zone</th>" !!}
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($activities as $activity)
                            @php
                                $sum = 0;
                            @endphp
                                <tr>
                                    <td class="text-left">{{ $activity->activity_name }}</td>
                                    @foreach ($zones2 as $item)
                                        @if(isset($count[$item]))
                                                <td>
                                                    @php
                                                        if(isset($count[$item][$activity->activity_name])){

                                                            $sum += $count[$item][$activity->activity_name];
                                                            echo $count[$item][$activity->activity_name];

                                                        }else{

                                                            echo "0";

                                                        }
                                                    @endphp
                                                </td>
                                        @else
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                    <td>{{$sum}}</td>
                                    <td>
                                        @php
                                            if(isset($maintenance[$activity->activity_name])){

                                                $sum += $maintenance[$activity->activity_name];
                                                echo $maintenance[$activity->activity_name];

                                            }else{

                                                echo "0";

                                            }
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <footer class="float-right mt-5">
            <p>Dicetak Pada:&emsp;{{date('d/m/Y')}}</p>
          </footer>
    </div>
</body>

</html>
