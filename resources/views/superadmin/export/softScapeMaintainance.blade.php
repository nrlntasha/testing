<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laporan Landskap Lembut</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style>
            body {
                margin: 20px;
                font-family: Helvetica;
                }
        </style>
    </head>
    <body>

        <div class="text-center">
            <img src="{{asset('assets/images/mbpj.png')}}" width="12%">
            <p>JABATAN LANDSKAP</p>
            <p>MAJLIS BANDARAYA PETALING JAYA</p>
            <p>LAPORAN LANDSKAP LEMBUT DARI</p>
            <p>{{date('d/m/Y', strtotime(session()->get('start_date')))}} HINGGA {{date('d/m/Y', strtotime(session()->get('end_date')))}}</p>
            <p>BAGI JABATAN LANDSKAP MBPJ</p>
        </div>
        <table class="table table-bordered table-condensed table-striped p">
            <tr class="text-center">
                <th>BIL</th>
                <th style="width: 30%">NAMA BIASA</th>
                <th>NO INVENTORI</th>
                <th style="width: 20%">LOKASI</th>
                <th>ZON</th>
                <th>TARAF RISIKO</th>
            </tr>
            @foreach($data as $row)
            <tr class="text-capitalize">
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$row->species->local_name}}</td>
                <td>{{$row->inventory_no}}</td>
                <td>{{$row->location}}</td>
                <td>{{$row->zone}}</td>
                <td>{{$row->getRisk->name}}</td>
            </tr>
            @endforeach
        </table>
    </body>
    <script>
        window.print();
    </script>
</html>
