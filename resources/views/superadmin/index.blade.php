@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">

    <div class="col-md-4 col-xl-4">
        <div class="card-box">
            <div class="row">
                <div class="col-10">
                    <div class="text-left">
                        <p class="text-muted mb-1 text-truncate">Bilangan Pokok</p>
                        <h3 class="text-dark my-1">
                            <span data-plugin="counterup">
                                @if (is_null($treeCount))
                                    0
                                @else
                                    {{ $treeCount -> treeCount }}
                                @endif
                            </span>
                        </h3>
                    </div>
                </div>

                <div class="col-1">
                    <div class=" my-1 text-right avatar-sm bg-soft-success rounded">
                        <i class="fe-feather avatar-title font-22 text-success"></i>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card-box">
            <div class="row">
                <div class="col-10">
                    <div class="text-left">
                        <p class="text-muted mb-1 text-truncate">Bilangan Selenggara Pokok</p>
                        <h3 class="text-dark my-1">
                            <span data-plugin="counterup">
                                @if (is_null($tainCount))
                                    0
                                @else
                                    {{ $tainCount -> tainCount }}
                                @endif
                            </span>
                        </h3>
                    </div>
                </div>

                <div class="col-1">
                    <div class=" my-1 text-right avatar-sm bg-soft-success rounded">
                        <i class="fe-bookmark avatar-title font-22 text-success"></i>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>


    <div class="col-md-4 col-xl-4">
        <div class="card-box">
            <div class="row">
                <div class="col-10">
                    <div class="text-left">
                        <p class="text-muted mb-1 text-truncate">Bilangan Selenggara Kejur</p>
                        <h3 class="text-dark my-1">
                            <span data-plugin="counterup">
                                @if (is_null($hardCount))
                                    0
                                @else
                                    {{ $hardCount -> hardCount }}
                                @endif
                            </span>
                        </h3>
                    </div>
                </div>

                <div class="col-1">
                    <div class=" my-1 text-right avatar-sm bg-soft-success rounded">
                        <i class="fe-gitlab avatar-title font-22 text-success"></i>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>

    {{-- <h4 class="page-title">{{ __("Statistik") }}</h4> --}}

    <div class="animated zoomInLeft duration1s col-xl-12">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs" id="bologna-list" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#treeCount" role="tab" aria-controls="treeCount" aria-selected="true" onclick="updateGraphData('tree')">Bilangan Pokok Mengikut Zon</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#maintainanceCount" role="tab" aria-controls="maintainanceCount" aria-selected="false" onclick="updateGraphData('maintainance')">Bilangan Penyelenggaraan Pokok</a>
                </li>
                </ul>
            </div>
            <div class="card-body"><button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#updateGraphModal">
                Tapis
                </button>
                {{-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" onclick="updateGraph('')">Semua</a>
                @foreach($month as $key => $val)
                    <a class="dropdown-item" onclick="updateGraph({{$key + 1}})" href="#">{{$val}}</a>
                @endforeach
                </div> --}}
                <div class="tab-content">
                    <div class="tab-pane" id="treeCount" role="tabpanel" aria-labelledby="treeCount-tab">
                        <div class="card-widgets row align-items-center">
                            <div class="dropdown mr-2">

                            </div>
                            <div><a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a></div>
                        </div>
                    </div>


                    <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                        {{-- <div class="chartjs-chart">
                            <canvas id="bilanganPokok"></canvas>
                        </div> --}}
                        <div id="chart" class="pt-3 px-0">
                            {{-- <span>kahkahkah</span> --}}
                            <center><div class="spinner-border text-primary"></div></center>
                        </div>
                    </div> <!-- collapsed end -->
                    {{-- <div class="tab-pane" id="maintainanceCount" role="tabpanel" aria-labelledby="maintainanceCount-tab">
                        erognajgna;ejneajkgner;jg;fjenrsgsnrhjotnhloakwengare
                        hawegaewhershsrserkhgm
                        ekrlsgmskelgmslerhkms
                        rehm
                        skrhlsrhms
                        rkhmkrsltnh
                        sklrthklrthm
                        lkthns
                        lkthnsrklthnskrthnrtskhnsktrnh
                        krmtkftrsmh
                        hkmglts
                        rimh
                    </div> --}}
                </div> <!-- End tab-content -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    {{-- <div class="animated zoomInRight duration1500ms col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Bilangan Pokok Mengikut Kategori</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart2"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col--> --}}

    <div class="col-xl-12">
        <div class="card-box" style="height:600px;" id="map">
            <div id="progress">
                <div id="progress-bar"></div>
            </div>
            <div></div>
        </div>
    </div>

    @if (session()->get('role')=='Superadmin' || session()->get('role') == 'Admin')
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Log Audit</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Pengguna</th>
                            <th>Aktiviti</th>
                            <th>Kategori</th>
                            <th>Masa</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($log_audits as $log_audit)
                        <tr>
                            <td>{{ $log_audit -> username }}</td>
                            <td>{{ $log_audit -> comments }}</td>
                            <td>{{ $log_audit -> category }}</td>
                            <td>{{ $log_audit -> created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <script>
                    $('.deleteDevice').click(function() {
                        var treeID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-pokok/" + treeID;
                            }
                        })
                    });
                </script> --}}
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>


    <div class="col-xl-12">
        <!-- Portlet card -->
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-0">100 Aktiviti Lepas</h4>

                <div id="cardCollpase3" class="collapse pt-3 show">
                    <div class="slimscroll" style="max-height: 350px;">
                        <div class="timeline-alt">

                            {{-- <div class="timeline-item">
                                <i class="timeline-icon"></i>
                                <div class="timeline-item-info">
                                    <a href="#" class="text-body font-weight-semibold mb-1 d-block">You sold an item</a>
                                    <small>Paul Burgess just purchased “Xeria - Admin Dashboard”!</small>
                                    <p>
                                        <small class="text-muted">5 minutes ago</small>
                                    </p>
                                </div>
                            </div> --}}

                            @foreach ($log_sistem as $logs)
                                <div class="timeline-item">
                                    <i class="timeline-icon"></i>
                                    <div class="timeline-item-info">
                                        <a href="#" class="text-body font-weight-semibold mb-1 d-block">{{ $logs->comments }}</a>
                                        <p>
                                            <small class="text-muted">{{ $logs->username}} / </small>
                                            <small class="text-muted">{{ $logs->category}}</small>
                                        </p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <!-- end timeline -->
                    </div> <!-- end slimscroll -->
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->
    @endif



</div>

<div class="modal fade" id="updateGraphModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel">Tapisan Mengikut Bulan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
                {{-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Filter
                </button> --}}
                {{-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> --}}
                <button class="btn btn-primary m-1" onclick="updateGraph('')">Semua</button>
                @foreach($month as $key => $val)
                    <button class="btn btn-primary m-1" onclick="updateGraph({{$key + 1}})"  data-dismiss="modal">{{$val}}</button>
                @endforeach
                </div>

        </div>
      </div>
    </div>
  </div>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>
@if(session()->has('success'))
    <script>
        setTimeout(function() {
            Swal.fire({
                title: "{{ session()->get('success') }}",
                type: "success",
                timer: 2000,
                showConfirmButton: false
            });
        }, 500);
    </script>
@endif
@push('customPageJS')
<script>

var BASE_URL = window.location.origin;

    @if(session()->has('error'))
        $.toast({
            heading: 'Error',
            icon: 'error',
            text: '{{ session("error") }}',
            position: 'top-right',
            loaderBg: '#bf441d',
            stack: false,
            hideAfter: 3000
        })
    @endif

    Chart.defaults.LineWithLine = Chart.defaults.line;
    Chart.controllers.LineWithLine = Chart.controllers.line.extend({
        draw: function(ease) {
            Chart.controllers.line.prototype.draw.call(this, ease);

            if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
                var activePoint = this.chart.tooltip._active[0],
                    ctx = this.chart.ctx,
                    x = activePoint.tooltipPosition().x,
                    topY = this.chart.legend.bottom,
                    bottomY = this.chart.chartArea.bottom;

                // draw line
                ctx.save();
                ctx.beginPath();
                ctx.moveTo(x, topY);
                ctx.lineTo(x, bottomY);
                ctx.lineWidth = 1;
                ctx.strokeStyle = '#808080';
                ctx.stroke();
                ctx.restore();
            }
        }

    });

    /* INITIATE GRAPH PROPERTIES id=chart */
    var options = {
            series: [{
                name: "Bilangan Pokok",
                data: []
            }],
            title: {
                text: "Bilangan Pokok Mengikut Zon",
                align: 'center',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false,
                style: {
                fontSize:  '18px',
                fontWeight:  'bold',
                fontFamily:  undefined,
                color:  '#263238'
                },
            },
            chart: {
                type: 'bar',
                height: 1000
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                    barHeight: '80%'
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: [],
                title: {
                    text: 'JUMLAH',
                    style:{
                        fontSize:  '22px',
                    },
                }

            },
            yaxis: {
                title: {
                    text: 'ZON',
                    style: {
                        fontSize:  '22px'
                    },
                }
            },
            noData: {
                text: "Tiada Maklumat",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    // color: undefined,
                    fontSize: '18px',
                    // fontFamily: undefined
                }
            }
        };

    /* FETCH API FOR FIRST LOAD */
    $.get('/graph/dashboard?category=tree&month=', (data, status) => {
        var cat = [];
        data.zone.forEach(element => {
            var res = element.toString().split(" ");
            res.length > 1 ? cat.push(res) : cat.push(res[0]);
        });
        options.series[0].data = data.value;
        options.xaxis.categories = cat;
    }).then(() => {
        $('#chart').empty();
        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
    });


    var lnglat = [101.597189, 3.144837]
    var tgm = new tgm.TargomoClient('asia', 'ROOLQGL0Y02SHLW29OFN235445622');

    var tgmOptions = {
        travelType: 'walk',
        travelEdgeWeights: [1800], // 30 minutes only
        maxEdgeWeight: 1800,
        edgeWeight: 'time',
        srid: 4326,
        simplify: 200,
        serializer: 'geojson',
        buffer: 0.002
    };


    var tgmSources = [
        { lat: lnglat[1], lng: lnglat[0], id: 1 }
    ]



    mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ';
    var map = new mapboxgl.Map({
        container: 'map',
        center:[101.597189, 3.144837],
        style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 11
    });

    map.loadImage('{{url("/marker.png")}}', function(error, image) {
        if (error) throw error;
        if (!map.hasImage('unclustered-marker')) map.addImage('unclustered-marker', image);
    });

    $.ajax({
        type: 'GET',
        url : BASE_URL+"/coordinate",
        success:function(response){
            var progress = document.getElementById('progress');
            var progressBar = document.getElementById('progress-bar');

            function updateProgressBar(processed, total, elapsed, layersArray) {
                if (elapsed > 1000) {
                    // if it takes more than a second to load, display the progress bar:
                    progress.style.display = 'block';
                    progressBar.style.width = Math.round(processed/total*100) + '%';
                }

                if (processed === total) {
                    // all markers processed - hide the progress bar:
                    progress.style.display = 'none';
                }
            }

            // Build GeoJSON object.
            var dataSource = {
                type: 'FeatureCollection',
                features: []
            }

            response.forEach(function(d, i) {


                var content =  '<div class="container-fluid"><table class="table table-bordered">';
                        content += '<tr><td>Nama</td><td> : </td><td>'+ (!d.local_name ? '-' : d.local_name ) +'</td></tr>';
                        content += '<tr><td>Nama Saintifik</td><td> : </td><td>'+ (!d.saintifik_name ? '-' : d.saintifik_name ) +'</td></tr>';
                        content += '<tr><td>Jenis</td><td> : </td><td>'+ (!d.type ? '-' : d.type) +'</td></tr>';
                        content += '<tr><td>Kategori</td><td> : </td><td>'+ (!d.category ? '-' : d.category) +'</td></tr>';
                        content += '<tr><td>Tinggi</td><td> : </td><td>'+ (!d.height ? '-' : d.height ) +'</td></tr>';
                        content += '<tr><td>Silara</td><td> : </td><td>'+ (!d.silara ? '-' : d.silara )+'</td></tr>';
                        content += '<tr><td>Lokasi</td><td> : </td><td>'+ (!d.location || d.location == "" ? '-' : d.location ) +'</td></tr>';
                        content += '<tr><td>Zon</td><td> : </td><td>'+ (!d.zone ? '-' : d.zone) +'</td></tr>';
                        content += '<tr><td>Latitude</td><td> : </td><td>'+ (!d.latitude ? '-' : d.latitude) +'</td></tr>';
                        content += '<tr><td>Longitude</td><td> : </td><td>'+ (!d.longitude ? '-' : d.longitude) +'</td></tr>';
                content += '</table></div>';

                d.html = content;

                dataSource.features[i] = {
                    type: 'Feature',
                    geometry: {
                        type: 'point',
                        coordinates: [
                            d.longitude, d.latitude,
                        ]
                    },
                    properties: d
                }

            })

            map.on('load', function() {

                $.getJSON('https://cors-anywhere.herokuapp.com/http://polygons.openstreetmap.fr/get_geojson.py?id=8347386&params=0', function(d) {

                    var inverse  = turf.difference(
                        turf.bboxPolygon([-180, 80, 180, -80]), // pretty much the inhabited world
                        turf.polygon(d.geometries[0].coordinates[0])
                    )

                    map.addLayer({
                        'id': 'polygons',
                        'type': 'fill',
                        'source': {
                            'type': 'geojson',
                            'data': inverse
                        },
                        'layout': {},
                        'paint': {
                            'fill-color': '#000',
                            'fill-opacity': .3,
                            'fill-outline-color': '#cd0000',
                            'fill-antialias': true
                        }
                    });
                })



                map.on('mouseenter', 'resiko-tinggi-cluster', function() {
                    map.getCanvas().style.cursor = 'pointer';
                });
                map.on('mouseleave', 'resiko-tinggi-cluster', function() {
                    map.getCanvas().style.cursor = '';
                });

                map.on('mouseenter', 'resiko-rendah-cluster', function() {
                    map.getCanvas().style.cursor = 'pointer';
                });
                map.on('mouseleave', 'resiko-rendah-cluster', function() {
                    map.getCanvas().style.cursor = '';
                });

                map.on('mouseenter', 'resiko-sederhana-cluster', function() {
                    map.getCanvas().style.cursor = 'pointer';
                });
                map.on('mouseleave', 'resiko-sederhana-cluster', function() {
                    map.getCanvas().style.cursor = '';
                });

                map.on('mouseenter', 'unclustered-point', function() {
                    map.getCanvas().style.cursor = 'pointer';
                });
                map.on('mouseleave', 'unclustered-point', function() {
                    map.getCanvas().style.cursor = '';
                });

                // Resiko comparison
                var resikoTinggi     = ['==', ['get', 'risk'], 3]
                var resikoSederhana  = ['==', ['get', 'risk'], 2]
                var resikoRendah     = ['==', ['get', 'risk'], 1]

                // Add the data source as mapbox data source
                map.addSource('trees', {
                    type: 'geojson',
                    data: dataSource,
                    cluster: true,
                    clusterProperties: {
                        'tinggi': ['+', ['case', resikoTinggi, 1, 0]],
                        'sederhana': ['+', ['case', resikoSederhana, 1, 0]],
                        'rendah': ['+', ['case', resikoRendah, 1, 0]],
                    }
                })

                // Add resiko tinggi cluster
                map.addLayer({
                    id: 'resiko-tinggi-cluster',
                    type: 'circle',
                    source: 'trees',
                    filter: [
                        'all',
                        ['>', ['get', 'tinggi'], 1]
                    ],
                    paint: {
                        'circle-color': '#FF5D7F',
                        'circle-radius': [
                            'step',
                            ['get', 'point_count'],
                            20,
                            100,
                            30,
                            750,
                            40
                        ]
                    }
                })


                // Add resiko sederhana cluster
                map.addLayer({
                    id: 'resiko-sederhana-cluster',
                    type: 'circle',
                    source: 'trees',
                    filter: [
                        'all',
                        ['>', ['get', 'sederhana'], 1]
                    ],
                    paint: {
                        'circle-color': '#FB9834',
                        'circle-radius': [
                            'step',
                            ['get', 'point_count'],
                            20,
                            100,
                            30,
                            750,
                            40
                        ]
                    }
                })


                // Add resiko rendah cluster
                map.addLayer({
                    id: 'resiko-rendah-cluster',
                    type: 'circle',
                    source: 'trees',
                    filter: [
                        'all',
                        ['>', ['get', 'rendah'], 1]
                    ],
                    paint: {
                        'circle-color': '#4AC0C0',
                        'circle-radius': [
                            'step',
                            ['get', 'point_count'],
                            20,
                            100,
                            30,
                            750,
                            40
                        ]
                    }
                })

                map.addLayer({
                    id: 'cluster-count',
                    type: 'symbol',
                    source: 'trees',
                    filter: ['has', 'point_count'],
                    layout: {
                        'text-field': '{point_count_abbreviated}',
                        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                        'text-size': 12,
                    },
                    paint: {
                        'text-color': '#fff'
                    }
                });

                map.addLayer({
                    id: 'unclustered-point',
                    type: 'symbol',
                    source: 'trees',
                    filter: ['!', ['has', 'point_count']],
                    layout: {
                        'icon-image': 'unclustered-marker',
                        'icon-size': 0.15
                    },
                });

                map.on('click', function(e) {
                    var features = map.queryRenderedFeatures(e.point, {
                        layers: ['unclustered-point'] // replace this with the name of the layer
                    });

                    if (!features.length) {
                        return;
                    }

                    var feature = features[0];

                    var popup = new mapboxgl.Popup({ offset: [0, -40] })
                        .setLngLat(feature.geometry.coordinates)
                        .setHTML(feature.properties.html)
                        .addTo(map);
                });

            })
        }
    });


    /* VARIABLE FOR GRAPH */
    var graphData = "tree";
    var graphMonth;

    /* CHANGE DATA COUNT FUNCTION */
    updateGraphData = (category) => {

        graphData = category;

        switch(category){
            case 'tree':
                var title = "Bilangan Pokok Mengikut Zon";
                var seriesName = "Bilangan Pokok";
            break;
            case 'maintainance':
                var title = "Bilangan Penyelenggaraan Mengikut Zon"
                var seriesName = "Bilangan Penyelenggaraan";
        }

        /* RENAME TITLE AND LABEL */
        options.title.text = title;
        options.series[0].name = seriesName;

        /* RUN BELOW FUNCTION */
        updateGraph();
    };

    updateGraph = (month) => {

        /* UNDEFINED CHECK */
        if(typeof month == 'undefined'){
            graphMonth = null;
        }else{
            graphMonth = `month=${month}`;
        }

        /* AJAX TO GET DATA FROM API */
        $.get(`/graph/dashboard?category=${graphData}&${graphMonth}`, (data, status) => {

            var cat = [];
            data.zone.forEach(element => {
                var res = element.toString().split(" ");
                res.length > 1 ? cat.push(res) : cat.push(res[0]);
            });
            options.series[0].data = data.value;
            options.xaxis.categories = cat;

        }).then(() => {
            /* PROMISES AFTER RESPONSE HAS BEEN SUCCESSFULLY RETURNED & RENDER CHART */
            $('#chart').empty();
            var chart = new ApexCharts(document.querySelector("#chart"), options);
            chart.render();

        });
    };

    $('#bologna-list a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

</script>
@endpush
@endsection
