@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Kawasan Lapang</u></h4>
                <div class="form-row">

                    <div class="form-group col-md-4">
                        <label for="location">Lokasi</label>
                        <input type="text" class="form-control" id="location" value="{{ $kejur -> location }}" readonly>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="inventory_no">No Inventory</label>
                        <input type="text" class="form-control" id="inventory_no" value="{{ $kejur -> no_inventori }}" readonly>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="acres">Keluasan (Ekar)</label>
                        <input type="number" class="form-control" id="acres" value="{{ $kejur -> keluasan }}" readonly>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="kategori">Tahap Keutamaan</label>
                        <input type="text" class="form-control" value="{{ $kejur -> maintance_status }}" readonly>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="intake_date">Tarikh Pengambilan</label>
                        <input type="date" class="form-control" value="{{ $kejur -> intake_date }}" readonly>
                    </div>


                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="lokasi">Catatan</label>
                    <textarea id="textarea" class="form-control" readonly>{{ $kejur -> catatan }}</textarea>
                    </div>
                </div>

                @if (session()->get('role')=='Superadmin')
                    <button type="button" onclick="window.location.href='/superadmin/kejur/update/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                    <button type="button" onclick="window.location.href='/superadmin/kejur/gambar/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini Peralatan</button>
                @elseif (session()->get('role')=='Kejur')
                    <button type="button" onclick="window.location.href='/kejur/kejur/update/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                    <button type="button" onclick="window.location.href='/kejur/kejur/gambar/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini Peralatan</button>
                @else

                @endif

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection

@push('customPageJS')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
@endpush
