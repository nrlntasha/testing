@push('customPageCSS')
    <!-- Lightbox css -->
    <link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Maklumat Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Maklumat Kejur</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">

    <div class="col-12">
        <div class="card-box">
            <!-- Logo & title -->
            <div class="clearfix">
                <div class="float-left">
                    <img src="{{asset('assets/images/mbpj.png')}}" alt="" height="8%" width="8%">
                </div>
                <div class="float-right">
                    <h4 class="m-0 d-print-none">Maklumat</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="mt-3">
                        <p><b>Lokasi : {{ $kejur->location }}</b></p>
                        <p class="text-muted">Catatan : {{ $kejur->catatan }}. </p>
                    </div>

                </div><!-- end col -->
            </div>
            <!-- end row -->

            <div class="row mt-3">
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>No. Inventori : </strong> {{ $kejur->no_inventori }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Keluasan (Ekar) : </strong> {{ $kejur->keluasan }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Tarikh Ambilalih : </strong> {{ $kejur->intake_date }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>No. Perwartaan : </strong> {{ $kejur->no_perwartaan }}</p>
                </div> <!-- end col -->

            </div>
            <!-- end row -->

            <hr>

            <div class="row">

                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Ambilalih : </strong> {{ $kejur->status }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Kawasan Lapang : </strong> {{ $kejur->kawasan_lapang ? 'Ya' : 'Tidak' }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Taman : </strong> {{ $kejur->taman ? 'Ya' : 'Tidak'}}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Kawasan Penampan : </strong> {{ $kejur->penampan ? 'Ya' : 'Tidak' }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Padang Bola : </strong> {{ $kejur->padang ? 'Ya' : 'Tidak' }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Dewan : </strong> {{ $kejur->dewan ? 'Ya' : 'Tidak' }}</p>
                </div> <!-- end col -->
                <div class="col-sm-3">
                    <p class="m-b-10"><strong>Dijumpai : </strong> {{ $kejur->dijumpai ? 'Ya' : 'Tidak' }}</p>
                </div> <!-- end col -->
            </div>

            <hr>

            <div class="row">

                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="header-title">Senarai Alatan</h4>

                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Alatan</th>
                                    <th>Keadaan Baik</th>
                                    <th>Keadaan Rosak</th>
                                    <th>Kuantiti Semasa</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $i = 1; ?>
                                  @foreach ($log_kejurs as $log_kejur)
                                    <tr>
                                      <th scope="row">{{ $i }}</th>
                                        <td>{{ $log_kejur->nama_alatan }}</td>
                                        <td class="text-success">{{ $log_kejur->kuantiti_baik }}</td>
                                        <td class="text-danger">{{ $log_kejur->kuantiti_rosak }}</td>
                                        <td>{{ $log_kejur->kuantiti_semasa }}</td>
                                    </tr>
                                    <?php $i++; ?>
                                  @endforeach
                                </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div> <!-- end col -->

    @foreach ($kejur_medias as $key=>$medias)
        <div class="col-md-12">
            <div class="card-box">
                <h3>{{ $key }}</h3>
                <hr>
                <div class="popup-gallery">
                    <div class="row">
                        @foreach ($medias as $media)
                            <div class="col-3">
                                <a href="{{url('/kejur/'.$media -> image_name)}}" title="{{ $key }}">
                                    <div class="img-responsive">
                                        <img src="{{url('/kejur/'.$media -> image_name)}}" alt="" class="img-fluid">
                                    </div>
                                </a>
                            </div> <!-- end col-->
                        @endforeach
                    </div>
                    <!-- end row-->

                </div> <!-- end .popup-gallery-->

            </div> <!-- end card-box-->
        </div> <!-- end col -->
    @endforeach
</div>
@endsection

@push('customPageJS')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
@endpush
