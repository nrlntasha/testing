@push('datepickercss')
    <link href="{{asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('customPageCSS')
    <!-- Lightbox css -->
    <link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Kawasan Lapang</u></h4>
                <form class="needs-validation" action="/superadmin/kejur/upload/{{ $kejur -> kejurID }}" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="location">Lokasi</label>
                            <input type="text" class="form-control" id="location" value="{{ $kejur -> location }}" readonly>
                            <div class="invalid-feedback">
                                Isikan location.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inventory_no">No Inventory</label>
                            <input type="text" class="form-control" id="inventory_no" name="inventory_no" value="{{ $kejur -> no_inventori }}" readonly>
                            <div class="invalid-feedback">
                                Isikan no inventory.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="acres">Keluasan (Ekar)</label>
                            <input type="number" class="form-control" id="acres" name="acres" value="{{ $kejur -> keluasan }}" readonly>
                            <div class="invalid-feedback">
                                Isikan keluasan.
                            </div>
                        </div>

                    </div>
                    <hr>
                    <h4 class="header-title"><u>Gambar Kawasan</u></h4>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="nama_alatan">Nama Alatan</label>
                            {{-- <input type="text" class="form-control" id="nama_alatan" name="nama_alatan" placeholder="Nama Alatan" required> --}}
                            <select class="selectpicker form-control" data-live-search="true" data-size="5" name="nama_alatan" required>
                                <option>-- Sila Pilih --</option>
                                @foreach($properties as $key => $values)
                                    <optgroup label="{{$key}}">
                                        @foreach($values as $value)
                                            <option value="{{$value}}">{{$value}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Nama Alatan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kuantiti_baik">Keadaan Baik</label>
                            <input type="number" min="0" class="form-control" id="kuantiti_baik" name="kuantiti_baik" placeholder="Kuantiti Baik" required>
                            <div class="invalid-feedback">
                                Kuantiti Baik
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kuantiti_rosak">Keadaan Rosak</label>
                            <input type="number" min="0" class="form-control" id="kuantiti_rosak" name="kuantiti_rosak" placeholder="Kuantiti Rosak" required>
                            <div class="invalid-feedback">
                                Kuantiti Rosak.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kuantiti_semasa">Kuantiti Semasa</label>
                            <input type="number" min="0" class="form-control" id="kuantiti_semasa" name="kuantiti_semasa" placeholder="Kuantiti Semasa" required>
                            <div class="invalid-feedback">
                                Kuantiti Semasa.
                            </div>
                        </div>

                        <div class="form-group col-md-12" >
                            <input id="gambar_alatan" type="file" name="gambar_alatan[]" onchange="preview_image();" multiple/>
                        </div>

                        {{-- <div class="form-group col-md-12">
                            <label for="example-fileinput">Gambar Alatan</label>
                            <input type="file" id="example-fileinput" name="gambar_alatan" class="form-control-file" requried>
                            <div class="invalid-feedback">
                                Gambar Alatan
                            </div>
                        </div> --}}



                        {{-- <input name="gambar_alatan[]" type="file" multiple /> --}}

                        {{-- <div class="form-group col-md-12">
                            <div class="fallback">
                                <input name="gambar_alatan[]" type="file" multiple />
                            </div>

                            <div class="dz-message needsclick">
                                <p class="h1 text-muted"><i class="mdi mdi-cloud-upload"></i></p>
                                <h3>Klik atau Letak Gambar Disini.</h3>
                            </div>

                            <div class="dropzone-previews mt-3" id="file-previews"></div>

                            <div class="d-none" id="uploadPreviewTemplate">
                                <div class="card mt-1 mb-0 shadow-none border">
                                    <div class="p-2">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
                                                <img data-dz-thumbnail class="avatar-sm rounded bg-light" alt="">
                                            </div>
                                            <div class="col pl-0">
                                                <a href="javascript:void(0);" class="text-muted font-weight-bold" data-dz-name></a>
                                                <p class="mb-0" data-dz-size></p>
                                            </div>
                                            <div class="col-auto">
                                                <!-- Button -->
                                                <a href="" class="btn btn-link btn-lg text-muted" data-dz-remove>
                                                    <i class="mdi mdi-close-circle"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> --}}
                   </div>
                   <div class="form-row">
                        <div class="form-group col-md-12" id="image_preview"></div>
                   </div>

                    <button class="btn btn-primary" type="submit">Tambah Alatan</button>
                    @if (session()->get('role')=='Superadmin')
                        <button type="button" class="btn btn-success" onclick="window.location.href='/superadmin/kejur'">Selesai</button>
                    @elseif (session()->get('role')=='Kejur')
                        <button type="button" class="btn btn-success" onclick="window.location.href='/kejur/kejur'">Selesai</button>
                    @else

                    @endif

                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Alatan</h4>
                <button class="btn btn-success mb-2" id="export-btn">Simpan Sebagai PDF</button>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Alatan</th>
                            <th>Baik</th>
                            <th>Rosak</th>
                            <th>Kuantiti</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($log_kejurs as $log_kejur)
                            <tr>
                                <td>{{ $log_kejur -> nama_alatan }}</td>
                                <td>{{ $log_kejur -> kuantiti_baik }}</td>
                                <td>{{ $log_kejur -> kuantiti_rosak }}</td>
                                <td>{{ $log_kejur -> kuantiti_semasa }}</td>
                                <td>
                                    <button type="button" id="padam" data-name="{{ $log_kejur -> nama_alatan }}" data-id="{{ $log_kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light padamGambarKejur">Padam</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    $('.padamGambarKejur').click(function() {
                        // var treeID = $(this).data('id');
                        var button = document.getElementById('padam');
                        var kejurID = button.getAttribute('data-id');
                        var alat = button.getAttribute('data-name');
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!',
                            cancelButtonText: 'Batal'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/kejur/deleteGambar/" + kejurID + "?id=" + alat;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

    @foreach ($kejur_medias as $key=>$medias)
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="header-title">{{ $key }}</h4>
                <hr>
                <div class="popup-gallery">
                    <div class="row">
                        @foreach ($medias as $media)
                            <div class="col-3">
                                <a href="{{url('/kejur/'.$media -> image_name)}}" title="{{ $key }}">
                                    <div class="img-responsive">
                                        <img src="{{url('/kejur/'.$media -> image_name)}}" alt="" class="img-fluid">
                                    </div>
                                </a>
                            </div> <!-- end col-->
                        @endforeach
                    </div>
                    <!-- end row-->

                </div> <!-- end .popup-gallery-->

            </div> <!-- end card-box-->
        </div> <!-- end col -->
    @endforeach
</div>
<!-- end row-->


@endsection

@push('autocomplete')
    <script src="{{asset('assets/js/dropzone.js')}}"></script>
    <script>
        function preview_image()
        {
            var total_file=document.getElementById("gambar_alatan").files.length;
            for(var i=0;i<total_file;i++)
            {
                $('#image_preview').append("<img height='40%' width='30%' src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }
    </script>
@endpush

@push('customPageJS')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>

    <script>
    $('input').keyup(function(){ // run anytime the value changes
        var firstValue  = Number($('#kuantiti_baik').val());   // get value of field
        var secondValue = Number($('#kuantiti_rosak').val()); // convert it to a float

        document.getElementById('kuantiti_semasa').value = firstValue + secondValue;
    // add them and output it
    });
    $("#export-btn").on("click", function(){
        window.open('{{route('kejur.export', $kejur -> kejurID)}}');
    });
    </script>
@endpush
