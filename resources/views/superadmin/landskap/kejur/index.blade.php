@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Kejur</h4>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">

    {{-- <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                    <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Penyelenggaraan Kejur</h4>

                <div id="cardCollpase5" class="collapse pt-3 show" dir="ltr">
                    <div id="chart" class="apex-charts"></div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col--> --}}

    <div class="animated flipInY duration1s col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Ambilalih</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="animated flipInY duration1500ms col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Kemudahan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart2"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="animated flipInY duration2s col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Perwartaan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart3"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <hr/>

    <div class="animated infinite pulse col-12">
        @if (session()->get('role')=='Superadmin')
            <button href="" onclick="window.location.href='/superadmin/kejur/new'" type="button" class="btn btn-block btn-success waves-effect waves-light">Tambah Penyelenggaraan Kejur</button>
        @elseif (session()->get('role')=='Kejur')
            <button href="" onclick="window.location.href='/kejur/kejur/new'" type="button" class="btn btn-block btn-success waves-effect waves-light">Tambah Penyelenggaraan Kejur</button>
        @else

        @endif

        <hr>
    </div>

    <div class="col-12">

        @if(session()->has('success'))
            <script>
                setTimeout(function() {
                    Swal.fire({
                        title: "{{ session()->get('success') }}!",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }, 500);
            </script>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors as $error)
                {{ $error }}
            @endforeach
        @endif

        <div class="animated slideInRight duration3s card">
            <div class="card-body">
                <h4 class="header-title">Senarai Kejur {{ session()->get('days') }}</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurs as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> no_inventori }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Papar"><i class="fas fa-eye"></i></button>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Kemaskini"><i class="fas fa-edit"></i></button>
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur" data-toggle="tooltip" title="Padam"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->

        <div class="animated slideInRight duration3s card">
            <div class="card-body">
                <h4 class="header-title">Senarai Kejur</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurss as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> no_inventori }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Papar"><i class="fas fa-eye"></i></button>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Kemaskini"><i class="fas fa-edit"></i></button>
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur" data-toggle="tooltip" title="Padam"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    $('.deleteKejur').click(function() {
                        var kejurID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Padam Semua Maklumat ini, Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/kejur/delete/" + kejurID;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->


</div>
<?php
    // constructor accepts all the formats from strtotime function
    $startdate = new DateTime('first day of january this year');
    // without a value it returns current date
    $enddate = new DateTime('last day of december this year');

    // all possible formats for DateInterval are in manual
    // but basically you need to start with P indicating period
    // and then number of days, months, seconds etc
    $interval = new DateInterval('P3M');
    $chart_data = '';
    while($startdate < $enddate){

        $periodstart = clone $startdate;
        $startdate->add($interval);
        $periodend = clone $startdate;
        $chart_data .="'".$periodstart->format('Y-m')."',";

    }
    $chart_data = substr($chart_data, 0, -1);
?>
<!-- end row-->
@push('apexchart')

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>

<script>
    var ctx = document.getElementById('myChart');
    var canvas = document.getElementById('myChart');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilAmbilalih ?>, <?php echo $bilBelumAmbilalih ?>, <?php echo $bilTidakAmbilalih ?>],
                backgroundColor: ['#1DB2E3', '#24DBB1', '#FED54B']
                // backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)', 'rgba(255,0,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Telah Ambilalih',
            'Belum Ambilalih',
            'Tidak Ambilalih'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },

        responsive: true
    };

    // For a pie chart
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    var ctx = document.getElementById('myChart2');
    var canvas = document.getElementById('myChart2');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilLengkapKemudahan ?>, <?php echo $bilTidakKemudahan ?>],
                backgroundColor: ['#1DB2E3', '#24DBB1', '#FED54B']
                // backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Lengkap Kemudahan',
            'Tidak Lengkap Kemudahan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },

        responsive: true
    };

    // For a pie chart
    var myChart2 = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    var ctx = document.getElementById('myChart3');
    var canvas = document.getElementById('myChart3');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilPerwartaan ?>, <?php echo $bilTidakPerwartaan ?>],
                backgroundColor: ['#1DB2E3', '#24DBB1', '#FED54B']
                // backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Lengkap Kemudahan',
            'Tidak Lengkap Kemudahan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },

        responsive: true
    };

    // For a pie chart
    var myChart3 = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });
</script>

@endpush

@endsection
