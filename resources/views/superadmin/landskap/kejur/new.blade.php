@push('datepickercss')
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Kejur</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Kawasan Lapang</u></h4>
                <form class="needs-validation" action="/superadmin/kejur/store" method="POST" novalidate>
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="location">Lokasi</label>
                            <input type="text" class="location form-control" name="location" value="{{ old('Location') }}" placeholder="Location" autocomplete="off" required>
                            <div class="invalid-feedback">
                               Isikan location.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="zon">Zon</label>
                            <input type="text" class="zone form-control" name="zon" value="{{ old('zon') }}" placeholder="Zon" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan Zon.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="acres">Keluasan (Ekar)</label>
                            <input type="number" step="0.001" class="form-control" id="acres" name="keluasan" value="{{ old('acres') }}" placeholder="Keluasan (Ekar)" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan keluasan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kategori">Tahap Keutamaan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="maintance_status" required>
                                <option value="1">Baik</option>
                                <option value="2">Naik Taraf</option>
                                <option value="3">Kerja Segera</option>
                                <option value="4">Penyelenggaran</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih kategori.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="intake_date">Tarikh Pengambilan</label>
                            <input type="date" class="form-control" name="intake_date" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                        <div class="col-12"><hr></div>

                        <div class="form-group col-md-4">
                            <label for="inventory_no">Status Ambilalih</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio1" name="status_pengambilan" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="customRadio1">Telah Diambilalih</label>
                        </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="customRadio2" name="status_pengambilan" value="2" class="custom-control-input" required>
                                <label class="custom-control-label" for="customRadio2">Belum Diambilalih</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="customRadio3" name="status_pengambilan" value="3" class="custom-control-input" required>
                                <label class="custom-control-label" for="customRadio3">Tidak Akan Diambilalih</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no">Status Kemudahan</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="kemudahan1" name="kemudahan" value="Lengkap Kemudahan" class="custom-control-input" required>
                                <label class="custom-control-label" for="kemudahan1">Lengkap Kemudahan</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="kemudahan2" name="kemudahan" value="Tidak Lengkap Kemudahan" class="custom-control-input" required>
                                <label class="custom-control-label" for="kemudahan2">Tidak Lengkap Kemudahan</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no">Kawasan Perwataan</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="pewartaan1" name="perwartaan" value="1" class="custom-control-input" onclick="show2();" required>
                                <label class="custom-control-label" for="pewartaan1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="pewartaan2" name="perwartaan" value="0" class="custom-control-input" onclick="show1();" required>
                                <label class="custom-control-label" for="pewartaan2">Tidak</label>
                            </div>
                        </div>

                        <div id="perwartaan_input" class="form-group col-md-2" style="display:none;">
                            <label for="no_perwartaan">No Perwartaan</label>
                            <input type="text" class="form-control" id="no_perwartaan" name="no_perwartaan" value="{{ old('no_perwartaan') }}" placeholder="No Perwartaan" autocomplete="off">
                            <div class="invalid-feedback">
                                Sila Penuhkan.
                            </div>
                        </div>

                        <div class="col-12"><hr><h3>Jenis Kawasan</h3></div>


                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Kawasan Lapang</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="kawasan_lapang1" name="kawasan_lapang" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="kawasan_lapang1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="kawasan_lapang2" name="kawasan_lapang" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="kawasan_lapang2">Tidak</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Taman Awam</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="taman1" name="taman" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="taman1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="taman2" name="taman" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="taman2">Tidak</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Zon Penampan</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="penampan1" name="penampan" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="penampan1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="penampan2" name="penampan" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="penampan2">Tidak</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Padang Bola</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="bola1" name="padang" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="bola1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="bola2" name="padang" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="bola2">Tidak</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Dewan</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="dewan1" name="dewan" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="dewan1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="dewan2" name="dewan" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="dewan2">Tidak</label>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inventory_no"><u>Tidak Dijumpai</u></label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="dijumpai1" name="dijumpai" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="dijumpai1">Ya</label>
                            </div>
                            <div class="custom-control custom-radio mt-1">
                                <input type="radio" id="dijumpai2" name="dijumpai" value="0" class="custom-control-input" required>
                                <label class="custom-control-label" for="dijumpai2">Tidak</label>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Catatan</label>
                            <textarea id="textarea" class="form-control" name="catatan" maxlength="191" rows="3" placeholder="This notes has a limit of 191 chars."></textarea>
                            <div class="invalid-feedback">
                                Isikan Notes.
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">Simpan Maklumat dan Terus kepada Kemaskini Gambar</button>

                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->


@endsection

@push('autocomplete')
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    {{-- <script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script></head>
    <script type="text/javascript">

    function show1(){
        document.getElementById('perwartaan_input').style.display ='none';
    }
    function show2(){
        document.getElementById('perwartaan_input').style.display = 'block';
    }

    var path = "{{ route('kejur-zon') }}";
    $('input.zone').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });

    var path2 = "{{ route('kejur-lokasi') }}";
    $('input.location').typeahead({
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
                return process(data);
            });
        }
    });

    </script>
@endpush

