@push('datepickercss')
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 
    
<div class="row">
    <div class="col-12">

            {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div> --}}

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <form class="needs-validation" action="/superadmin/kejur/store" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="zon">Zon</label>
                            <input type="text" class="form-control" id="zon" name="zone" value="{{ old('zon') }}" placeholder="Zon" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan nama zon.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="location">Lokasi</label>
                            <input type="text" class="form-control" id="location" name="location" value="{{ old('Location') }}" placeholder="Location" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan location.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inventory_no">No Inventory</label>
                            <input type="text" class="form-control" id="inventory_no" name="inventory_no" value="{{ old('inventory_no') }}" placeholder="No Inventory" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan no inventory.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kategori">Status Program</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="maintance_status" required>
                                <option value="Penyelenggaran">Penyelenggaran</option>
                                <option value="Naik Taraf">Naik Taraf</option>
                                <option value="Kerja Segera">Kerja Segera</option>
                                <option value="Pemindahan">Pemindahan</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih kategori.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nilai">Nilai</label>
                            <input type="number" class="form-control" min="0.01" step="0.01" id="tinggi" name="value" value="{{ old('value') }}" placeholder="Nilai" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan nilai.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area">Keluasan (Ekar)</label>
                            <input type="number" class="form-control" id="area" name="area" value="{{ old('area') }}" placeholder="Keluasan (Ekar)" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan keluasan.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_type">Jenis Kawasan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="area_type" required>
                                <option value="Padang">Padang</option>
                                <option value="Dewan">Dewan</option>
                                <option value="Taman">Taman</option>
                                <option value="Zon Penempatan">Zon Penempatan</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih jenis kawasan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="intake_date">Tarikh Pengambilan</label>
                            <input type="text" class="form-control" name="intake_date" data-provide="datepicker" data-date-autoclose="true" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="intake_status">Status Pengambilalihan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="intake_status" required>
                                <option value="Belum Diambil Alih">Belum Diambil Alih</option>
                                <option value="Tidak Ambil Alih">Tidak Ambil Alih</option>
                                <option value="Rendah">Rendah</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih status pengambilalihan.
                            </div>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="gambar_kejur">Gambar Pokok</label>
                            <input type="file" name="gambar_kejur[]" id="example-fileinput" class="form-control-file" multiple required >
                            <div class="invalid-feedback">
                                Pilih Gambar.
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Notes</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This notes has a limit of 191 chars."></textarea>
                            <div class="invalid-feedback">
                                Isikan Notes.
                            </div>
                        </div>
                    </div>
                    
                    
                    <button class="btn btn-primary" type="submit">Simpan Maklumat Kejur</button>
                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->


@endsection

@push('autocomplete')
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script></head>
    <script type="text/javascript">


    var path = "{{ url('kejur-zon') }}";

    $('#zon').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.zone);
                });
                return process(dataset);
            });
        }
    });

    var path2 = "{{ url('kejur-lokasi') }}";

    $('#location').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.location);
                });
                return process(dataset);
            });
        }
    });

    var path3 = "{{ url('kejur-inventory') }}";

    $('#inventory_no').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path3, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.inventory_no);
                });
                return process(dataset);
            });
        }
    });
    </script>
@endpush