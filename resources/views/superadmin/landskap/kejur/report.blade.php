@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="animated fadeInUp slow row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Kejur</h4>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="animated fadeInRight duration1500ms col-12">

        @if(session()->has('success'))
            <script>
                setTimeout(function() {
                    Swal.fire({
                        title: "{{ session()->get('success') }}!",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }, 500);
            </script>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors as $error)
                {{ $error }}
            @endforeach
        @endif

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Kejur Keadaan Baik</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurBaik as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> no_inventori }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                {{-- <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Papar</button> --}}
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                {{-- <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur">Padam</button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

    <div class="animated fadeInLeft duration2s col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Kejur Naik Taraf</h4>

                <table id="basic-datatable2" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurNaikTaraf as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> no_inventori }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                {{-- <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Papar</button> --}}
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                {{-- <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur">Padam</button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

    <div class="animated fadeInRight duration2500ms col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Kejur Segera</h4>

                <table id="basic-datatable3" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurSegera as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> inventory_no }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                {{-- <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Papar</button> --}}
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                {{-- <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur">Padam</button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

    <div class="animated fadeInLeft duration3s col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Kejur Selenggara</h4>

                <table id="basic-datatable4" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>No Inventory</th>
                            <th>Tarikh Ambil Alih</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($kejurSelenggara as $kejur)
                        <tr>
                            <td>{{ $kejur -> location }}</td>
                            <td>{{ $kejur -> no_inventori }}</td>
                            <td>{{ $kejur -> intake_date }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='kejur/detail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                {{-- <button type="button" onclick="window.location.href='kejur/fulldetail/{{ $kejur -> kejurID }}'" class="btn btn-outline-info waves-effect waves-light">Papar</button> --}}
                                {{-- <button type="button" onclick="window.location.href='kejur/delete/{{ $kejur -> kejurID }}'" class="btn btn-outline-danger waves-effect waves-light">Padam</button> --}}
                                {{-- <button type="button" data-id="{{ $kejur -> kejurID }}" class="btn btn-outline-danger waves-effect waves-light deleteKejur">Padam</button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

</div>

@endsection
