@push('datepickercss')
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Kawasan Lapang</u></h4>
                <form class="needs-validation" action="/superadmin/kejur/save/{{ $kejur -> kejurID }}" method="POST" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="location">Lokasi</label>
                            <input type="text" class="form-control" id="location" name="location" value="{{ $kejur -> location }}" placeholder="Location" autocomplete="off" required>
                            <div class="invalid-feedback">
                               Isikan location.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inventory_no">No Inventori</label>
                            <input type="text" class="form-control" id="inventory_no" name="inventory_no" value="{{ $kejur -> no_inventori }}" placeholder="No Inventori" autocomplete="off" disabled>
                            <div class="invalid-feedback">
                                Isikan no inventory.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="acres">Keluasan (Ekar)</label>
                            <input type="number" class="form-control" id="acres" name="acres" value="{{ $kejur -> keluasan }}" placeholder="Keluasan (Ekar)" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan keluasan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="kategori">Tahap Keutamaan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="maintance_status" required>
                                <option value="{{ $kejur -> maintance_status }}">{{ $kejur -> maintance_status }}</option>
                                <option value="Baik">Baik</option>
                                <option value="Naik Taraf">Naik Taraf</option>
                                <option value="Kerja Segera">Kerja Segera</option>
                                <option value="Pemindahan">Pemindahan</option>
                                <option value="Penyelenggaran">Penyelenggaran</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih kategori.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="intake_date">Tarikh Pengambilan</label>
                            <input type="date" class="form-control" name="intake_date" value="{{ $kejur -> intake_date }}" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Catatan</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This notes has a limit of 191 chars.">{{ $kejur -> catatan }}</textarea>
                            <div class="invalid-feedback">
                                Isikan Notes.
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">Kemaskini Maklumat</button>

                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->


@endsection

@push('autocomplete')
    <script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script></head>
    <script type="text/javascript">


    var path = "{{ url('kejur-zon') }}";

    $('#zon').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.zone);
                });
                return process(dataset);
            });
        }
    });

    var path2 = "{{ url('kejur-lokasi') }}";

    $('#location').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.location);
                });
                return process(dataset);
            });
        }
    });

    var path3 = "{{ url('kejur-inventory') }}";

    $('#inventory_no').typeahead({
        minLength: 2,
        source:  function (query, process) {
        return $.get(path3, { query: query }, function (data) {
            var dataset = [];
                data.forEach(function(value){
                    dataset.push(value.inventory_no);
                });
                return process(dataset);
            });
        }
    });

    var postURL = "<?php echo url('addmore'); ?>";
    var i=0;


    $('#add').click(function(){
        i++;
        $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="nama_alatan[]" required></td><td><input type="number" min="0" name="baik[]" required></td><td><input type="number" min="0" name="rosak[]" required></td><td><input type="number" min="0" name="kuantiti[]" required></td><td><input type="file" name="gambar_alat['+i+'][]" multiple required></td><td><input type="text" name="catatan[]"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });


    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });
    </script>
@endpush

