@push('lightbox')
    <!-- Lightbox css -->
    <link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
@endpush

@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Maklumat dan Kemaskini</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

            {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div> --}}

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <br>
                <button type="button" data-id="{{ $trees -> treeID }}" class="btn btn-block btn-danger waves-effect waves-light deleteDevice">Padam Maklumat Pokok</button>
                <hr>
                <form class="needs-validation" action="/superadmin/update-pokok" method="POST" novalidate>
                    @method('PATCH')
                    @csrf
                    <div class="form-row">
                        <input type="hidden" name="treeID" value="{{ $trees -> treeID }}">
                        <div class="form-group col-md-4">
                            <label for="nama_saintifik">Nama Saintifik</label>
                            <input type="text" class="form-control" id="nama_saintifik" name="nama_saintifik" value="{{ $trees -> species -> scientific_name }}" placeholder="Nama Saintifik" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nama_biasa">Nama Biasa</label>
                            <input type="text" class="form-control" id="nama_biasa" name="nama_biasa" value="{{ $trees -> species -> local_name }}" placeholder="Nama Biasa" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="jenis">Jenis</label>
                            <input type="text" class="form-control" id="jenis" name="jenis" value="{{ $trees -> species -> genus }}" placeholder="Jenis" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kategori">Kategori</label>
                            <input type="text" class="form-control" id="kategori" name="kategori" value="{{ $trees -> getCategory -> category }}" placeholder="Kategori" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tinggi">Tinggi (cm)</label>
                            <input type="number" class="form-control" min="0.01" step="0.01" id="tinggi" name="tinggi" value="{{ $trees -> height }}" placeholder="Tinggi" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Silara">Silara</label>
                            <input type="text" class="form-control" id="Silara" name="silara" value="{{ $trees -> getSilara -> silara }}" placeholder="Silara" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inventory_no">No Inventory</label>
                            <input type="text" class="form-control" id="inventory_no" name="inventory_no" value="{{ $trees -> inventory_no }}" placeholder="No Inventory" disabled>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silrisikoara">Taraf Risiko</label>
                            <input type="text" class="form-control" id="risiko" name="risiko" value="{{ $trees -> getRisk -> risk }}" placeholder="Risiko" disabled>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Gambar Pokok</label>
                            <div class="popup-gallery">

                                <div class="row">
                                    @foreach ($medias as $media)
                                        <div class="col-3">
                                            <a href="{{url('/pokok/'.$media -> image_name)}}" title="{{ $trees -> species -> local_name }}">
                                                <div class="img-responsive">
                                                    <img src="{{asset('/pokok/'.$media -> image_name)}}" alt="" class="img-fluid">
                                                </div>
                                            </a>
                                        </div> <!-- end col-->
                                    @endforeach
                                </div>
                                <!-- end row-->

                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Notes</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This notes has a limit of 191 chars." disabled>{{ $trees -> notes }}</textarea>
                            <div class="invalid-feedback">
                                Isikan Notes.
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="lokasi">Lokasi</label>
                            <input type="text" class="form-control" id="lokasi" name="lokasi" value="{{ $trees -> location }}" placeholder="Lokasi" disabled>
                            <div class="invalid-feedback">
                                Isikan lokasi.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="zon">Zon</label>
                            <input type="text" class="form-control" id="zon" name="zon" value="{{ $trees -> zone }}" placeholder="Zon" disabled>
                            <div class="invalid-feedback">
                                Isikan zon.
                            </div>
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control" id="Latitude" name="latitude" value="{{ $trees -> latitude }}" placeholder="Latitude" disabled>
                            <div class="invalid-feedback">
                                Pilih Latitude.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control" id="Longitude" name="longitude" value="{{ $trees -> longitude }}" placeholder="Longitude" disabled>
                            <div class="invalid-feedback">
                                Pilih Longitude.
                            </div>
                        </div>

                        {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                                <div id="progress">
                                    <div id="progress-bar"></div>
                                </div>
                                <div></div>
                            </div> --}}

                    </div>

                </form>
                <hr/>
                <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div>

                <script>
                    $('.deleteDevice').click(function() {
                        var treeID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!',
                            cancelButtonText: 'Batal'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-pokok/" + treeID;
                            }
                        })
                    });
                </script>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->

@push('lightboxscript')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>

    <script>
        // use below if you want to specify the path for leaflet's images
        //L.Icon.Default.imagePath = '@Url.Content("~/Content/img/leaflet")';

        var curLocation = [0, 0];
        // use below if you have a model
        // var curLocation = [@Model.Location.Latitude, @Model.Location.Longitude];

        if (curLocation[0] == 0 && curLocation[1] == 0) {
          curLocation = [{{ $trees -> latitude }}, {{ $trees -> longitude }}];
        }

        var map = L.map('MapLocation').setView(curLocation, 14);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ', {
            maxZoom: 20,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ',
        }).addTo(map);

        map.attributionControl.setPrefix(false);

        var marker = new L.marker(curLocation, {
            draggable: false
        });

        // marker.on('dragend', function(event) {
        //     var position = marker.getLatLng();
        //     marker.setLatLng(position, {
        //     draggable: 'false'
        //     }).bindPopup(position).update();
        //     $("#Latitude").val(position.lat);
        //     $("#Longitude").val(position.lng).keyup();
        // });

        $("#Latitude, #Longitude").change(function() {
            var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
            marker.setLatLng(position, {
            draggable: false
            }).bindPopup(position).update();
            map.panTo(position);
        });

        map.addLayer(marker);

    </script>
@endpush

@endsection
