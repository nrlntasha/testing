@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Landskap Lembut</a></li>
                </ol>
            </div>
            <h4 class="page-title">Landskap Lembut</h4>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">

    <div class="animated flipInX duration1s col-xl-6">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Penyelenggaraan Pokok</h4>

                <div id="cardCollpase5" class="collapse pt-3 show" dir="ltr">
                    <div class="mt-4 chartjs-chart">
                        <canvas id="myChart"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="animated flipInX duration2s col-xl-6">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Kategori Pokok</h4>

                <div id="cardCollpase5" class="collapse pt-3 show" dir="ltr">
                    <div class="mt-4 chartjs-chart">
                        <canvas id="myChart2"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <hr/>

    <div class="animated infinite pulse col-12">
        @if (session()->get('role')=='Superadmin')
            <button href="" onclick="window.location.href='/superadmin/tree'" type="button" class="btn btn-block btn-success waves-effect waves-light">Tambah Pokok</button>
        @elseif (session()->get('role')=='Landskap')
            <button href="" onclick="window.location.href='/landskap/tree'" type="button" class="btn btn-block btn-success waves-effect waves-light">Tambah Pokok</button>
        @else

        @endif
        <hr/>
    </div>

    <div class="col-12">

        @if(session()->has('success'))
            <script>
                setTimeout(function() {
                    Swal.fire({
                        title: "{{ session()->get('success') }}!",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }, 500);
            </script>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors as $error)
                {{ $error }}
            @endforeach
        @endif

        {{-- FILTER SECTION FOR SENARAI POKOK --}}
        <div class="animated fadeIn duration1s card card-box" style="z-index: 1;">
            <form method="POST" id="export-form" action="{{route('export.softscape.maintainance')}}" target="_blank" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-md-0">
                        Senarai Zon
                    </p>
                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="zone" name="zone" >
                        <option value="0">-- Pilih Zon --</option>
                        @foreach ($zoneLists as $zoneList)
                            <option value="{{ $zoneList->zone }}">{{ $zoneList->zone }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Lokasi ( Sila Pilih Zon Dahulu )
                    </p>

                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" name="lokasi">
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Jenis Pokok ( Sila Pilih Lokasi Dahulu )
                    </p>

                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="nama_biasa" name="nama_biasa">
                    </select>
                </div>
            </div>

            <a class="collapsed" id="filterHeading" data-toggle="collapse" href="#filterCollapse" aria-expanded="false" aria-controls="filterCollapse">
                <h5 class="text-muted">MORE FILTER <i class="fas fa-angle-down rotate-icon"></i></h5>
            </a>
            <div class="row my-3 collapse" aria-labelledby="filterHeading" id="filterCollapse" aria-expanded="false">
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Taraf Risiko
                    </p>
                    <div style="display: inline-grid">
                        @foreach($treeRiskCheckbox as $risk)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="risk{{$risk -> id}}" name="risk[]" value="{{$risk -> id}}">
                                <label class="custom-control-label" for="risk{{$risk -> id}}">{{$risk -> name}}</label>
                            </div>
                        @endforeach
                    </div>
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Kategori
                    </p>
                    <div style="display: inline-grid">
                        @foreach($treeCategoryCheckbox as $category)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="category{{$category -> id}}" name="category[]" value="{{$category -> id}}">
                                <label class="custom-control-label" for="category{{$category -> id}}">{{$category -> category}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Silara
                    </p>
                    <div style="display: inline-grid">
                    @foreach($treeTypeCheckbox as $silara)
                    <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="silera{{$silara -> id}}" name="silera[]" value="{{$silara -> id}}">
                            <label class="custom-control-label" for="silera{{$silara -> id}}">{{$silara -> name}}</label>
                    </div>
                        @endforeach
                    </div>
                </div>
            </div>
            </form>
            <button id="btnFilter" class="btn btn-primary waves-effect waves-light"><i class="fas fa-icon"></i> Tapis</button>
        </div>
        {{-- END FILTER SECTION --}}

        {{-- SECTION FOR POKOK HARI INI --}}
        <div class="animated slideInRight duration2s card">
            <div class="card-body">
                <h4 class="header-title">Senarai Pokok {{ session()->get('days') }}</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Biasa</th>
                            <th>Nama Saintifik</th>
                            <th>Lokasi</th>
                            <th>Zon</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($trees as $tree)
                        <tr>
                            <td>{{ $tree -> species -> local_name }}</td>
                            <td>{{ $tree -> species -> scientific_name }}</td>
                            <td>{{ $tree -> species -> genus }}</td>
                            <td>{{ $tree -> getCategory -> category }}</td>
                            <td>
                                {{-- @if (session()->get('role')=='Superadmin')
                                    <button type="button" onclick="window.location.href='detail/{{ $tree -> treeID }}'" class="btn btn-outline-success waves-effect waves-light">Papar</button>
                                    <button type="button" onclick="window.location.href='detail-pokok/{{ $tree -> treeID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                    <button type="button" data-id="{{ $tree -> treeID }}" class="btn btn-outline-danger waves-effect waves-light deleteDevice">Padam</button>
                                @elseif (session()->get('role')=='Landskap')
                                    <button type="button" onclick="window.location.href='detail/{{ $tree -> treeID }}'" class="btn btn-outline-success waves-effect waves-light">Papar</button>
                                    <button type="button" onclick="window.location.href='detail-pokok/{{ $tree -> treeID }}'" class="btn btn-outline-info waves-effect waves-light">Kemaskini</button>
                                    <button type="button" data-id="{{ $tree -> treeID }}" class="btn btn-outline-danger waves-effect waves-light deleteDevice">Padam</button>
                                @else

                                @endif --}}
                                <button type="button" onclick="window.location.href='detail/{{ $tree -> treeID }}'" class="btn btn-outline-success waves-effect waves-light" data-toggle="tooltip" title="Papar"><i class="fas fa-eye"></i></button>
                                <button type="button" onclick="window.location.href='detail-pokok/{{ $tree -> treeID }}'" class="btn btn-outline-info waves-effect waves-light" data-toggle="tooltip" title="Kemaskini"><i class="fas fa-edit"></i></button>
                                <button type="button" onclick="window.location.href='delete-pokok/{{ $tree -> treeID }}'" class="btn btn-outline-danger waves-effect waves-light" data-toggle="tooltip" title="Padam"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->

        {{-- SECTION SEMUA POKOK --}}
        <div class="animated fadeInRight duration3s card">
            <div class="card-body">
                <h4 class="header-title">Senarai Pokok</h4>
                <button id="btn-export" class="btn btn-success mb-2">Simpan Sebagai PDF</button>
                <table class="table dt-responsive" width="100%" id="treeListTable">
                    <thead>
                        <tr>
                            <th>Nama Biasa</th>
                            <th>Lokasi</th>
                            <th>Zon</th>
                            <th>Kategori</th>
                            <th>Silara</th>
                            <th>Taraf Risiko</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                <script>
                    $('.deleteDevice').click(function() {
                        var treeID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!',
                            cancelButtonText: 'Batal'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "delete-pokok/" + treeID;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->

</div>

@endsection

@push('chartjs')
<script>
    $(document).ready(function(){

        var table = $('#treeListTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax" :{
                "url": "{{ url('treeListData') }}",
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{ csrf_token() }}", route:'treeData' }
            },
            "columns": [
                { "data": "local_name" },
                { "data": "location" },
                { "data": "zone" },
                { "data": "category" },
                { "data": "silara" },
                { "data": "risk" },
                { "data" : "action" }
            ],
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [-1]
                }
            ],
            drawCallback: function(setting){
                $('#treeListTable_paginate ul').addClass("pagination-rounded");
            },
        });

        /* FILTERING FUNCTION FOR SEMUA POKOK*/
        filterTable = () => {

            let url = "{{ url('treeListData') }}";
            let riskArr = [];
            let category = [];
            let silera = []

            $.each($("[name='risk[]']:checked"),function(){
                riskArr.push($(this).val());
            });

            $.each($("[name='category[]']:checked"),function(){
                category.push($(this).val());
            });

            $.each($("[name='silera[]']:checked"),function(){
                silera.push($(this).val());
            });

            let datas =  {
                "_token": "{{ csrf_token() }}",
                "route":'treeData',
                "action": "filter",
                "zone": $("#zone").val(),
                "location": $("[name='lokasi']").val(),
                "name": $("#nama_biasa").val(),
                "risk": riskArr,
                "category": category,
                "silera": silera
            };

            table.destroy();
            console.log(datas);
            table = $('#treeListTable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax" :{
                    "url": "{{ url('treeListData') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": datas
                },
                "columns": [
                    // { "data": "treeID" },
                    { "data": "local_name" },
                    // { "data": "saintifik_name" },
                    { "data": "location" },
                    { "data": "zone" },
                    { "data": "category" },
                    { "data": "silara" },
                    { "data": "risk" },
                    { "data" : "action" }
                ],
                aoColumnDefs: [
                    {
                        bSortable: false,
                        aTargets: [-1]
                    }
                ],
                drawCallback: function(setting){
                    $('#treeListTable_paginate ul').addClass("pagination-rounded");
                },
            });
        }

        /* FIRE FUNCTION WHEN FILTER BUTTON WAS CLICKED */
        $("#btnFilter").on("click", function(){
            filterTable();
        });

        /* AUTO POPULATE FILTER DROPDOWN */
        $('select[name="zone"]').on('change', function(){
                var zoneID = $(this).val();
                if(zoneID) {
                    $.ajax({
                        url: '/getLocation?zone='+zoneID,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="lokasi"]').empty().append('<option value="0">-- Sila Pilih --</option>');
                            $.each(data, function(key, value){

                                let data = `<option value="${value}">${value}</option>`;
                                $('select[name="lokasi"]').append(data);

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                            $('[name="lokasi"]').selectpicker('refresh');
                        }
                    });
                } else {
                    $('select[name="lokasi"]').empty();
                }

            });

            $('select[name="lokasi"]').on('change', function(){

                if($("#zone option:selected").text()==null){
                    $('select[name="nama_biasa"').empty();
                }

                var selValue = $("#zone :selected").text();

                var lokasiID = $(this).val();
                if(lokasiID) {
                    $.ajax({
                        url: '/getTreeType?zone='+selValue+'&lokasi='+lokasiID,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('[name="nama_biasa"]').empty().append('<option value="0">-- Sila Pilih --</option>');
                            $.each(data, function(key, value){
                                let appendData = `<option value="${key}">${value}</option>`;
                                $('select[name="nama_biasa"]').append(appendData);

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                            $('[name="nama_biasa"]').selectpicker('refresh');
                        }
                    });
                } else {
                    $('select[name="nama_biasa"]').empty();
                }

            });

    });
</script>
    <!-- Chart JS -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>

    <script>
        var ctx = document.getElementById('myChart');
        var canvas = document.getElementById('myChart');

        var data = {
            datasets: [
                {
                    label: 'Rendah',
                    data: [{{$rendahReport}}],
                    backgroundColor: '#689F38'
                },
                {
                    label: 'Sederhana',
                    data: [{{$sederhanaReport}}],
                    backgroundColor: '#FFC300'
                },
                {
                    label: 'Tinggi',
                    data: [{{$tinggiReport}}],
                    backgroundColor: '#FF5733'
                }
            ],
            labels: [{!!$chartData!!}]
        };

        var option = {
            plugins: {
                datalabels: {
                    color: 'white',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex]>0;
                    },
                    font:{
                        weight: 'bold'
                    }
                }
            },
            legend:{
                display: true,
                labels: {
                    fontColor: 'Black'
                }
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bil. Pokok'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Setiap 3 Bulan',
                        fontStyle: 'bold'
                    }
                }]
            }
        }

        var myChart = new Chart(ctx, {
            responsive: true,
            type: 'bar',
            data: data,
            options: option
        });

        canvas.onclick = function(evt){
            var activePoint = myChart.getElementAtEvent(evt)[0];
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;

            // console.log(activePoint.$datalabels._index);
            // var labels = data.labels[datasetIndex].;
            var labels =  data.labels[activePoint.$datalabels._index];
            var label = data.datasets[datasetIndex].label;
            var value = data.datasets[datasetIndex].data[activePoint._index];

            if(labels == 'January-March'){
                window.location.href="#";
            }else if(labels == 'April-June'){
                window.location.href="#";
            }else if(labels == 'July-September'){
                window.location.href="#";
            }else if(labels == 'October-December'){
                window.location.href="#";
                // window.location.href="/superadmin/report-month/10/12";
            }else{
                console.log(label, value, labels);
            }
            // console.log(label, value, labels);
        }

        //second chart
        var ctx = document.getElementById('myChart2');
        var canvas = document.getElementById('myChart2');

        var data = {
            datasets: [
                {
                    data: [<?php echo $bilPokokRenek ?>, <?php echo $bilPokokTeduhan ?>, <?php echo $bilPokokPalma ?>],
                    backgroundColor: ['#1DB2E3', '#24DBB1', '#FED54B']
                    // label: ['Rendah', 'Sederhana', 'Tinggi']
                }
            ],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Renek',
                'Teduhan',
                'Palma'
            ]
        };

        var option = {
            plugins: {
                datalabels: {
                    color: 'white',
                    display: function(context) {
                        return context.dataset.data[context.dataIndex]>0;
                    },
                    font:{
                        weight: 'bold'
                    }
                }
            },

            responsive: true
        };

        // For a pie chart
        var myChart2 = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: option
        });

        $("#btn-export").on("click", function(){
            $("#export-form").submit();
        });

    </script>
@endpush
