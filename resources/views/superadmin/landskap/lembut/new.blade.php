@extends('layouts.admin')

@push('customPageCSS')
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
type="text/css"
/>
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
@endpush

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Tambah Pokok</a></li>
                </ol>
            </div>
            <h4 class="page-title">Tambah Pokok</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <form class="needs-validation" action="/superadmin/save-tree" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-row">

                        <div class="form-group col-md-4" id="custom-search-input">
                            <label for="nama_biasa">Nama Biasa</label>
                            <input type="text" class="nama_biasa form-control" name="nama_biasa" value="{{ old('nama_biasa') }}" placeholder="Nama Biasa" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan nama biasa.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nama_saintifik">Nama Saintifik</label>
                            <input type="text" class="nama_saintifik form-control" name="nama_saintifik" value="{{ old('nama_saintifik') }}" placeholder="Nama Saintifik" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan nama saintifik.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="jenis">Genus</label>
                            <input type="text" class="form-control" id="jenis" name="jenis" value="{{ old('jenis') }}" placeholder="Genus" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan genus.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kategori">Kategori</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="kategori" required>
                                @foreach($categoryDropdown as $category)
                                <option value="{{ $category -> id }}">{{ $category -> category }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih kategori.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tinggi">Tinggi (cm)</label>
                            <input type="number" class="form-control" min="0.01" step="0.01" id="tinggi" name="tinggi" value="{{ old('tinggi') }}" placeholder="Tinggi" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan tinggi.
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Silara">Silara</label>
                            <select class="selectpicker form-control" data-live-search="true" data-style="btn-light" name="silara" required>
                                @foreach($silaraDropdown as $silara)
                                    <option value="{{ $silara -> id }}">{{ $silara -> name }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih silara.
                            </div>
                        </div>

                        {{-- <div class="form-group col-md-4">
                            <label for="inventory_no">No Inventory</label>
                            <input type="text" class="inventory_no form-control" name="inventory_no" value="{{ old('inventory_no') }}" placeholder="No Inventory" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan no inventory.
                            </div>
                        </div> --}}

                        <div class="form-group col-md-4">
                            <label for="Silara">Taraf Risiko</label>
                            <select class="selectpicker form-control" data-live-search="true" data-style="btn-light" name="risiko" required>
                                @foreach($riskDropdown as $risk)
                                    <option value="{{ $risk -> id }}">{{ $risk -> name }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih risiko.
                            </div>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="gambar">Gambar Pokok</label>
                            <input type="file" name="gambar_pokok[]" id="example-fileinput" class="form-control-file" multiple required>
                            <div class="invalid-feedback">
                                Pilih Gambar.
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Notes</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This notes has a limit of 191 chars."></textarea>
                            <div class="invalid-feedback">
                                Isikan Notes.
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="lokasi">Lokasi</label>
                            <input type="text" class="lokasi form-control" name="lokasi" value="{{ old('lokasi') }}" placeholder="Lokasi" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan lokasi.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="zon">Zon</label>
                            <input type="text" class="zon form-control" name="zon" value="{{ old('zon') }}" placeholder="Zon" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Isikan zon.
                            </div>
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control" id="Latitude" name="latitude" value="{{ old('latitude') }}" placeholder="Latitude" readonly required>
                            <div class="invalid-feedback">
                                Pilih Latitude.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control" id="Longitude" name="longitude" value="{{ old('longitude') }}" placeholder="Longitude" readonly required>
                            <div class="invalid-feedback">
                                Pilih Longitude.
                            </div>
                        </div>

                    </div>

                    <button class="btn btn-primary" type="submit">Simpan Maklumat</button>
                </form>
                <hr/>
                <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->

@push('autocomplete')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">

    var path = "{{ route('autocomplete-saintifik') }}";
    $('input.nama_saintifik').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });

    var path2 = "{{ route('autocomplete') }}";
    $('input.nama_biasa').typeahead({
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
                return process(data);
            });
        },
        afterSelect:function(data){
            let autoFill = "{{ route('autofill-tree') }}";
            return $.get(autoFill, { query: data}, function(item){
                $('input.nama_saintifik').val(item.scientific_name);
                $('input#jenis').val(item.genus);
            });
        }
    });

    var path3 = "{{ route('autocomplete-inventory') }}";
    $('input.inventory_no').typeahead({
        source:  function (query, process) {
        return $.get(path3, { query: query }, function (data) {
                return process(data);
            });
        }
    });

    var pathToCoords = [];

    var path4 = "{{ route('autocomplete-lokasi') }}";
    $('input.lokasi').typeahead({
        source:  function (query, process) {
        return $.get(path4, { query: query }, function (data) {

                data.map(function(d, i) {
                    pathToCoords[d.location] = [d.longitude, d.latitude]
                })

                return process(data.map(function(d) { return d.location; }));
            });
        }
    });

    var path5 = "{{ route('autocomplete-zon') }}";
    $('input.zon').typeahead({
        source:  function (query, process) {
        return $.get(path5, { query: query }, function (data) {
                return process(data);
            });
        }
    });
    // var path2 = "{{ url('autocomplete-saintifik') }}";

    // $('#nama_saintifik').typeahead({
    //      minLength: 2,
    //     source:  function (query, process) {
    //     return $.get(path2, { query: query }, function (data) {
    //         var dataset = [];
    //             data.forEach(function(value){
    //                 dataset.push(value.saintifik_name);
    //             });
    //             return process(dataset);
    //         });
    //     }
    // });

    // var path3 = "{{ url('autocomplete-inventory') }}";

    // $('#inventory_no').typeahead({
    //     minLength: 2,
    //     source:  function (query, process) {
    //     return $.get(path3, { query: query }, function (data) {
    //         var dataset = [];
    //             data.forEach(function(value){
    //                 dataset.push(value.inventory_no);
    //             });
    //             return process(dataset);
    //         });
    //     }
    // });

    // var path4 = "{{ url('autocomplete-lokasi') }}";

    // $('#lokasi').typeahead({
    //     minLength: 2,
    //     source:  function (query, process) {
    //     return $.get(path4, { query: query }, function (data) {
    //         var dataset = [];
    //             data.forEach(function(value){
    //                 dataset.push(value.location);
    //             });
    //             return process(dataset);
    //         });
    //     }
    // });

    // var path5 = "{{ url('autocomplete-zon') }}";

    // $('#zon').typeahead({
    //     minLength: 2,
    //     source:  function (query, process) {
    //     return $.get(path5, { query: query }, function (data) {
    //         var dataset = [];
    //             data.forEach(function(value){
    //                 dataset.push(value.zone);
    //             });
    //             return process(dataset);
    //         });
    //     }
    // });
</script>
@endpush


<script>

    // use below if you want to specify the path for leaflet's images
    //L.Icon.Default.imagePath = '@Url.Content("~/Content/img/leaflet")';

    var curLocation = [0, 0];
    // use below if you have a model
    // var curLocation = [@Model.Location.Latitude, @Model.Location.Longitude];

    if (curLocation[0] == 0 && curLocation[1] == 0) {
        curLocation = [101.597189, 3.144837];
    }

	mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ';
    var map = new mapboxgl.Map({
        'container': 'MapLocation',
        'style': 'mapbox://styles/mapbox/streets-v11',
        'center': curLocation,
        'zoom': 13
    });

    var geocoder = new MapboxGeocoder({
                        accessToken: mapboxgl.accessToken,
                        mapboxgl: mapboxgl,
                        marker: false
                    });

    map.addControl(geocoder);

    map.on('load', function() {

        var marker;

        marker = new mapboxgl.Marker()
                            .setLngLat(curLocation)
                            .addTo(map)

        geocoder.on('result', function(e) {

            marker.remove()

            marker = new mapboxgl.Marker()
                                .setLngLat(e.result.geometry.coordinates)
                                .addTo(map)

            $("#Latitude").val(e.result.geometry.coordinates[1]);
            $("#Longitude").val(e.result.geometry.coordinates[0]).keyup();
            $('input.lokasi').val(e.result.place_name);
        })

        $('input.lokasi').on('change', function(e) {
            var key = $(e.target).val(),
                coords = pathToCoords[key];

            map.flyTo({
                center: coords})

            marker.remove()

            marker = new mapboxgl.Marker()
                                .setLngLat(coords)
                                .addTo(map)

            $("#Latitude").val(coords[1]);
            $("#Longitude").val(coords[0]).keyup();
        })

    });


</script>


@endsection
