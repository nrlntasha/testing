@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Kejur</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 


<div class="row">

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Ambilalih</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Kemudahan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart2"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Perwartaan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="myChart3"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Kawasan Lapang</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="cartaLapang"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Taman</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="cartaTaman"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Penampan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="cartaPenampan"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Padang Bola</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="cartaPadangBola"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                </div>
                <h4 class="header-title mb-0">Carta Dewan</h4>

                <div id="cardCollpase5" class="collapse pt-2 show" dir="ltr">
                    <div class="chartjs-chart">
                        <canvas id="cartaDewan"></canvas>
                    </div>
                </div> <!-- collapsed end -->
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

</div>
<!-- end row-->
@push('apexchart')

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>

<script>
    var ctx = document.getElementById('myChart');
    var canvas = document.getElementById('myChart');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilAmbilalih ?>, <?php echo $bilBelumAmbilalih ?>, <?php echo $bilTidakAmbilalih ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)', 'rgba(255,0,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Telah Ambilalih',
            'Belum Ambilalih',
            'Tidak Ambilalih'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta kemudahan
    var ctx = document.getElementById('myChart2');
    var canvas = document.getElementById('myChart2');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilLengkapKemudahan ?>, <?php echo $bilTidakKemudahan ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Lengkap Kemudahan',
            'Tidak Lengkap Kemudahan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var myChart2 = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta perwartaan
    var ctx = document.getElementById('myChart3');
    var canvas = document.getElementById('myChart3');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilPerwartaan ?>, <?php echo $bilTidakPerwartaan ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Perwartaan',
            'Tidak Perwartaan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var myChart3 = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta kawasan lapang
    var ctx = document.getElementById('cartaLapang');
    var canvas = document.getElementById('cartaLapang');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilKawasanLapang ?>, <?php echo $bilBukanLapang ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Kawasan Lapang',
            'Bukan Kawasan Lapang'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var cartaLapang = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta taman
    var ctx = document.getElementById('cartaTaman');
    var canvas = document.getElementById('cartaTaman');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilTaman ?>, <?php echo $bilBukanTaman ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Taman',
            'Bukan Taman'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var cartaTaman = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta Penampan
    var ctx = document.getElementById('cartaPenampan');
    var canvas = document.getElementById('cartaPenampan');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilPenampan ?>, <?php echo $bilBukanPenampan ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Kawasan Penampan',
            'Kawasan Bukan Penampan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var cartaPenampan = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta padang bola
    var ctx = document.getElementById('cartaPadangBola');
    var canvas = document.getElementById('cartaPadangBola');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilPadangBola ?>, <?php echo $bilBukanPadangBola ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Padang Bola',
            'Tiada Padang Bola'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var cartaPadangBola = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

    //carta dewan
    var ctx = document.getElementById('cartaDewan');
    var canvas = document.getElementById('cartaDewan');

    var data = {
        datasets: [
            {
                data: [<?php echo $bilDewan ?>, <?php echo $bilBukanDewan ?>],
                backgroundColor: ['rgba(0,128,0, 0.8)', 'rgba(255,255,0, 0.8)']
                // label: ['Rendah', 'Sederhana', 'Tinggi']
            }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Dewan',
            'Tidak Dewan'
        ]
    };

    var option = {
        plugins: {
            datalabels: {
                color: 'black',
                display: function(context) {
                    return context.dataset.data[context.dataIndex]>0;
                },
                font:{
                    weight: 'bold'
                }
            }
        },
        
        responsive: true
    };

    // For a pie chart
    var cartaDewan = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: option
    });

</script>

@endpush

@endsection
