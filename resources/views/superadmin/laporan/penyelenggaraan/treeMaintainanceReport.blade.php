@extends('layouts.admin')
@section('content')

    <div class="animated fadeIn duration1s card card-box mt-5" style="z-index: 1;">
        <form method="POST" id="export-form" action="{{route('treeMaintainanceReport')}}" target="_blank">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-4">
                <p class="text-muted font-13 mt-md-0">
                    Bulan
                </p>
                <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="month" name="month" >
                    <option value="0">-- Pilih Bulan --</option>
                    @foreach ($month as $key => $val)
                        <option value="{{ $key + 1 }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <p class="text-muted font-13 mt-3 mt-md-0">
                    Tahun
                </p>

                <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="year" name="year">
                    <option value="">-- Pilih Tahun --</option>
                    @foreach ($years as $year)
                        <option value="{{$year}}">{{$year}}</option>
                    @endforeach
                </select>
            </div>
            {{-- <div class="col-md-4">
                <p class="text-muted font-13 mt-3 mt-md-0">
                    Senarai Jenis Pokok ( Sila Pilih Lokasi Dahulu )
                </p>

                <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="nama_biasa" name="nama_biasa">
                </select>
            </div> --}}
            <div class="col-md-4">
                <p class="font-13 mt-md-0" style="color: white; cursor: default;">Tapis</p>
                <input type="submit" id="btnFilter" class="btn btn-primary waves-effect waves-light form-control" value="Tapis">
            </div>
        </div>
        </form>
    </div>

@endsection
