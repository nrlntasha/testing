@push('customPageCSS')
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pokok</a></li>
                </ol>
            </div>
            <h4 class="page-title">Pokok</h4>
        </div>
    </div>
</div>
<!-- end page title -->

{{-- <div class="row">
    <div class="col-xl-5">
        <div class="form-group mb-3">
            <input type="text" id="zoneInput" name="example-input-large" class="form-control form-control-lg" placeholder="Cari nama zon">
        </div>
    </div>
</div> --}}

<div class="row">
    <div class="col-12" >
        <div class="card-box">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Zon
                    </p>

                    <select class="form-control" id="zone" name="zone" data-toggle="select2">
                        <option>-- Pilih Zon --</option>
                        @foreach ($zoneLists as $zoneList)
                            <option value="{{ $zoneList->zone }}">{{ $zoneList->zone }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Lokasi ( Sila Pilih Zon Dahulu )
                    </p>

                    <select class="form-control" name="lokasi" data-toggle="select2">
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Jenis Pokok ( Sila Pilih Lokasi Dahulu )
                    </p>

                    <select class="form-control" id="nama_biasa" name="nama_biasa" data-toggle="select2">
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="results">
teST
</div>

<div class="row">
    @foreach ($zoneCounts as $zoneCount)
    <div class="col-4 zoneName">
        <div class="card-box">
            <div class="row">
                <div class="col-6">
                    <div class="">
                        <h4 class="text-muted my-1 zoneTitle">Jumlah Zon</h4>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark my-1"><span data-plugin="counterup">
                            {{$zoneCount->zoneCount}}
                        </span></h3>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>
    @endforeach
    @foreach ($locationCounts as $locationCount)
    <div class="col-4 zoneName">
        <div class="card-box">
            <div class="row">
                <div class="col-6">
                    <div class="">
                        <h4 class="text-muted my-1 zoneTitle">Jumlah Lokasi</h4>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark my-1"><span data-plugin="counterup">
                            {{$locationCount->locationCount}}
                        </span></h3>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>
    @endforeach
    <div class="col-4 zoneName">
        <div class="card-box">
            <div class="row">
                <div class="col-6">
                    <div class="">
                        <h4 class="text-muted my-1 zoneTitle">Jumlah Pokok</h4>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark my-1"><span data-plugin="counterup">
                            {{$treeCounts}}
                        </span></h3>
                    </div>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>
</div>

<div class="row">
    <div class="animated fadeInLeft slow col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Bilangan Pokok</h4>
                <br>
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Pokok</th>
                            <th>Bilangan</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($treeTypes as $treetype)
                        <tr>
                            <td>{{ $treetype -> name }}</td>
                            <td>{{ $treetype -> count }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

{{-- <div class="row">
    <div class="animated fadeInLeft slow col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Bilangan Pokok Mengikut Nama Sainstifik</h4>
                <br>
                <table id="basic-datatable2" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Pokok</th>
                            <th>Bilangan</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($saintifikTreeTypes as $saintifikTreeType)
                        <tr>
                            <td>{{ $saintifikTreeType -> saintifik_name }}</td>
                            <td>{{ $saintifikTreeType -> saintifikTree }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div> --}}
<!-- end row-->

@push('searchZone')
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>

    <script>
        $(document).ready(function() {

        /* POPULATE DROPDOWN FILTER ONCE THE FIRST DROPDOWN WAS SELECTED */
        $('select[name="zone"]').on('change', function(){
                var zoneID = $(this).val();
                if(zoneID) {
                    $.ajax({
                        url: '/getLocation?zone='+zoneID,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="lokasi"]').empty().append('<option>-- Sila Pilih --</option>');
                            $.each(data, function(key, value){

                                let data = `<option value="${value}">${value}</option>`;
                                $('select[name="lokasi"]').append(data);

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="lokasi"]').empty();
                }

            });

            $('select[name="lokasi"]').on('change', function(){

                if($("#zone option:selected").text()==null){
                    $('select[name="nama_biasa"').empty();
                }

                var selValue = $("#zone :selected").text();

                var lokasiID = $(this).val();
                if(lokasiID) {
                    $.ajax({
                        url: '/getTreeType?zone='+selValue+'&lokasi='+lokasiID,
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('[name="nama_biasa"]').empty().append('<option>-- Sila Pilih --</option>');
                            $.each(data, function(key, value){

                                let appendData = `<option value="${key}">${value}</option>`;
                                $('select[name="nama_biasa"]').append(appendData);

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="nama_biasa"]').empty();
                }

            });

            $("#results").hide();

            $(function(){
                $("#nama_biasa").change(function(){
                    if($(this).val() === ""){
                        $("#results").hide();
                    }else{
                        $("#results").show();
                    }
                });
            });

        });
    </script>

@endpush

@push('chartjs')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>

@endpush

@endsection
