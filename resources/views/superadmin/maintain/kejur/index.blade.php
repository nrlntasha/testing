@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kejur</a></li>
                </ol>
            </div>
            <h4 class="page-title">Penyelenggaraan Kejur</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 
    
<div class="row">
    <div class="col-12">

            {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div> --}}

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <form class="needs-validation" action="/superadmin/update-maintance-pokok" method="POST" novalidate>
                    @method('PATCH')
                    @csrf
                    <input type="hidden" name="maintainID" value="{{ $maintain -> maintainID }}">
                    <hr/>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="Silara">Taraf Risiko</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="risk_level" required>
                                <option value="{{ $maintain -> risk_level }}">Semasa - {{ $maintain -> risk_level }}</option>
                                <option value="Tinggi">Tinggi</option>
                                <option value="Sederhana">Sederhana</option>
                                <option value="Rendah">Rendah</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih risiko.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Jenis Penyelenggaraan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="type_maintance" required>
                                <option value="{{ $maintain -> type_maintance }}">Semasa - {{ $maintain -> type_maintance }}</option>
                                <option value="Belum Diselenggara">Belum Diselenggara</option>
                                <option value="Sedang Diselenggara">Sedang Diselenggara</option>
                                <option value="Telah Diselenggara">Telah Diselenggara</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih jenis penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">status Penyelenggaraan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="status_maintance" required>
                                <option value="{{ $maintain -> status_maintance }}">Semasa - {{ $maintain -> status_maintance }}</option>
                                <option value="Tebang Pokok Tanpa Mencabut Akar">Tebang Pokok Tanpa Mencabut Akar</option>
                                <option value="Tebang Pokok Dengan Mencabut Akar">Tebang Pokok Dengan Mencabut Akar</option>
                                <option value="Membersih Pokok Tumbang">Membersih Pokok Tumbang</option>
                                <option value="Bongkar Tunggul">Bongkar Tunggul</option>
                                <option value="Merincih Tunggul">Merincih Tunggul</option>
                                <option value="Cantasan / Pemangkasan">Cantasan / Pemangkasan</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih status penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Jenis Tarikh</label>
                            <input type="text" class="form-control" name="maintain_date" value={{ $maintain -> maintain_date }} data-provide="datepicker" data-date-autoclose="true" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="sebelum">Gambar Sebelum</label>
                            <div class="popup-gallery">
                                <div class="row">
                                    @foreach ($medias as $media)
                                        <div class="col-3">
                                            <a href="{{url('/maintance/sebelum/'.$media -> image_name)}}" title="Sebelum">
                                                <div class="img-responsive">
                                                    <img src="{{asset('/maintance/sebelum/'.$media -> image_name)}}" alt="" class="img-fluid">
                                                </div>
                                            </a>
                                        </div> <!-- end col-->
                                    @endforeach
                                </div>
                                <!-- end row-->
                            </div>                        
                        </div>
                    </div>
                    <hr/>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="selepas">Gambar Selepas</label>
                            <div class="popup-gallery">
                                <div class="row">
                                    @foreach ($medias as $media)
                                        <div class="col-3">
                                            <a href="{{url('/maintance/selepas/'.$media -> image_name)}}" title="Selepas">
                                                <div class="img-responsive">
                                                    <img src="{{asset('/maintance/selepas/'.$media -> image_name)}}" alt="" class="img-fluid">
                                                </div>
                                            </a>
                                        </div> <!-- end col-->
                                    @endforeach
                                </div>
                                <!-- end row-->
                            </div>                        
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Catatan</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This catatan has a limit of 191 chars.">{{ $maintain -> notes }}</textarea>
                            <div class="invalid-feedback">
                                Isikan catatan.
                            </div>
                        </div>
                    </div>
                    
                    <button class="btn btn-primary" type="submit">Kemaskini Maklumat Selenggara</button>
                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->

@endsection