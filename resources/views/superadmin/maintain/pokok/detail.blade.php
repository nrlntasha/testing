@push('datepickercss')
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('customPageCSS')
    <!-- Lightbox css -->
    <link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
@endpush

@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

            {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div> --}}

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <form class="needs-validation" action="/superadmin/update-maintance-pokok" method="POST" novalidate>
                    @method('PATCH')
                    @csrf
                    <input type="hidden" name="maintainID" value="{{ $maintain -> maintainID }}">
                    <hr/>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="Silara">Taraf Risiko</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="risk_level" required>
                                <option value="{{ $currentRisk -> id }}">Semasa - {{ $currentRisk -> name }}</option>
                                @foreach($riskDropdown as $risk)
                                    <option value="{{$risk -> id}}">{{$risk -> name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih risiko.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Status Penyelenggaraan</label>
                            <select class="selectpicker" data-live-search="true" data-style="btn-light" name="status_maintance" required>
                                <option value="{{$currentStatus -> id}}">{{$currentStatus -> status}}</option>
                                @foreach($statusDropdown as $status)
                                    <option value="{{$status -> id}}">{{$status -> status}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih jenis penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Jenis Penyelenggaraan</label>
                            <select class="selectpicker form-control" data-size="5" data-live-search="true" data-style="btn-light" name="type_maintance" required>
                                <option value="{{$maintain -> type_maintance}}">Semasa - {{$currentActivity -> activity_name}}</option>
                                @foreach($activityDropdown as $activity)
                                    <option value="{{$activity -> id}}">{{$activity -> activity_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih status penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Tarikh Selenggara</label>
                            <input type="text" class="form-control" name="maintain_date" value={{ $maintain -> maintain_date }} data-provide="datepicker" data-date-autoclose="true" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="sebelum">Gambar Sebelum</label>
                            <div class="popup-gallery">
                                <div class="row">
                                    @foreach ($sebelum as $belum)
                                        <div class="col-3">
                                            <a href="{{url('/maintance/sebelum/'.$belum -> image_name)}}" title="Sebelum">
                                                <div class="img-responsive">
                                                    <img src="{{asset('/maintance/sebelum/'.$belum -> image_name)}}" alt="" class="img-fluid">
                                                </div>
                                            </a>
                                        </div> <!-- end col-->
                                    @endforeach
                                </div>
                                <!-- end row-->
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="selepas">Gambar Selepas</label>
                            <div class="popup-gallery">
                                <div class="row">
                                    @foreach ($selepas as $lepas)
                                        <div class="col-3">
                                            <a href="{{url('/maintance/selepas/'.$lepas -> image_name)}}" title="Selepas">
                                                <div class="img-responsive">
                                                    <img src="{{asset('/maintance/selepas/'.$lepas -> image_name)}}" alt="" class="img-fluid">
                                                </div>
                                            </a>
                                        </div> <!-- end col-->
                                    @endforeach
                                </div>
                                <!-- end row-->
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Catatan</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This catatan has a limit of 191 chars.">{{ $maintain -> notes }}</textarea>
                            <div class="invalid-feedback">
                                Isikan catatan.
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">Kemaskini Maklumat Selenggara</button>
                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->

@push('datepicker')
<script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script>
@endpush


@endsection
