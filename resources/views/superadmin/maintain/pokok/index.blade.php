@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Penyelenggaraan</a></li>
                </ol>
            </div>
            <h4 class="page-title">Penyelenggaraan</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">

    <div class="animated fadeInLeft duration2s col-xl-6">
        <div class="card cta-box bg-info text-white">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body">

                        <h3 class="m-0 font-weight-normal text-white sp-line-1 cta-box-title">Carta Penyelenggaraan Mengikut Kategori</h3>
                        <p class="text-light mt-2 sp-line-2"></p>
                        <button type="button" onclick="window.location.href='category-maintain'" class="animated infinite pulse btn btn-info waves-effect waves-light">Papar Maklumat <i class="mdi mdi-arrow-right-thick mr-1"></i></button>
                    </div>
                    {{-- <img class="ml-3" src="{{asset('assets/images/update.svg')}}" width="120" alt="Generic placeholder image"> --}}
                </div>
            </div>
            <!-- end card-body -->
        </div>
    </div>

    <div class="animated fadeInRight duration3s col-xl-6">
        <div class="card cta-box bg-success text-white">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body">

                        <h3 class="m-0 font-weight-normal text-white sp-line-1 cta-box-title">Carta Penyelenggaraan Mengikut Zon</h3>
                        <p class="text-light mt-2 sp-line-2"></p>
                        <button type="button" onclick="window.location.href='zone-maintain'" class="animated infinite pulse btn btn-success waves-effect waves-light">Papar Maklumat <i class="mdi mdi-arrow-right-thick mr-1"></i></button>
                    </div>
                    {{-- <img class="ml-3" src="{{asset('assets/images/update.svg')}}" width="120" alt="Generic placeholder image"> --}}
                </div>
            </div>
            <!-- end card-body -->
        </div>
    </div>

</div>

<div class="row">

    {{-- FILTER SECTION FOR SENARAI POKOK --}}
    {{-- <div class="col-12" style="z-index: 1">
        <div class="animated fadeIn duration1s card card-box">
            <form method="POST" id="export-form" action="{{route('export.softscape.maintainance')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-md-0">
                        Senarai Zon
                    </p>
                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="zone" name="zone" >
                        <option value="0">-- Pilih Zon --</option>
                        @foreach ($zoneLists as $zoneList)
                            <option value="{{ $zoneList->zone }}">{{ $zoneList->zone }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Lokasi ( Sila Pilih Zon Dahulu )
                    </p>

                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" name="lokasi">
                    </select>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Senarai Jenis Pokok ( Sila Pilih Lokasi Dahulu )
                    </p>

                    <select class="selectpicker form-control" data-live-search="true" data-size="5" data-style="btn-light" id="nama_biasa" name="nama_biasa">
                    </select>
                </div>
            </div>

            <a class="collapsed" id="filterHeading" data-toggle="collapse" href="#filterCollapse" aria-expanded="false" aria-controls="filterCollapse">
                <h5 class="text-muted">MORE FILTER <i class="fas fa-angle-down rotate-icon"></i></h5>
            </a>
            <div class="row my-3 collapse" aria-labelledby="filterHeading" id="filterCollapse" aria-expanded="false">
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Taraf Risiko
                    </p>
                    <div style="display: inline-grid">
                        @foreach($riskCheckbox as $key => $value)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="risk" id="risk{{$key}}" value="{{$value}}">
                                <label class="custom-control-label" for="risk{{$key}}">{{$value}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Kategori
                    </p>
                    <div style="display: inline-grid">
                        @foreach($categoryCheckbox as $key => $value)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="category" id="category{{$key}}" value="{{$value}}">
                                <label class="custom-control-label" for="category{{$key}}"> {{$value}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-muted font-13 mt-3 mt-md-0">
                        Silera
                    </p>
                    <div style="display: inline-grid">
                        @foreach($silaraCheckbox as $key => $value)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="silera" id="silera{{$key}}" value="{{$value}}">
                                <label class="custom-control-label" for="silera{{$key}}"> {{$value}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            </form>
            <button id="btnFilter" class="btn btn-primary waves-effect waves-light"><i class="fas fa-icon"></i> Tapis</button>
        </div>
    </div> --}}
    {{-- END FILTER SECTION --}}

    <div class="col-xl-12">
        <div class="card-box" style="height:600px;" id="map">
            <div id="progress">
                <div id="progress-bar"></div>
            </div>
            <div></div>
        </div>
    </div>

    <div class="animated fadeInLeft slow col-12">
        <div class="card">
            {{-- <button type="submit" target="_blank" id="btnEksport" class="btn btn-success">Eksport</button></form> --}}
            <div class="card-body">
            <h4 class="header-title">Senarai Pokok Diselenggara {{ session()->get('days') }}</h4>

                <table id="basic-datatable2" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Biasa</th>
                            <th>Lokasi</th>
                            <th>Zon</th>
                            <th>Taraf Risiko</th>
                            <th>Jenis Penyelenggaraan</th>
                            <th>Status</th>
                            {{-- <th>Tarikh</th> --}}
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($maintancess as $maintain)
                        <tr>
                            <td>{{ $maintain -> getTree -> species -> local_name }}</td>
                            <td>{{ $maintain -> getTree -> location }}</td>
                            <td>{{ $maintain -> getTree -> zone }}</td>
                            <td>{{ $maintain -> getRisk -> name }}</td>
                            <td>{{ $maintain -> getActivity -> activity_name }}</td>
                            <td>{{ $maintain -> getStatus -> status }}</td>
                            {{-- <td>{{ date('d-m-Y', strtotime($maintain -> maintain_date)) }}</td> --}}
                            <td>
                                <button type="button" onclick="window.location.href='maintance-update-pokok/{{ $maintain -> maintainID }}'" class="btn btn-outline-success waves-effect waves-light" data-toggle="tooltip" title="Kemaskini"><i class="fas fa-edit"></i></button>
                                <button onclick="window.open('maintance/print/{{$maintain->maintainID}}')" class="btn btn-outline-info"><i class="fas fa-print"></i></button>
                                <button type="button" data-id="{{ $maintain -> maintainID }}" class="btn btn-outline-danger waves-effect waves-light deleteMaintain" data-toggle="tooltip" title="Padam"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    $('.deleteMaintain').click(function() {
                        var maintainID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-maintance-pokok/" + maintainID;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="animated fadeInRight slow col-12">
        @if(session()->has('success'))
            <script>
                setTimeout(function() {
                    Swal.fire({
                        title: "{{ session()->get('success') }}!",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }, 500);
            </script>
        @endif

        {{-- <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Pokok</h4>
                <div class="my-3"> --}}
                {{-- <button class="btn btn-info" id="exportBtn">Simpan Sebagai PDF</button> --}}
                {{-- <button target="_blank" id="btn-export" class="btn btn-success">Simpan Sebagai PDF</button>
                </div>
                <table class="table dt-responsive nowrap" width="100%" id="treeListTable">
                    <thead>
                        <tr> --}}
                            {{-- <th>Bil</th> --}}
                            {{-- <th>Nama Biasa</th>
                            <th>Nama Saintifik</th>
                            <th>Lokasi</th>
                            <th>Zon</th> --}}
                            {{-- <th>No. Inventori</th> --}}
                            {{-- <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table> --}}
            {{-- </div> <!-- end card body--> --}}
        {{-- </div> <!-- end card --> --}}
    </div><!-- end col-->
</div>

<!-- end row-->


@endsection

@push('datatables')
<script>

    // $(document).ready(function(){

    //     let riskArr = [];
    //     let categoryArr = [];
    //     let silaraArr = [];

    //     $.each($("[name='risk']:checked"),function(){
    //         riskArr.push($(this).val());
    //     });

    //     $.each($("[name='category']:checked"),function(){
    //         categoryArr.push($(this).val());
    //     });

    //     $.each($("[name='silera']:checked"), function(){
    //         silaraArr.push($(this).val());
    //     });

    //     var datass = {
    //         "_token": "{{ csrf_token() }}",
    //         "route":'treeDataMaintain' ,
    //         "zone": $("#zone").val(),
    //         "location": $("[name='lokasi']").val(),
    //         "name": $("[name='nama_biasa']").val(),
    //         "risk": riskArr,
    //         "category": categoryArr,
    //         "silara": silaraArr
    //     };

    //     var table = $('#treeListTable').DataTable({
    //         "processing": true,
    //         "serverSide": true,
    //         "dataSrc": "",
    //         "ajax" :{
    //             "url": "{{ url('treeListDataMaintain') }}",
    //             "dataType": "json",
    //             "type": "POST",
    //             "data": datass
    //         },
    //         "columns": [
    //             // { "data": "treeID" },
    //             // { "data": "DT_Row_Index", "className": "text-center", "orderable": false, "searchable": false },
    //             { "data": "local_name" },
    //             { "data": "saintifik_name" },
    //             { "data": "location" },
    //             { "data": "zone" },
    //             // { "data" : "inventory_no" },
    //             // { "data" : "action" }
    //         ],
    //         aoColumnDefs: [
    //             {
    //                 bSortable: false,
    //                 aTargets: [-1]
    //             }
    //         ],
    //         drawCallback: function(setting){
    //             $('#treeListTable_paginate ul').addClass("pagination-rounded");
    //         },
    //     });

        filtertable = () => {
            riskArr.length = 0;
            categoryArr.length = 0;
            silaraArr.length = 0;

            $.each($("[name='risk']:checked"),function(){
                riskArr.push($(this).val());
            });

            $.each($("[name='category']:checked"),function(){
                categoryArr.push($(this).val());
            });

            $.each($("[name='silera']:checked"), function(){
                silaraArr.push($(this).val());
            });

            var datas = {
                "_token": "{{ csrf_token() }}",
                "route":'treeDataMaintain' ,
                "zone": $("#zone").val(),
                "location": $("[name='lokasi']").val(),
                "name": $("[name='nama_biasa']").val(),
                "risk": riskArr,
                "category": categoryArr,
                "silara": silaraArr
            };

            table.destroy();

            let url = "{{ url('treeListDataMaintain') }}";

            table = $('#treeListTable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax" :{
                    "url": " {{route('filter.table')}} ",
                    "dataType": "json",
                    "type": "POST",
                    "data": datas
                },
                "columns": [
                    // { "data": "treeID" },
                    // { "data": "DT_RowIndex", "className": "text-center" , "orderable": false, "searchable": false},
                    { "data": "local_name" },
                    { "data": "saintifik_name" },
                    { "data": "location" },
                    { "data": "zone" },
                    // { "data" : "inventory_no" },
                    { "data" : "action" }
                ],
                aoColumnDefs: [
                    {
                        bSortable: false,
                        aTargets: [-1]
                    },
                    {
                        searchable: false,
                        targets: -1
                    }
                ],
                drawCallback: function(setting){
                    $('#treeListTable_paginate ul').addClass("pagination-rounded");
                },
            });
        }

        /* AUTO POPULATE FILTER DROPDOWN */
        $('select[name="zone"]').on('change', function(){
            var zoneID = $(this).val();
            if(zoneID) {
                $.ajax({
                    url: '/getLocation?zone='+zoneID,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('select[name="lokasi"]').empty().append('<option value="0">-- Sila Pilih --</option>');
                        $.each(data, function(key, value){

                            let data = `<option value="${value}">${value}</option>`;
                            $('select[name="lokasi"]').append(data);

                        });
                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                        $('[name="lokasi"]').selectpicker('refresh');
                    }
                });
            } else {
                $('select[name="lokasi"]').empty();
            }

        });

        $('select[name="lokasi"]').on('change', function(){

            if($("#zone option:selected").text()==null){
                $('select[name="nama_biasa"').empty();
            }

            var selValue = $("#zone :selected").text();

            var lokasiID = $(this).val();
            if(lokasiID) {
                $.ajax({
                    url: '/getTreeType?zone='+selValue+'&lokasi='+lokasiID,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('[name="nama_biasa"]').empty().append('<option value="0">-- Sila Pilih --</option>');
                        $.each(data, function(key, value){

                            let appendData = `<option value="${value}">${value}</option>`;
                            $('select[name="nama_biasa"]').append(appendData);

                        });
                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                        $('[name="nama_biasa"]').selectpicker('refresh');
                    }
                });
            } else {
                $('select[name="nama_biasa"]').empty();
            }

        });

        $("#btnFilter").on("click", function(){
            filtertable();
        });

        $("#exportBtn").on("click", function(){
            window.open("{{ route('index.table') }}?action=print&"+$.param(table.ajax.params())).print();
        });

        var lnglat = [101.597189, 3.144837]
        var tgm = new tgm.TargomoClient('asia', 'ROOLQGL0Y02SHLW29OFN235445622');

        var tgmOptions = {
            travelType: 'walk',
            travelEdgeWeights: [1800], // 30 minutes only
            maxEdgeWeight: 1800,
            edgeWeight: 'time',
            srid: 4326,
            simplify: 200,
            serializer: 'geojson',
            buffer: 0.002
        };


        var tgmSources = [
            { lat: lnglat[1], lng: lnglat[0], id: 1 }
        ];

        mapboxgl.accessToken = 'pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ';
        var map = new mapboxgl.Map({
            container: 'map',
            center:[101.597189, 3.144837],
            style: 'mapbox://styles/mapbox/streets-v11',
            zoom: 11
        });

        map.loadImage('{{url("/marker.png")}}', function(error, image) {
            if (error) throw error;
            if (!map.hasImage('unclustered-marker')) map.addImage('unclustered-marker', image);
        });

        var BASE_URL = window.location.origin;

        $.ajax({
            type: 'GET',
            url : BASE_URL+"/maintainance-coordinate",
            success:function(response){
                var progress = document.getElementById('progress');
                var progressBar = document.getElementById('progress-bar');

                function updateProgressBar(processed, total, elapsed, layersArray) {
                    if (elapsed > 1000) {
                        // if it takes more than a second to load, display the progress bar:
                        progress.style.display = 'block';
                        progressBar.style.width = Math.round(processed/total*100) + '%';
                    }

                    if (processed === total) {
                        // all markers processed - hide the progress bar:
                        progress.style.display = 'none';
                    }
                }

                // Build GeoJSON object.
                var dataSource = {
                    type: 'FeatureCollection',
                    features: []
                }

                response.forEach(function(d, i) {


                    var content =  '<div class="container-fluid"><table class="table table-bordered">';
                            content += '<tr><td>Nama</td><td> : </td><td>'+ (!d.local_name ? '-' : d.local_name ) +'</td></tr>';
                            content += '<tr><td>Nama Saintifik</td><td> : </td><td>'+ (!d.saintifik_name ? '-' : d.saintifik_name ) +'</td></tr>';
                            content += '<tr><td>Jenis</td><td> : </td><td>'+ (!d.type ? '-' : d.type) +'</td></tr>';
                            content += '<tr><td>Kategori</td><td> : </td><td>'+ (!d.category ? '-' : d.category) +'</td></tr>';
                            content += '<tr><td>Tinggi</td><td> : </td><td>'+ (!d.height ? '-' : d.height ) +'</td></tr>';
                            content += '<tr><td>Silara</td><td> : </td><td>'+ (!d.silara ? '-' : d.silara )+'</td></tr>';
                            content += '<tr><td>Lokasi</td><td> : </td><td>'+ (!d.location || d.location == "" ? '-' : d.location ) +'</td></tr>';
                            content += '<tr><td>Zon</td><td> : </td><td>'+ (!d.zone ? '-' : d.zone) +'</td></tr>';
                            content += '<tr><td>Latitude</td><td> : </td><td>'+ (!d.latitude ? '-' : d.latitude) +'</td></tr>';
                            content += '<tr><td>Longitude</td><td> : </td><td>'+ (!d.longitude ? '-' : d.longitude) +'</td></tr>';
                    content += '</table></div>';

                    d.html = content;

                    dataSource.features[i] = {
                        type: 'Feature',
                        geometry: {
                            type: 'point',
                            coordinates: [
                                d.longitude, d.latitude,
                            ]
                        },
                        properties: d
                    }

                })

                map.on('load', function() {

                    /* GET POLYGON COORDINATES */
                    $.getJSON('https://cors-anywhere.herokuapp.com/http://polygons.openstreetmap.fr/get_geojson.py?id=8347386&params=0', function(d) {

                        /* INVERSE SELECTED POLYGON COLOR */
                        var inverse  = turf.difference(
                            turf.bboxPolygon([-180, 80, 180, -80]), // pretty much the inhabited world
                            turf.polygon(d.geometries[0].coordinates[0])
                        );

                        map.addLayer({
                            'id': 'polygons',
                            'type': 'fill',
                            'source': {
                                'type': 'geojson',
                                'data': inverse
                            },
                            'layout': {},
                            'paint': {
                                'fill-color': '#000',
                                'fill-opacity': .3,
                                'fill-outline-color': '#cd0000',
                                'fill-antialias': true
                            }
                        });
                    })



                    map.on('mouseenter', 'not-maintained-cluster', function() {
                        map.getCanvas().style.cursor = 'pointer';
                    });
                    map.on('mouseleave', 'not-maintained-cluster', function() {
                        map.getCanvas().style.cursor = '';
                    });

                    map.on('mouseenter', 'maintaining-cluster', function() {
                        map.getCanvas().style.cursor = 'pointer';
                    });
                    map.on('mouseleave', 'maintaining-cluster', function() {
                        map.getCanvas().style.cursor = '';
                    });

                    map.on('mouseenter', 'done-maintained-cluster', function() {
                        map.getCanvas().style.cursor = 'pointer';
                    });
                    map.on('mouseleave', 'done-maintained-cluster', function() {
                        map.getCanvas().style.cursor = '';
                    });

                    map.on('mouseenter', 'unclustered-point', function() {
                        map.getCanvas().style.cursor = 'pointer';
                    });
                    map.on('mouseleave', 'unclustered-point', function() {
                        map.getCanvas().style.cursor = '';
                    });

                    // status comparison
                    var notMaintained     = ['==', ['get', 'status_maintance'], 1]
                    var Maintaining  = ['==', ['get', 'status_maintance'], 2]
                    var doneMaintain     = ['==', ['get', 'status_maintance'], 3]

                    // Add the data source as mapbox data source
                    map.addSource('trees', {
                        type: 'geojson',
                        data: dataSource,
                        cluster: true,
                        clusterProperties: {
                            'Belum Diselenggara': ['+', ['case', notMaintained, 1, 0]],
                            'Sedang Diselenggara': ['+', ['case', Maintaining, 1, 0]],
                            'Sudah Diselenggara': ['+', ['case', doneMaintain, 1, 0]],
                        }
                    })

                    // Add maintainance cluster
                    map.addLayer({
                        id: 'not-maintained-cluster',
                        type: 'circle',
                        source: 'trees',
                        filter: [
                            'all',
                            ['>', ['get', 'Belum Diselenggara'], 1]
                        ],
                        paint: {
                            'circle-color': '#FF5D7F',
                            'circle-radius': [
                                'step',
                                ['get', 'point_count'],
                                20,
                                100,
                                30,
                                750,
                                40
                            ]
                        }
                    })


                    // Add maintainance cluster
                    map.addLayer({
                        id: 'maintaining-cluster',
                        type: 'circle',
                        source: 'trees',
                        filter: [
                            'all',
                            ['>', ['get', 'Sedang Diselenggara'], 1]
                        ],
                        paint: {
                            'circle-color': '#FB9834',
                            'circle-radius': [
                                'step',
                                ['get', 'point_count'],
                                20,
                                100,
                                30,
                                750,
                                40
                            ]
                        }
                    })


                    // Add maintainance cluster
                    map.addLayer({
                        id: 'done-maintained-cluster',
                        type: 'circle',
                        source: 'trees',
                        filter: [
                            'all',
                            ['>', ['get', 'Sudah Diselenggara'], 1]
                        ],
                        paint: {
                            'circle-color': '#4AC0C0',
                            'circle-radius': [
                                'step',
                                ['get', 'point_count'],
                                20,
                                100,
                                30,
                                750,
                                40
                            ]
                        }
                    })

                    map.addLayer({
                        id: 'cluster-count',
                        type: 'symbol',
                        source: 'trees',
                        filter: ['has', 'point_count'],
                        layout: {
                            'text-field': '{point_count_abbreviated}',
                            'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                            'text-size': 12,
                        },
                        paint: {
                            'text-color': '#fff'
                        }
                    });

                    map.addLayer({
                        id: 'unclustered-point',
                        type: 'symbol',
                        source: 'trees',
                        filter: ['!', ['has', 'point_count']],
                        layout: {
                            'icon-image': 'unclustered-marker',
                            'icon-size': 0.15
                        },
                    });

                    /*
                    map.on('click', 'unclustered-point', function(e) {

                        var coordinates = e.features[0].geometry.coordinates.slice();
                        var html        = e.features[0].properties.html;

                        // Ensure that if the map is zoomed out such that
                        // multiple copies of the feature are visible, the
                        // popup appears over the copy being pointed to.
                        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                        }

                        new mapboxgl.Popup()
                        .setLngLat(coordinates)
                        .setHTML(html)
                        .addTo(map);
                    });
                    */

                    map.on('click', function(e) {
                        var features = map.queryRenderedFeatures(e.point, {
                            layers: ['unclustered-point'] // replace this with the name of the layer
                        });

                        if (!features.length) {
                            return;
                        }

                        var feature = features[0];

                        var popup = new mapboxgl.Popup({ offset: [0, -40] })
                            .setLngLat(feature.geometry.coordinates)
                            .setHTML(feature.properties.html)
                            .addTo(map);
                    });

                })


                // var markers = L.markerClusterGroup({ chunkedLoading: true, chunkProgress: updateProgressBar() });

                // Add the Risk Tinggi layer.


                // var markerList = [];

                /*
                for(var i = 0;i<response.length;i++)
                {
                    var latlng = L.latLng(d.latitude,d.longitude);
                    var reportMarker = L.marker(latlng);

                    var content =  '<table class="table table-bordered">';
                            content += '<tr><td>Nama</td><td> : </td><td>'+ (!d.local_name ? '-' : d.local_name ) +'</td></tr>';
                            content += '<tr><td>Nama Saintifik</td><td> : </td><td>'+ (!d.saintifik_name ? '-' : d.saintifik_name ) +'</td></tr>';
                            content += '<tr><td>Jenis</td><td> : </td><td>'+ (!d.type ? '-' : d.type) +'</td></tr>';
                            content += '<tr><td>Kategori</td><td> : </td><td>'+ (!d.category ? '-' : d.category) +'</td></tr>';
                            content += '<tr><td>Tinggi</td><td> : </td><td>'+ (!d.height ? '-' : d.height ) +'</td></tr>';
                            content += '<tr><td>Silara</td><td> : </td><td>'+ (!d.silara ? '-' : d.silara )+'</td></tr>';
                            content += '<tr><td>Lokasi</td><td> : </td><td>'+ (!d.location || d.location == "" ? '-' : d.location ) +'</td></tr>';
                            content += '<tr><td>Zon</td><td> : </td><td>'+ (!d.zone ? '-' : d.zone) +'</td></tr>';
                            content += '<tr><td>Latitude</td><td> : </td><td>'+ (!d.latitude ? '-' : d.latitude) +'</td></tr>';
                            content += '<tr><td>Longitude</td><td> : </td><td>'+ (!d.longitude ? '-' : d.longitude) +'</td></tr>';
                            content += '</table>';
                    reportMarker.bindPopup(content);
                    markers.addLayers(reportMarker);
                }
                map.addLayer(markers);
                */
            }
        });
</script>
@endpush
