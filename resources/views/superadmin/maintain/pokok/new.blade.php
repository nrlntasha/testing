@push('datepickercss')
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                </ol>
            </div>
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

            {{-- <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div> --}}

        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pokok</u></h4>
                <form class="needs-validation" action="/superadmin/save-maintance-pokok" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                    <input type="hidden" name="treeID" value="{{ $tree -> treeID }}">
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="lokasi">Lokasi</label>
                            <input type="text" class="form-control" id="lokasi" name="lokasi" value="{{ $tree -> location }}" placeholder="Lokasi" required readonly>
                            <div class="invalid-feedback">
                                Isikan lokasi.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="zon">Zon</label>
                            <input type="text" class="form-control" id="zon" name="zon" value="{{ $tree -> zone }}" placeholder="Zon" required readonly>
                            <div class="invalid-feedback">
                                Isikan zon.
                            </div>
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control" id="Latitude" name="latitude" value="{{ $tree -> latitude }}" placeholder="Latitude" readonly required>
                            <div class="invalid-feedback">
                                Pilih Latitude.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control" id="Longitude" name="longitude" value="{{ $tree -> longitude }}" placeholder="Longitude" readonly required>
                            <div class="invalid-feedback">
                                Pilih Longitude.
                            </div>
                        </div>

                    </div>
                    <hr/>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="Silara">Taraf Risiko</label>
                            <select class="selectpicker form-control" data-live-search="true" data-style="btn-light" name="risk_level" required>
                                @foreach($maintainanceRisk as $risk)
                                    <option value="{{$risk -> id}}">{{$risk -> name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih risiko.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Status Penyelenggaraan</label>
                            <select class="selectpicker form-control" data-live-search="true" data-style="btn-light" name="status_maintance" required>
                                @foreach($maintainanceStatus as $status)
                                    <option value="{{$status -> id}}">{{$status -> status}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih jenis penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Jenis Penyelenggaraan</label>
                            <select style="max-width: 300px !important;" class="selectpicker form-control" data-size="5" data-live-search="true" data-style="btn-light" name="type_maintance" required>
                                @foreach($maintainanceActivity as $maintainance)
                                    <option value="{{$maintainance -> id}}">{{$maintainance -> activity_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Pilih status penyelenggaraan.
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Silara">Tarikh</label>
                            <input type="text" class="form-control" name="maintain_date" data-provide="datepicker" data-date-autoclose="true" autocomplete="off" required>
                            <div class="invalid-feedback">
                                Pilih tarikh.
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="lokasi">Catatan</label>
                            <textarea id="textarea" class="form-control" name="notes" maxlength="191" rows="3" placeholder="This catatan has a limit of 191 chars."></textarea>
                            <div class="invalid-feedback">
                                Isikan catatan.
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="gambar_sebelum">Gambar Sebelum</label>
                            <input type="file" name="gambar_sebelum[]" id="example-fileinput" class="form-control-file" multiple required>
                            <div class="invalid-feedback">
                                Pilih Gambar.
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="gambar_selepas">Gambar Selepas</label>
                            <input type="file" name="gambar_selepas[]" id="example-fileinput" class="form-control-file" multiple required>
                            <div class="invalid-feedback">
                                Pilih Gambar.
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">Simpan Maklumat Selenggara</button>
                </form>
                <hr/>
                <div class="card-box" style="height:600px;" id="MapLocation">
                    <div id="progress">
                        <div id="progress-bar"></div>
                    </div>
                    <div></div>
                </div>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->

@push('datepicker')
<script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script>
@endpush


<script>

    // use below if you want to specify the path for leaflet's images
    //L.Icon.Default.imagePath = '@Url.Content("~/Content/img/leaflet")';

    var curLocation = [0, 0];
    // use below if you have a model
    // var curLocation = [@Model.Location.Latitude, @Model.Location.Longitude];

    if (curLocation[0] == 0 && curLocation[1] == 0) {
        curLocation = [{{ $tree -> latitude }}, {{ $tree -> longitude }}];
    }

    var map = L.map('MapLocation').setView(curLocation, 15);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ', {
        maxZoom: 20,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZGV2dHJhY2tlcmhlcm8iLCJhIjoiY2p1a2prNDdyMGt5NjN6bWlnbjRibWpqZyJ9.1CAf5eHM9omDqb7hh64arQ',
    }).addTo(map);

    map.attributionControl.setPrefix(false);

    var marker = new L.marker(curLocation, {
        draggable: false
    });

    // marker.on('dragend', function(event) {
    //     var position = marker.getLatLng();
    //     marker.setLatLng(position, {
    //     draggable: 'true'
    //     }).bindPopup(position).update();
    //     $("#Latitude").val(position.lat);
    //     $("#Longitude").val(position.lng).keyup();
    // });

    $("#Latitude, #Longitude").change(function() {
        var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
        marker.setLatLng(position, {
        draggable: false
        }).bindPopup(position).update();
        map.panTo(position);
    });

    map.addLayer(marker);

</script>


@endsection
