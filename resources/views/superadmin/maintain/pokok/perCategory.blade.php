@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Penyelenggaraan</a></li>
                </ol>
            </div>
            <h4 class="page-title">Penyelenggaraan {{ $categoryType }} </h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="animated fadeInLeft slow col-12">
        <div class="card">
            <div class="card-body">
            <h4 class="header-title">Senarai Pokok Diselenggara {{ session()->get('days') }}</h4>

                <table id="basic-datatable2" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Risiko</th>
                            <th>Jenis</th>
                            <th>Status</th>
                            <th>Zon</th>
                            <th>Tarikh</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($zonCategory as $maintain)
                        <tr>
                            <td>{{ $maintain -> risk_level }}</td>
                            <td>{{ $maintain -> type_maintance }}</td>
                            <td>{{ $maintain -> status_maintance }}</td>
                            <td>{{ $maintain -> zone }}</td>
                            <td>{{ date('d-m-Y', strtotime($maintain -> maintain_date)) }}</td>
                            <td>
                                <button type="button" onclick="window.location.href='{{route('maintainance.update.pokok', $maintain -> maintainID)}}'" class="btn btn-outline-success waves-effect waves-light">Kemaskini</button>
                                <button type="button" data-id="{{ $maintain -> maintainID }}" class="btn btn-outline-danger waves-effect waves-light deleteMaintain">Padam</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    $('.deleteMaintain').click(function() {
                        var maintainID = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-maintance-pokok/" + maintainID;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<!-- end row-->


@endsection

@push('datatables')
<script>
    $.fn.dataTable.ext.errMode = 'none';

    $(document).ready(function(){
        $('#treeListTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax" :{
                "url": "{{ url('treeListDataMaintain') }}",
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{ csrf_token() }}", route:'treeDataMaintain' }
            },
            "columns": [
                // { "data": "treeID" },
                { "data": "local_name" },
                { "data": "saintifik_name" },
                { "data": "location" },
                { "data": "zone" },
                { "data" : "inventory_no" },
                { "data" : "action" }
            ],
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [-1]
                }
            ]
        });
    });
</script>
@endpush
