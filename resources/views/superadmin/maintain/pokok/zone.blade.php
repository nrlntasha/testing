@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Zon</a></li>
                </ol>
            </div>
            <h4 class="page-title">Zon</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-xl-5">
        <div class="form-group mb-3">
            <input type="text" id="zoneInput" name="example-input-large" class="form-control form-control-lg" placeholder="Cari nama zon">
        </div>
    </div>
</div>

<div class="row" id="zoneSearch">

  @foreach ($perZones as $perZone)
    <div class="col-4 zoneName">
        <div class="card-box">
            <div class="row">
                <div class="col-6">
                    <div class="">
                        <h4 class="text-muted my-1 zoneTitle">{{$perZone -> zone}}</h4>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark my-1"><span data-plugin="counterup">
                            {{$perZone -> dataCount}}
                        </span> - Pokok</h3>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <form action="/superadmin/category-maintain/per-zone" method="POST">
                        @csrf
                       <input type="hidden" name="zone_name" value="{{ $perZone -> zone }}">
                       <input type="submit" class="animated infinite pulse btn btn-success waves-effect waves-light" name="submit" value="Papar Pokok">
                    </form>
                    {{-- <a href="/category-maintain/per-category/{{ $perCategory ->status_maintance }}">Senarai Pokok</a> --}}
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>
  @endforeach



    {{-- @foreach ($perCategorys as $perCategory)
        <div class="animated fadeIn slow col-xl-4 col-md-4 zoneName">
            <div class="card-box">
                <h4 class="header-title mt-0 mb-3 zoneTitle">{{$perCategory -> status_maintance}}</h4>

                <div class="row">
                    <div class="col-4">
                        <h3 class="mt-0 font-20">{{$perCategory -> dataCount}}</h3>
                    </div>
                </div>

            </div>
        </div><!-- end col -->
    @endforeach --}}
</div>
<!-- end row-->

@push('searchZone')
<script>
$(document).ready(function(){
  $('#zoneInput').keyup(function(){

   // Search text
   var text = $(this).val().toLowerCase();

   // Hide all content class element
   $('.zoneName').hide();

   // Search
   $('.zoneTitle').each(function(){

    if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ){
     $(this).closest('.zoneName').show();
    }
  });
 });
});

</script>

@endpush

@push('chartjs')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{asset('assets/js/pages/chartjs-plugin-datalabels.js')}}"></script>
@endpush

@endsection
