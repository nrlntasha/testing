@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Tetapan</a></li>
                </ol>
            </div>
            <h4 class="page-title">Tetapan</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 
    
<div class="row">
    <div class="col-12">

        @if(session()->has('success'))
            <script>
                setTimeout(function() {
                    Swal.fire({
                        title: "{{ session()->get('success') }}!",
                        type: "success",   
                        timer: 2000,   
                        showConfirmButton: false 
                    });
                }, 500);
            </script>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors as $error)
                {{ $error }}
            @endforeach
        @endif

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Kategori</h4>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach ($categorys as $category)
                        <tr>
                            <td>{{ $category -> category }}</td>
                            <td>
                                <button type="button" data-id="{{ $category -> id }}" class="btn btn-outline-danger waves-effect waves-light deleteDevice">Padam</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    $('.deleteCategory').click(function() {
                        var id = $(this).data('id')
                        Swal.fire({
                            title: 'Anda Pasti?',
                            text: "Tindakan ini tidak boleh diulang kembali!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Padam!'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/superadmin/delete-category/" + id;
                            }
                        })
                    });
                </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->


@endsection