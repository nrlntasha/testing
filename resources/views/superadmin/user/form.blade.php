@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pengguna</a></li>
                </ol>
            </div>
            <h4 class="page-title">Tambah Pengguna</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">

@if($errors->any())
  <div class="animated pulse infinite alert alert-danger bg-danger text-white border-0" role="alert">
    @foreach($errors->getMessages() as $this_error)
        !!! {{$this_error[0]}}
    @endforeach
  </div>
@endif
        <div class="card">
            <div class="card-body">
                <h4 class="header-title"><u>Maklumat Pengguna</u></h4>
                <hr>
                <form class="needs-validation" action="/superadmin/user/store" method="POST" enctype="multipart/form-data" novalidate>
                  @csrf
                    <div class="form-row">


                        <div class="form-group col-md-4">
                            <label for="role">Jawatan</label>
                            <select name="role" id="role" class="form-control">
                                <option value="Admin">Urusetia</option>
                                <option value="Landskap">Kontraktor Landskap Lembut</option>
                                <option value="Kejur">Kontraktor Landskap Kejur</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="full_name">Nama Penuh</label>
                            <input type="text" class="form-control" name="full_name" id="full_name" value="{{ old('full_name') }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="ic_number">Nombor IC</label>
                            <input type="text" class="form-control" name="ic_number" id="ic_number" value="{{ old('ic_number') }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="phone_number">Nombor Bimbit</label>
                            <input type="text" class="form-control" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username" value="{{ old('username') }}" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="password">Kata Laluan</label>
                            <input type="password" class="form-control" name="password" id="password" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="profile_img">Gambar Profil</label>
                            <input type="file" name="profile_img" class="form-control-file" id="profile_img">

                        </div>


                    </div>

                    <button type="submit" name="submit" class="btn btn-outline-success waves-effect waves-light">Simpan</button>
                    <button type="button" onclick="history.go(-1); return false;" class="btn btn-outline-danger waves-effect waves-light">Batal</button>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection

@push('customPageJS')
    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Tour init js-->
    <script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
@endpush
