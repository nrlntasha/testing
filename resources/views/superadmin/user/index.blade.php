@extends('layouts.admin')

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">MBPJ</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pengguna {{ $user }}</a></li>
                </ol>
            </div>
            <h4 class="page-title">{{ $user }}</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">

    <div class="animated fadeInLeft slow col-xl-6">
        <div class="card cta-box bg-success text-white">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body">

                        <h3 class="m-0 font-weight-normal text-white sp-line-1 cta-box-title">Pengguna baharu</h3>
                        <p class="text-light mt-2 sp-line-2"></p>
                        {{-- <a href="javascript: void(0);" class="text-white font-weight-semibold text-uppercase">Read More <i class="mdi mdi-arrow-right"></i></a> --}}
                        <button type="button" onclick="window.location.href='/superadmin/user/new/{{ $user }}'" class="animated infinite pulse btn btn-success waves-effect waves-light">Tambah Pangguna {{ $user }} <i class="mdi mdi-arrow-right-thick mr-1"></i></button>
                    </div>
                    {{-- <img class="ml-3" src="{{asset('assets/images/update.svg')}}" width="120" alt="Generic placeholder image"> --}}
                </div>
            </div>
            <!-- end card-body -->
        </div>
    </div>

</div>

<div class="row">
    <div class="animated fadeInRight duration2s col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Senarai Pengguna</h4>

                @if(session()->has('success'))
                    <script>
                        setTimeout(function() {
                            Swal.fire({
                                title: "{{ session()->get('success') }}!",
                                type: "success",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }, 500);
                    </script>
                @endif

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nama Penuh</th>
                            <th>Nombor K/P</th>
                            <th>No Telefon Bimbit</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($landskaps as $landskap)
                        <tr>
                            <td>{{ $landskap -> full_name }}</td>
                            <td>{{ $landskap -> ic_number }}</td>
                            <td>{{ $landskap -> phone_number }}</td>
                            <td>
                                <button type="button" data-id="{{ $landskap -> ic_number }}" class="btn btn-outline-danger waves-effect waves-light deleteUser" title="Padam" data-toggle="tooltip"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <script>
                        $('.deleteUser').click(function() {
                            var ic_number = $(this).data('id')
                            Swal.fire({
                                title: 'Anda Pasti?',
                                text: "Tindakan ini tidak boleh diulang kembali!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ya, Padam!',
                                cancelButtonText: 'Batal'
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = "/superadmin/user/delete/" + ic_number;
                                }
                            })
                        });
                    </script>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
</div>
<!-- end row-->
@endsection
