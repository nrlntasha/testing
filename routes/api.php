<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::middleware('jwt.verify')->group(function(){

    Route::get('/zone', 'API\APISearchController@allzone');
    Route::get('/lokasi', 'API\APISearchController@lokasi');
    Route::get('/nama-biasa', 'API\APISearchController@nama_biasa');
    Route::get('/semua', 'API\APISearchController@semua');
    Route::get('/saintifik', 'API\APISearchController@saintifik');
    Route::get('/user/detail', API\APIUserDetailController::class);

    /* TREE MODULE */
    Route::prefix('pokok')->group(function(){
        Route::post('/store', 'API\APITreeController@store');
        Route::get('', 'API\APITreeController@index');
        Route::post('/update/{treeID}', 'API\APITreeController@update');
        Route::get('/delete/{treeID}', 'API\APITreeController@destroy');
        Route::get('/chart/{id}', 'API\APITreeController@treeGroupData');
        Route::get('/byID/{id}', 'API\APITreeController@treeByID');
        Route::get('/count', 'API\APITreeController@treeCount');
        Route::get('/statistic', 'API\APITreeController@statistic');
        Route::get('/risk_statistic', 'API\APITreeController@riskStatistic');
        Route::get('/risk', 'API\APITreeController@risk');
        Route::get('/all-scientific-name', 'API\APISearchController@allSaintifik');
        Route::get('/all-local-name', 'API\APISearchController@allLocal');
        Route::get('/findbyscientificname', 'API\APISearchController@findByScientificName');
        Route::get('/getsilara', 'API\APISearchController@getSilara');
        Route::get('/getzone', 'API\APISearchController@getZone');
        Route::get('/summary/count', 'API\APITreeController@count');
        Route::get('/summary/perzone', 'API\APITreeController@sumPerzone');
        Route::get('/summary/permonth/{zone}', 'API\APITreeController@sumPermonth');
    });

    /* MAINTAINANCE */
    Route::prefix('maintance')->group(function(){
        Route::get('', 'API\APIMaintanceController@index');
        Route::post('/store', 'API\APIMaintanceController@store');
        Route::post('/update/{mainID}', 'API\APIMaintanceController@update');
        Route::delete('/delete/{mainID}', 'API\APIMaintanceController@destroy');
        Route::get('/byID/{mainID}', 'API\APIMaintanceController@getByID');
        Route::get('/getmaintainanceactivity', 'API\APIMaintanceController@getMaintainanceActivity');
    });

    Route::prefix('kejur')->group(function(){
        /* KEJUR */
        Route::get('', 'API\APIKejurController@index');
        Route::get('/getarea', 'API\APIKejurController@getKawasan');
        Route::get('/getproperty', 'API\APIKejurController@getAlatan');
        Route::post('/store', 'API\APIKejurController@store');
        Route::get('/detail/{id}', 'API\APIKejurController@detail');
        Route::post('/save/{id}', 'API\APIKejurController@saveUpdate');
        Route::get('/deleteGambar/{id}', 'API\APIKejurController@deleteGambar');
        Route::post('/upload', 'API\APIKejurController@upload');
        Route::post('/maintainance/update', 'API\APIKejurController@updateMaintainance');
        /* KEJUR STATISTIC */
        Route::get('/summary/count', 'API\APIKejurController@countKejur');
        Route::get('/summary/perzone', 'API\APIKejurController@perZone');
        Route::get('/summary/property', 'API\APIKejurController@propertyList');
        Route::get('/sync', 'API\APIKejurController@syncKejur');
        /* END HERE */
    });

    /* REPORT */
    Route::prefix('report')->group(function(){
        Route::get('pokok', 'API\APIReportController@treeReport');
        Route::get('kejur', 'API\APIReportController@kejurReport');
    });
    /* END HERE */

    /* NOTIFICATION SECTION */
    Route::prefix('notification')->group(function(){
        /* FETCH USER LIST */
        Route::post('getuser', 'API\APINotificationController@listUsers');

        /* MESSAGE FUNCTION SECTION */
        Route::prefix('message')->group(function(){
            Route::post('send', 'API\APINotificationController@sendMessage');
            Route::get('read/{id}', 'API\APINotificationController@read');
            Route::get('count/{username}', 'API\APINotificationController@unreadCount');//Unread count
            Route::get('/{username}', 'API\APINotificationController@message');
        });

    });
// });

    /* END HERE */

    Route::group(['prefix' => 'auth'], function() {
        Route::post('login', API\Auth\LoginController::class);
        Route::post('logout', API\Auth\LogoutController::class);
        Route::post('register', API\Auth\RegisterController::class);
    });
