<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//

// Setting
Route::prefix('settings')->group(function() {
    Route::get('', 'SettingController@settingPage');
    Route::post('password', 'SettingController@updatePassword');
});

Route::get('/', 'LoginController@index');
Route::group(['middleware' => 'checkuser'],function(){
    Route::post('/', 'LoginController@check_login');
});

Route::group(['middleware' => 'sessionsuperadmin'], function(){

  Route::prefix('superadmin')->group(function(){

    Route::get('/', 'SuperadminController@index');

    Route::post('/settings', 'SettingController@update');

    Route::post('/ajax/onesignal/store', 'SuperadminController@storeOnesignal');

    Route::get('/lembut', 'LembutController@index');
    Route::get('/lembut/custom', 'LembutController@customData');

    Route::get('/log-audit', 'Superadmin\LogAuditController@index');

    //tree
    Route::get('/tree', 'TreeController@index');
    Route::post('/save-tree', 'TreeController@save');
    Route::get('/detail/{id}', 'TreeController@detail2');
    Route::get('/detail-pokok/{treeID}', 'TreeController@detail');
    Route::patch('/update-pokok', 'TreeController@update');
    Route::get('/delete-pokok/{treeID}', 'TreeController@delete');

    //kejur
    Route::get('/kejur', 'KejurController@index');
    Route::get('/kejur/new', 'KejurController@new');
    Route::post('/kejur/store', 'KejurController@store');
    Route::get('kejur/detail/{id}', 'KejurController@detail');
    Route::get('kejur/update/{id}', 'KejurController@detail2');
    Route::patch('kejur/save/{id}', 'KejurController@saveUpdate');
    Route::get('/kejur/gambar/{id}', 'KejurController@gambar');
    Route::get('/kejur/deleteGambar/{id}', 'KejurController@deleteGambar');
    Route::post('/kejur/upload/{id}', 'KejurController@upload');
    Route::get('/kejur/delete/{id}', 'KejurController@deleteAll');
    Route::get('/kejur/fulldetail/{id}', 'KejurController@fulldetail');
    Route::get('/kejur/export/{id}', 'KejurController@export')->name('kejur.export');

    Route::get('/kejur/report/detail/{id}', 'ReportGenerateController@detailKejur');

    // report report pokok
    Route::get('/report-maintainance-pokok', 'treeMaintainanceController@index');
    Route::post('/report-maintainance-pokok', 'treeMaintainanceController@report')->name('treeMaintainanceReport');
    //report pokok
    Route::get('/report-pokok', 'TreeReportController@index');

    //report kejur
    Route::get('/report-kejur', 'KejurReportController@index');

    //maintance
    Route::get('/maintance-pokok', 'MaintainController@pokok');
    Route::get('/maintance-pokok/datatables', 'MaintainController@datatables')->name('index.table');
    Route::post('/maintance-pokok/datatables', 'MaintainController@datatables')->name('filter.table');
    Route::get('/maintance-pokok/{treeID}', 'MaintainController@detail_pokok');
    Route::post('/save-maintance-pokok', 'MaintainController@save_pokok');
    Route::get('/maintance-update-pokok/{maintainID}', 'MaintainController@update_pokok');
    Route::patch('/update-maintance-pokok', 'MaintainController@save_update');
    Route::get('/delete-maintance-pokok/{maintainID}', 'MaintainController@delete');
    Route::get('/category-maintain', 'MaintainController@allCategory');
    Route::get('/zone-maintain', 'MaintainController@allZone');
    Route::any('/category-maintain/per-category', 'MaintainController@perCategory');
    Route::any('/category-maintain/per-zone', 'MaintainController@listPerZone');
    Route::post('/export-pokok', 'LembutController@exportPDF')->name('export.softscape.maintainance');
    Route::get('/maintance/print/{id}', 'MaintainController@printById');

    //Maintaince Kejur
    Route::get('/maintance-kejur', 'KejurMaintenanceController@index');

    //akrib
    Route::get('/akrib', 'UploadController@index');

    //user
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@showUser');
        Route::get('/new', 'UserController@formUser');
        Route::post('/store', 'UserController@storeUser');
        Route::post('/update', 'UserController@updateUser');
        Route::get('/delete/{id}', 'UserController@deleteUser');
    });


    //tetapan
    Route::get('/tetapan/lembut/1', 'Superadmin\Setting\LembutController@category');

    Route::get('/report-month/{first}/{second}', 'ReportController@byMonth');

    Route::get('/test/pdf/{id}', 'GenerateReportController@test');

  });
});

Route::group(['middleware' => 'sessionlandskap'], function(){
    Route::prefix('landskap')-> group(function(){
      Route::get('/', 'SuperadminController@index');

      //softscape
      Route::get('/lembut', 'LembutController@index');
      Route::get('/lembut/custom', 'LembutController@customData');

      Route::get('/log-audit', 'Superadmin\LogAuditController@index');

      //tree
      Route::get('/tree', 'TreeController@index');
      Route::post('/save-tree', 'TreeController@save');
      Route::get('/detail/{id}', 'TreeController@detail2');
      Route::get('/detail-pokok/{treeID}', 'TreeController@detail');
      Route::patch('/update-pokok', 'TreeController@update');
      Route::get('/delete-pokok/{treeID}', 'TreeController@delete');

      //maintance
      Route::get('/maintance-pokok', 'MaintainController@pokok');
      Route::get('/maintance-pokok/{treeID}', 'MaintainController@detail_pokok');
      Route::post('/save-maintance-pokok', 'MaintainController@save_pokok');
      Route::get('/maintance-update-pokok/{maintainID}', 'MaintainController@update_pokok')->name('maintainance.update.pokok');
      Route::patch('/update-maintance-pokok', 'MaintainController@save_update');
      Route::get('/delete-maintance-pokok/{maintainID}', 'MaintainController@delete');
      Route::get('/category-maintain', 'MaintainController@allCategory');
      Route::get('/zone-maintain', 'MaintainController@allZone');
      Route::any('/category-maintain/per-category', 'MaintainController@perCategory');
      Route::any('/category-maintain/per-zone', 'MaintainController@listPerZone');

      //akrib
      Route::get('/akrib', 'UploadController@index');

    });
});

Route::group(['middleware' => 'sessionkejur'], function(){
  Route::prefix('kejur')-> group(function(){
    Route::get('/dashboard', 'SuperadminController@index');

    Route::get('/log-audit', 'Superadmin\LogAuditController@index');

    //kejur
    Route::get('/kejur', 'KejurController@index');
    Route::get('/kejur/new', 'KejurController@new');
    Route::post('/kejur/store', 'KejurController@store');
    Route::get('kejur/detail/{id}', 'KejurController@detail');
    Route::get('kejur/update/{id}', 'KejurController@detail2');
    Route::patch('kejur/save/{id}', 'KejurController@saveUpdate');
    Route::get('/kejur/gambar/{id}', 'KejurController@gambar');
    Route::get('/kejur/deleteGambar/{id}', 'KejurController@deleteGambar');
    Route::post('/kejur/upload/{id}', 'KejurController@upload');
    Route::get('/kejur/delete/{id}', 'KejurController@deleteAll');
    Route::get('/kejur/fulldetail/{id}', 'KejurController@fulldetail');

    Route::get('/kejur/report/detail/{id}', 'ReportGenerateController@detailKejur');

    //Maintaince Kejur
    Route::get('/maintance-kejur', 'KejurMaintenanceController@index');

    //report kejur
    Route::get('/report-kejur', 'KejurReportController@index');

    //akrib
    Route::get('/akrib', 'UploadController@index');

  });
});

Route::group(['middleware' => 'sessionintern'], function(){
  Route::prefix('intern')-> group(function(){
    Route::get('/', 'SuperadminController@index');

  });
});

Route::get('coordinate','SuperadminController@getCoordinate');
Route::get('maintainance-coordinate','SuperadminController@getMaintainanceCoordinate');
Route::get('nama-saintifik', 'AutocompleteController@saintifik');
Route::get('graph/dashboard', 'SuperadminController@dashboardGraph');

Route::get('autocomplete', 'AutocompleteController@nama_biasa')->name('autocomplete');
Route::get('autocomplete-saintifik', 'AutocompleteController@nama_saintifik')->name('autocomplete-saintifik');
Route::get('autocomplete-inventory', 'AutocompleteController@inventory_no')->name('autocomplete-inventory');
Route::get('autocomplete-lokasi', 'AutocompleteController@lokasi')->name('autocomplete-lokasi');
Route::get('autocomplete-zon', 'AutocompleteController@zon')->name('autocomplete-zon');

/* AUTO FILL TREE FORM URL */
Route::get('autofill-tree', 'AutocompleteController@autoFill')->name('autofill-tree');

//autocomplete kejur
Route::get('kejur-zon', 'AutocompleteController@kejur_zon')->name('kejur-zon');
Route::get('kejur-lokasi', 'AutocompleteController@kejur_lokasi')->name('kejur-lokasi');
Route::get('kejur-inventory', 'AutocompleteController@kejur_inventory')->name('kejur-inventory');

//DataTables
Route::get('treeData', 'LembutController@index');
Route::post('treeListData', 'LembutController@treeListData');

Route::get('treeDataMaintain', 'MaintainController@pokok');
Route::post('treeListDataMaintain', 'DataTablesController@treeListDataMaintance');

Route::get('getLocation', 'TreeReportController@getLocation');
Route::get('getTreeType', 'TreeReportController@getTreeType');

Route::get('superadmin/date-session/{id}', 'DateSessionController@index');
Route::post('superadmin/custom-date-session/', ['uses'=> 'DateSessionController@customDate', 'as'=>'custom-date']);

//exit
Route::get('logout', 'LoginController@logout');
